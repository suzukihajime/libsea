
/**
 * @file naive_linear_banded.c
 *
 * @brief the simplest implementation of the linear-gap cost, banded DP.
 *
 * @details
 * This impementation serves as the reference implementation of the linear-gap cost
 * banded dynamic programming algorithm. The reference of this implementation is
 * the naive full algorithm in naive_linear_full.c. The consistency of the results
 * are tested in the test_cross_naive_linear_banded function.
 */
#ifdef DEBUG
	#include <stdio.h>
#endif
#ifdef MAIN
	#include <stdio.h>
	#include <string.h>
#endif
#ifdef TEST
 	#include "../util/tester.h"
#endif

#include <stdlib.h>						/* for definition of the NULL */
#include "../include/sea.h"				/* for API definitions, flag definitions, and structs */
#include "../util/util.h"				/* for internal utility functions */
#include "../seqreader/seqreader.h"		/* for input sequence reader and output alignment writer */
#include "../util/bench.h"				/* for benchmarking */

#define	ADDR(p, q, band)			( (band)*(p)+(q)+(band)/2 )
#define ADDRI(x, y, band) 			( ADDR(COP(x, y, band), COQ(x, y, band), band) )
#define COX(p, q, band)				( ((p)>>1) - (q) )
#define COY(p, q, band)				( (((p)+1)>>1) + (q) )
#define COP(x, y, band)				( (x) + (y) )
#define COQ(x, y, band) 			( ((y)-(x))>>1 )
#define INSIDE(x, y, p, q, band)	( (COX(p, q, band) < (x)) && (COY(p, q, band) < (y)) )

#define TOPQ(p, q, band) 			( - !((p)&0x01) )
#define LEFTQ(p, q, band) 			( ((p)&0x01) )

#define TOP(p, q, band)				( -(band) + TOPQ(p, q, band) )
#define LEFT(p, q, band)			( -(band) + LEFTQ(p, q, band) )
#define TOPLEFT(p, q, band) 		( -2*(band) )

#define DIR_TOP 					( 0x01 )
#define DIR_LEFT 					( 0 )
#define DIR(p)						( ((p) & 0x01) ? DIR_TOP : DIR_LEFT )		// どちらから来たか表す

/**
 * @fn naive_linear_banded
 *
 * @brief a non-SIMD linear-gap cost banded dynamic programming function. 
 *
 * @param[ref] aln : a pointer to sea_result structure. aln must NOT be NULL. (a structure which contains an alignment result.)
 * @param[in] aln->a : a pointer to query sequence a.
 * @param[inout] aln->apos : (input) the start position of the search section on sequence a. (0 <= apos < length(sequence a)) (output) the start position of the alignment.
 * @param[inout] aln->alen : (input) the length of the search section on sequence a. (0 < alen) (output) the length of the alignment.
 * @param[in] aln->b : a pointer to query sequence b.
 * @param[inout] aln->bpos : same as apos.
 * @param[inout] aln->blen : same as alen.
 *
 * @param[out] aln->aln : a pointer to the alignment string.
 * @param[inout] aln->len : (input) the reserved length of the aln->aln. (output) the length of the alignment string.
 * @param[out] aln->score : an alignment score.
 *
 * @param[in] scmat : a pointer to the DP cost array. see also: sea_context, sea_init.
 * @param[in] xdrop : the xdrop threshold.
 * @param[in] bandwidth : the band width of the DP array.
 * @param[in] mat : a pointer to the DP matrix.
 * @param[in] matsize : the size of the DP matrix.
 *
 * @return status. zero means success. see sea.h for the other error code.
 *
 * @detail
 * this function implements the linear-gap cost, banded dynamic programming (DP) algorithms.
 * this one function serves as the common three alignment algorithms,
 * the Smith-Waterman (local), the Needleman-Wunsch (global), and the seed-and-extend alignment.
 * the selection of the algorithm is done by giving proper definitions under compilation,
 * or giving flag such as -DALG=SW or equivalent to the compiler yields an object
 * which contains the Smith-Waterman alginment algorithm.
 *
 * the following define macros MUST BE given to the compiler to generate correct objects.
 *     ALG : algorithm switch. see wscript for more details.
 *     SEQ : input sequence format. see seqreader for more details.
 *     ALN : output alignment format. see seqreader for more details.
 */
sea_int_t
DECLARE_FUNC_GLOBAL(naive_linear_banded, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	void *mat)
{
	/**
	 * a banded matrix and its coordinates:
	 *
	 * i and j represents horizontal and vertical coordinates, the same as
	 * the normal (rectangular) matrix in naive_linear_full.c.
	 * p and q represents diagonal and anti-diagonal coordinates in banded matrix.
	 * the (i, j)-coordinate is used to designate positions of bases in query sequences
	 * and the (p, q)-coordinate is used to designate address of cells in
	 * the dynamic programming matrix.
	 *
	 * the conversions between the two coordinates are:
	 *     (p, q) = (i + j, floor((j - i) / 2))
	 *       or
	 *     (i, j) = (floor(p / 2) - q, ceil(p / 2) + q)
	 *
	 * the index of a cell at (p, q) in the dpmat array is:
	 *     p * bandwidth + q + bandwidth / 2
	 *
	 * a bandwidth is defined as the number of the cells in an antidiagonal cross section.
	 * a 4-cell wide band is illustrated below:
	 *
	 *              input sequence a
	 *                 A  T  A  C  ..
	 *           O  0  1  2  3  4  ..             i
	 *            +-------------------------------->
	 *           0| a|  |  |  |  |
	 *            |--+--+--+--+--+--+
	 *  input  A 1| b| c|  |  |  |  |
	 * sequence   |--+--+--+--+--+--+--+
	 *    b    T 2|  |  |  |  |  |d0|  |      (A)
	 *            |--+--+--+--+--+--+--+--+
	 *         C 3|  |  |  |  |d1|  |  |  |
	 *            |--+--+--+--+--+--+--+--+--+
	 *         C 4|  |  |  |d2|  |  |  |  |  |
	 *            |  +--+--+--+--+--+--+--+--+--+
	 *         : :|     |d3|  |  |  |  |  |  |  |
	 *            |     +--+--+--+--+--+--+--+--+-
	 *            |        |  |  |  |  |  |  |  |
	 *            |  (B)   +--+--+--+--+--+--+--+-
	 *          j v
	 * 
	 * a : the cell at top-left corner of the matrix. (i, j) = (0, 0) and (p, q) = (0, 0).
	 *     inirialized with zero. the address of this cell is 2 (= 0 * bandwidth + 0 + bandwidth / 2).
	 * b : the cell at (i, j) = (0, 1) or (p, q) = (1, 0). the address of this cell is
	 *     6 (= 1 * bandwidth + 0 + bandwidth / 2).
	 * c : the first calculated cell in the matrix. (i, j) = (1, 1) and (p, q) = (2, 0).
	 * d0..d3 : a set of cells in an anti-diagonal bar.
	 *          (p, q)-coordinates of d0 .. d3 are (7, -2), (7, -1), (7, 0), and (7, 1).
	 * (A) and (B) : omitted areas.
	 */
	sea_int_t i, j, mi = 0, mj = 0;		/** (i, j)-coordinates */
	sea_int_t p, q, mp = 0, mq = 0;		/** (p, q)-coordinates */
	sea_int_t t, d, l, max = 0;			/** temporaries to hold scores from top, diagonal, and left cells */
	sea_cell_t *pdp;					/** ptr to a current cell */
	sea_int_t cell, sc;
	sea_int_t xacc;						/** xdrop accumulator */
	sea_cell_t const min = (ALG == SW) ? 0 : SEA_CELL_MIN;		/** the lower limit of the scores */

	DECLARE_SEQ(a);
	DECLARE_SEQ(b);
	DECLARE_ALN(l);
	DECLARE_BENCH(fill);
	DECLARE_BENCH(search);
	DECLARE_BENCH(trace);
	sea_int_t	m = param.m,
				x = param.x,
				g = param.gi,
				xdrop = param.xdrop,
				bw = param.bandwidth;
	sea_int_t 	alen = aln->alen,
				blen = aln->blen;
	sea_int_t 	amat = alen+1,
				bmat = blen+1;
	sea_int_t	alim = alen+bw/2,
				blim = blen+bw/2;
	sea_cell_t	*dpmat = (sea_cell_t *)mat + bw;

	START_BENCH(fill);						/** start of fill step */

	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);

	pdp = dpmat-bw;
	for(q = 0; q < 2*bw; q++) {
		*pdp++ = SEA_CELL_MIN;
	}
	dpmat[ADDRI(0, 0, bw)] = 0;

	p = 0; i = 0; j = 0;					/** pdp = dpmat+ADDR(1, -bw/2, bw); */
	while(i < alim && j < blim) {
		i += p & 0x01; j += !(p & 0x01); p++;
		if(ALG == XSEA) { xacc = -1; }
		for(q = -bw/2; q < bw/2; q++) {
			FETCH(a, (i-q)-1); FETCH(b, (j+q)-1);
			d = pdp[TOPLEFT(p, q, bw)] + (COMPARE(a, b) ? m : x);
			t = (q == -bw/2 && DIR(p) == DIR_LEFT)
				? SEA_CELL_MIN				/** when the top cell is out of the band */
				: (pdp[TOP(p, q, bw)] + g);
			l = (q == bw/2-1 && DIR(p) == DIR_TOP)
				? SEA_CELL_MIN				/** when the left cell is out of the band */
				: (pdp[LEFT(p, q, bw)] + g);
			*pdp++ = d = MAX4(d, t, l, min);
			if(i-q < amat && j+q < bmat) {
				if(ALG != NW && d >= max) {
					max = d; mi = i-q; mj = j+q; mp = p; mq = q;
				}
				if(ALG == XSEA) {
					xacc &= (d + xdrop - max);
				}
			}
		}
		if(ALG == XSEA && xacc < 0) { break; }
	}

	if(ALG == NW) {
		mi = alen; mj = blen; mp = COP(mi, mj, bw); mq = COQ(mi, mj, bw);
		aln->score = dpmat[ADDR(mp, mq, bw)];
	} else {
		if(max > (SEA_CELL_MAX - m)) { return SEA_ERROR_OVERFLOW; }
		if(max == 0) { mi = mj = 0; } 		/** wind back to (0, 0) if no meaningful score was found */
		aln->alen = mi;
		aln->blen = mj;
		aln->score = max;
	}

	END_BENCH(fill);						/** end of fill step */

	#ifdef DEBUG
	fprintf(stderr, "ldmat, %d, %d\n", alen+bw, blen+bw);
	for(i = 0; i < p; i++) {
		for(j = -bw/2; j < bw/2; j++) {
			fprintf(stderr, "%d, %d, %d, %d, %d\n", COX(i, j, bw), COY(i, j, bw), i, j, dpmat[ADDR(i, j, bw)]);
		}
	}
	fprintf(stderr, "\n");
	#endif

	if(aln->aln == NULL) { return SEA_SUCCESS; }

	START_BENCH(trace);						/** start of trace step */

	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);
	CLEAR_ALN(l, aln->aln, aln->len);
	pdp = dpmat+ADDR(mp, mq, bw);
	FETCH(a, mi-1); FETCH(b, mj-1);
	while(mi > 0 && mj > 0) {
		cell = *pdp; sc = COMPARE(a, b) ? m : x;
		if(cell == (pdp[TOPLEFT(mp, mq, bw)] + sc)) {
			pdp += TOPLEFT(mp, mq, bw); mp -= 2;
			mi--; FETCH(a, mi-1);
			mj--; FETCH(b, mj-1);
			PUSH(l, (sc == m) ? MATCH_CHAR : MISMATCH_CHAR);
			if(ALG == SW && cell <= sc) {
				aln->apos += mi; aln->bpos += mj;
				aln->alen -= mi; aln->blen -= mj;
				mi = 0; mj = 0; break;
			}
		} else if(cell == (pdp[LEFT(mp, mq, bw)] + g)) {
			pdp += LEFT(mp, mq, bw); mq += LEFTQ(mp, mq, bw); mp--;
			mi--; FETCH(a, mi-1); PUSH(l, DELETION_CHAR);
		} else if(cell == (pdp[TOP(mp, mq, bw)] + g)) {
			pdp += TOP(mp, mq, bw); mq += TOPQ(mp, mq, bw); mp--;
			mj--; FETCH(b, mj-1); PUSH(l, INSERTION_CHAR);
		} else {
			return SEA_ERROR_OUT_OF_BAND;
		}
	}
	while(mi > 0) { mi--; PUSH(l, DELETION_CHAR); }
	while(mj > 0) { mj--; PUSH(l, INSERTION_CHAR); }
	aln->len = LENGTH(l);
	REVERSE(l);

	END_BENCH(trace); 						/** end of trace step */
	return SEA_SUCCESS;
}

/**
 * @fn naive_linear_banded_matsize
 *
 * @brief returns the size of a matrix in bytes (linear-gap cost banded matrix).
 *
 * @param[in] alen : the length of the input sequence a (in base pairs).
 * @param[in] blen : the length of the input sequence b (in base pairs).
 * @param[in] bandwidth : the width of the band.
 *
 * @return the size of a matrix in bytes.
 */
sea_int_t 
DECLARE_FUNC_GLOBAL(naive_linear_banded_matsize, SUFFIX)(
	sea_int_t alen,
	sea_int_t blen,
	sea_int_t bandwidth)
{
	return(sizeof(sea_int_t) * (alen + blen + bandwidth + 2) * bandwidth);
}

/**
 * @fn main
 *
 * @brief gets two ascii strings from stdin, align strings with naive_affine_full, and print the result.
 */
#ifdef MAIN

int main(int argc, char *argv[])
{
	int matlen, alnlen;
	void *mat;
	struct sea_result aln;
	struct sea_params param;

	param.flags = 0;
	param.m = 2;
	param.x = -3;
	param.gi = -4;
	param.ge = -1;
	param.xdrop = 12;
	param.bandwidth = 32;

	aln.a = argv[1];
	aln.alen = strlen(aln.a);
	aln.apos = 0;
	aln.b = argv[2];
	aln.blen = strlen(aln.b);
	aln.bpos = 0;
	alnlen = aln.alen + aln.blen;
	aln.len = alnlen;

	alnlen = aln.alen + aln.blen;
	matlen = CALL_FUNC(naive_linear_banded_matsize, SUFFIX)(
		aln.alen, aln.blen, param.bandwidth);

	aln.aln = (void *)malloc(alnlen);
	mat = (void *)malloc(matlen);
	CALL_FUNC(naive_linear_banded, SUFFIX)(&aln, param, mat);

	printf("%d, %d, %s\n", aln.score, aln.len, aln.aln);

	free(mat);
	free(aln.aln);
	return 0;
}

#endif /* #ifdef MAIN */

/**
 * unittest functions.
 *
 * give a definition of TEST to a compiler, e.g. -DTEST=1, to make a library for tests.
 */
#ifdef TEST
#if SEQ == ascii && ALN == ascii

extern sea_int_t
DECLARE_FUNC_GLOBAL(naive_linear_full_matsize, SUFFIX)(
	sea_int_t alen,
	sea_int_t blen,
	sea_int_t bandwidth);

extern sea_int_t
DECLARE_FUNC_GLOBAL(naive_linear_full, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	void *mat);

/**
 * @fn test_1_naive_linear_banded
 *
 * @brief a unittest function of naive_linear_banded.
 *
 * @detail
 * This function is an aggregation of simple fixed ascii queries.
 * In this function, a sea_assert_align macro in tester.h is called. This macro
 * calls sea_align function with given context, checks if the score and the alignment
 * string is the same as the given score and string. If both or one of the results
 * are different, the macro prints the failure status (filename, lines, input sequences,
 * and the content of sea_result) and dumps a content of dynamic programming matrix
 * to dumps.log.
 */
void
DECLARE_FUNC_GLOBAL(test_1_naive_linear_banded, SUFFIX)(
	void)
{
	sea_int_t m = 2,
			  x = -3,
			  g = -4;
	struct sea_context *ctx;

	ctx = sea_init_fp(
		SEA_BANDWIDTH_64,
		CALL_FUNC(naive_linear_banded, SUFFIX),
		CALL_FUNC(naive_linear_banded_matsize, SUFFIX),
		m, x, g, 0,			/** the default blast scoring scheme */
		12);				/** xdrop threshold */

	/**
	 * when both sequences are empty
	 */
	sea_assert_align(ctx, "", 				"", 			0,			"");
	/**
	 * when one sequence is empty
	 */
	sea_assert_align(ctx, "AAA", 			"", 			0,			"");
	sea_assert_align(ctx, "TTTTTTT", 		"", 			0,			"");
	sea_assert_align(ctx, "", 				"AAA", 			0,			"");
	sea_assert_align(ctx, "", 				"TTTTTTT", 		0,			"");
	sea_assert_align(ctx, "CTAG",			"", 			0,			"");

	/**
	 * a match
	 */
	sea_assert_align(ctx, "A",				"A", 			m,			"M");
	sea_assert_align(ctx, "C", 				"C", 			m,			"M");
	sea_assert_align(ctx, "G", 				"G", 			m,			"M");
	sea_assert_align(ctx, "T", 				"T", 			m,			"M");

	/**
	 * a mismatch
	 */
	if(ALG == NW) {
		/**
		 * the Needleman-Wunsch algorithm
		 */
		sea_assert_align(ctx, "A", 				"C", 			x,			"X");
		sea_assert_align(ctx, "C", 				"A", 			x,			"X");
		sea_assert_align(ctx, "G", 				"T", 			x,			"X");
		sea_assert_align(ctx, "T", 				"A", 			x,			"X");
	} else if(ALG == SEA || ALG == SW || ALG == XSEA) {
		/**
		 * the Smith-Waterman algorithm and the seed-and-extend algorithm
		 */
		sea_assert_align(ctx, "A", 				"C", 			0,			"");
		sea_assert_align(ctx, "C", 				"A", 			0,			"");
		sea_assert_align(ctx, "G", 				"T", 			0,			"");
		sea_assert_align(ctx, "T", 				"A", 			0,			"");
	}

	/**
	 * homopolymers with different lengths.
	 */
	if(ALG == NW) {
		/**
		 * the Needleman-Wunsch algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			g+m,			"IM");
		sea_assert_align(ctx, "A", 				"AAA", 			g+g+m,			"IIM");
		sea_assert_align(ctx, "AAAA", 			"AA", 			g+g+m+m,		"DDMM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	4*g+4*m,		"IIIIMMMM");
	} else if(ALG == SEA || ALG == SW || ALG == XSEA) {
		/**
		 * the Smith-Waterman algorithm and the seed-and-extend algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			m,				"M");
		sea_assert_align(ctx, "A", 				"AAA", 			m,				"M");
		sea_assert_align(ctx, "AAAA", 			"AA", 			m+m,			"MM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	4*m,			"MMMM");
	}

	/**
	 * when mismatches occurs.
	 */
	sea_assert_align(ctx, "AAAAAAAA", 		"AAAATAAA", 	7*m+x,			"MMMMXMMM");
	sea_assert_align(ctx, "TTTTTTTT", 		"TTTCTTTT", 	7*m+x,			"MMMXMMMM");
	sea_assert_align(ctx, "CCCCCCCC", 		"CCTCCCCC", 	7*m+x,			"MMXMMMMM");
	sea_assert_align(ctx, "GGGGGGGG", 		"GGCGGTGG", 	6*m+2*x,		"MMXMMXMM");

	/**
	 * when gaps with a base occurs on seq a (insertion).
	 */
	sea_assert_align(ctx, "AAAAATTTT", 		"AAAAAGTTTT", 	9*m+g,			"MMMMMIMMMM");
	sea_assert_align(ctx, "TTTTCCCCC", 		"TTTTACCCCC", 	9*m+g,			"MMMMIMMMMM");
	sea_assert_align(ctx, "CCCGGGGGG", 		"CCCTGGGGGG", 	9*m+g,			"MMMIMMMMMM");
	sea_assert_align(ctx, "GGGAATTT", 		"GGGCAAGTTT", 	8*m+2*g,		"MMMIMMIMMM");

	/**
	 * when gaps with a base occurs on seq b (deletion).
	 */
	sea_assert_align(ctx, "AAAAAGTTTT", 	"AAAAATTTT", 	9*m+g,			"MMMMMDMMMM");
	sea_assert_align(ctx, "TTTTACCCCC", 	"TTTTCCCCC", 	9*m+g,			"MMMMDMMMMM");
	sea_assert_align(ctx, "CCCTGGGGGG", 	"CCCGGGGGG", 	9*m+g,			"MMMDMMMMMM");
	sea_assert_align(ctx, "GGGCAAGTTT", 	"GGGAATTT", 	8*m+2*g,		"MMMDMMDMMM");

	/**
	 * when a gap longer than two bases occurs on seq a.
	 */
	sea_assert_align(ctx, "AAAAATTTTT", 	"AAAAAGGTTTTT", 10*m+2*g,		"MMMMMIIMMMMM");
	sea_assert_align(ctx, "TTTAAAAGGG", 	"TTTCAAAACGGG", 10*m+2*g,		"MMMIMMMMIMMM");

	/**
	 * when a gap longer than two bases occurs on seq b.
	 */
	sea_assert_align(ctx, "AAAAAGGTTTTT",	"AAAAATTTTT", 	10*m+2*g,		"MMMMMDDMMMMM");
	sea_assert_align(ctx, "TTTCAAAACGGG", 	"TTTAAAAGGG", 	10*m+2*g,	 	"MMMDMMMMDMMM");

	/**
	 * when outer gaps occurs.
	 */
	if(ALG == NW) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m+4*x, 		"XXMMMMMMMMXX");
	} else if(ALG == SEA || ALG == XSEA) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m+2*x, 		"XXMMMMMMMM");
	} else if(ALG == SW) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m,	 		"MMMMMMMM");
	}

	/**
	 * X-drop test (here xdrop threshold is 12)
	 */
	if(ALG == XSEA) {
		sea_assert_align(ctx, "TTTTTAAA",		"GGGGGAAA",		0, 			"");
		sea_assert_align(ctx, "TTTTAAAA",		"GGGGAAAA",		0, 			"");
		sea_assert_align(ctx, "TTTAAAAA",		"GGGAAAAA",		0, 			"");
		sea_assert_align(ctx, "TTAAAAAA",		"GGAAAAAA",		6*m+2*x, 	"XXMMMMMM");

		sea_assert_align(ctx, "AAAATTTAAAAA",	"AAAAGGGAAAAA",	4*m, 		"MMMM");
		sea_assert_align(ctx, "AAAATTAAAAAA",	"AAAAGGAAAAAA",	10*m+2*x, 	"MMMMXXMMMMMM");
	}

	sea_clean(ctx);
	return;
}

/**
 * @fn test_7_cross_naive_linear_banded
 *
 * @brief cross test between naive_linear_full and naive_linear_banded
 */
#if ALG != XSEA
#if HAVE_NAIVE_FULL

void
DECLARE_FUNC_GLOBAL(test_7_cross_naive_linear_banded, SUFFIX)(
	void)
{
	int i;
	int const cnt = 5,
			  len = 10000;
	sea_int_t m = 2,
			  x = -3,
			  g = -4;
	char *a, *b;
	struct sea_context *full, *band;

	full = sea_init_fp(
		0,
		CALL_FUNC(naive_linear_full, SUFFIX),
		CALL_FUNC(naive_linear_full_matsize, SUFFIX),
		m, x, g, 0,
		10000);
	band = sea_init_fp(
		SEA_BANDWIDTH_64,
		CALL_FUNC(naive_linear_banded, SUFFIX),
		CALL_FUNC(naive_linear_banded_matsize, SUFFIX),
		m, x, g, 0,
		10000);


	for(i = 0; i < cnt; i++) {
		a = rseq(len);
		b = mseq(a, 20, 100, 100);
		sea_assert_cross(
			full, band, a, b);
		free(a); free(b);
	}

	sea_clean(full);
	sea_clean(band);
	return;
}

#endif /* #if HAVE_NAIVE_FULL */
#endif /* #if ALG != XSEA */

#endif	/* #if SEQ == ascii && ALN == ascii */
#endif	/* #ifdef TEST */

/**
 * end of naive_linear_banded.c
 */
