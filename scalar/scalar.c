
/**
 * @file scalar.c
 *
 * @brief an implementation of matsize functions and get_sea function, called from sea_align in sea.c.
 *
 * @sa sea.c
 */
#include <stdlib.h>
#include "scalar.h"
#include "../include/sea.h"
#include "../util/util.h"
#include "../build/config.h"

#if HAVE_NAIVE_FULL
	#include "naive_linear_full_decl.h"
	#include "naive_linear_full_table.h"
	#include "naive_affine_full_decl.h"
	#include "naive_affine_full_table.h"
#endif

#if HAVE_NAIVE_BANDED
	#include "naive_linear_banded_decl.h"
	#include "naive_linear_banded_table.h"
	#include "naive_affine_banded_decl.h"
	#include "naive_affine_banded_table.h"
#endif

#if HAVE_NAIVE_DYNAMIC_BANDED
	#include "naive_linear_dynamic_banded_decl.h"
	#include "naive_linear_dynamic_banded_table.h"
	#include "naive_affine_dynamic_banded_decl.h"
	#include "naive_affine_dynamic_banded_table.h"
#endif

/**
 * @fn scalar_get_sea
 *
 * @brief retrieve a pointer to alignment function.
 *
 * @param[in] flags : option flag bitfield. see parameter of sea_init in sea.h
 * @param[in] len : the maximum length of the query sequence. (unused in this function.)
 * @param[in] alg_type : an algorithm index. see table.th and sea.h.
 * @param[in] seq_type : an input sequence type index. see table.th and sea.h.
 * @param[in] aln_type : an output alignment type index. see table.th and sea.h.
 * @param[in] scmat : the scores in the dynamic programming. scmat[0] = M, scmat[1] = X, scmat[2] = Gi, and scmat[3] = Ge. see sea.h.
 * @param[in] xdrop : the xdrop threshold in positive integer.
 * @param[out] sea_matsize : a pointer to a function which returns the size of a matrix.
 * @param[out] sea_sea : a pointer to a function which do an alignment.
 *
 * @return SEA_SUCCESS if proper pointer is set, SEA_ERROR_UNSUPPORTED_ALG if not.
 */
sea_int_t scalar_get_sea(
	struct sea_funcs *fp,
	struct sea_params param,
	sea_int_t len,
	sea_int_t alg_type, sea_int_t seq_type, sea_int_t aln_type)
{
	switch(param.flags & (SEA_FLAGS_MASK_DP | SEA_FLAGS_MASK_COST)) {
	#if HAVE_NAIVE_FULL
		case SEA_FULL_DP | SEA_LINEAR_GAP_COST:
			*fp = naive_linear_full_table[alg_type][seq_type][aln_type];
			return SEA_SUCCESS;
		case SEA_FULL_DP | SEA_AFFINE_GAP_COST:
			*fp = naive_affine_full_table[alg_type][seq_type][aln_type];
			return SEA_SUCCESS;
	#endif

	#if HAVE_NAIVE_BANDED
		case SEA_BANDED_DP | SEA_LINEAR_GAP_COST:
			*fp = naive_linear_banded_table[alg_type][seq_type][aln_type];
			return SEA_SUCCESS;
		case SEA_BANDED_DP | SEA_AFFINE_GAP_COST:
			*fp = naive_affine_banded_table[alg_type][seq_type][aln_type];
			return SEA_SUCCESS;
	#endif

	#if HAVE_NAIVE_DYNAMIC_BANDED
		case SEA_DYNAMIC_BANDED_DP | SEA_LINEAR_GAP_COST:
			*fp = naive_linear_dynamic_banded_table[alg_type][seq_type][aln_type];
			return SEA_SUCCESS;
		case SEA_DYNAMIC_BANDED_DP | SEA_AFFINE_GAP_COST:
			*fp = naive_affine_dynamic_banded_table[alg_type][seq_type][aln_type];
			return SEA_SUCCESS;
	#endif
		default:
			fp->_sea_matsize = NULL;
			fp->_sea_sea = NULL;
			return SEA_ERROR_UNSUPPORTED_ALG;
	}
	return SEA_ERROR_UNSUPPORTED_ALG;
}

/**
 * end of scalar.c
 */
