
/**
 * @file naive_linear_full.c
 *
 * @brief the simplest implementation of the linear-gap cost full DP.
 *
 * @detail
 * This file implements the linear-gap cost dynamic programming algorithm.
 * The implementatoin serves as the reference implementation of the linear-gap cost,
 * full DP algorithm in the other implementations of the linear-gap cost algorithms.
 */
#ifdef DEBUG
	#include <stdio.h>
#endif
#ifdef MAIN
	#include <stdio.h>
	#include <string.h>
#endif
#ifdef TEST
 	#include "../util/tester.h"
#endif

#include <stdlib.h>						/* for definition of the NULL */
#include "../include/sea.h"				/* for API definitions, flag definitions, and structs */
#include "../util/util.h"				/* for internal utility functions */
#include "../seqreader/seqreader.h"		/* for input sequence reader and output alignment writer */
#include "../util/bench.h"				/* for benchmarking */

#define	ADDR(x, y, len)			( (x)+(len)*(y) )

/**
 * in the full-size matrix implementation, the X-drop termination is not
 * implemented for now. the XSEA algorithm (the seed-and-extend alignment
 * algorithm with the X-drop termination) is interpreted as the SEA algorithm
 * (the seed-and-extend alignment algorithm WITHOUT X-drop termination.)
 */
#if ALG == XSEA
 	#undef  ALG
 	#define ALG 	SEA
#endif

/**
 * @fn naive_linear_full
 *
 * @brief a non-SIMD linear-gap cost full (rectangular) dynamic programming function. 
 *
 * @param[ref] aln : a pointer to sea_result structure. aln must NOT be NULL. (a structure which contains an alignment result.)
 * @param[in] aln->a : a pointer to query sequence a.
 * @param[inout] aln->apos : (input) the start position of the search section on sequence a. (0 <= apos < length(sequence a)) (output) the start position of the alignment.
 * @param[inout] aln->alen : (input) the length of the search section on sequence a. (0 < alen) (output) the length of the alignment.
 * @param[in] aln->b : a pointer to query sequence b.
 * @param[inout] aln->bpos : same as apos.
 * @param[inout] aln->blen : same as alen.
 *
 * @param[out] aln->aln : a pointer to the alignment string.
 * @param[inout] aln->len : (input) the reserved length of the aln->aln. (output) the length of the alignment string.
 * @param[out] aln->score : an alignment score.
 *
 * @param[in] scmat : a pointer to the DP cost array. see also: sea_context, sea_init.
 * @param[in] xdrop : the xdrop threshold.
 * @param[in] bandwidth : the band width of the DP array.
 * @param[in] mat : a pointer to the DP matrix.
 * @param[in] matsize : the size of the DP matrix.
 *
 * @return status. zero means success. see sea.h for the other error code.
 *
 * @detail
 * this function implements the linear-gap cost, full dynamic programming (DP) algorithms.
 * this one function serves as the common three alignment algorithms,
 * the Smith-Waterman (local), the Needleman-Wunsch (global), and the seed-and-extend alignment.
 * the selection of the algorithm is done by giving proper definitions under compilation,
 * or giving flag such as -DALG=SW or equivalent to the compiler yields an object
 * which contains the Smith-Waterman alginment algorithm.
 *
 * the following define macros MUST BE given to the compiler to generate correct objects.
 *     ALG : algorithm switch. see wscript for more details.
 *     SEQ : input sequence format. see seqreader for more details.
 *     ALN : output alignment format. see seqreader for more details.
 */
sea_int_t
DECLARE_FUNC_GLOBAL(naive_linear_full, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	void *mat)
{
	/**
	 * a composition of the matrix:
	 *
	 * the cells in the matrix are pointed by (i, j) coordinates. i-coordinate
	 * corresponds to the index of input sequence a, and j-coordinate to sequence b.
	 * the origin of the dpmat array is the cell at (0, 0).
	 * the width (or the length of a row of the matrix) and the height (length of a column)
	 * are denoted as amat and bmat, respectively.
	 * the illustraion of the matrix is following:
	 *
	 *            input sequence a
	 *                A  T  A  C  C ..
	 *          O  0  1  2  3  4  5 ..      i
	 *           +--------------------------->
	 *          0| a| b|  |  |  |  |  |  | ..
	 *           |--+--+--+--+--+--+--+--+
	 * seq.b  A 1|  | c|  |  |  |  |  |  |
	 *           |--+--+--+--+--+--+--+--+
	 *        T 2|  |  |  |  |  |  |  |  |
	 *           |--+--+--+--+--+--+--+--+
	 *        C 3|  |  |  |  |  |  |  |  |
	 *           |--+--+--+--+--+--+--+--+
	 *        C 4|  |  |  |  |  |  |  |  |
	 *           |--+--+--+--+--+--+--+--+
	 *        : :|  |  |  |  |  |  |  |  |
	 *           |--+--+--+--+--+--+--+--+
	 *         j v
	 *
	 * a : the cell at top-left corner of the matrix. (i, j) = (0, 0), dpmat[0].
	 * b : (i, j) = (1, 0), dpmat[1].
	 * c : (i, j) = (1, 1), dpmat[1 * amat + 1]
	 */
	sea_int_t i, j, mi = 0, mj = 0;
	sea_int_t t, l, d = 0, max = 0;
	sea_int_t cell, sc;
	sea_cell_t const min = (ALG == SW) ? 0 : SEA_CELL_MIN;		/** the lower limit of the scores */

	DECLARE_SEQ(a);
	DECLARE_SEQ(b);
	DECLARE_ALN(l);
	DECLARE_BENCH(fill);
	DECLARE_BENCH(search);
	DECLARE_BENCH(trace);
	sea_int_t	m = param.m,
				x = param.x,
				g = param.gi;
	sea_int_t	amat = aln->alen+1,
				bmat = aln->blen+1;
	sea_cell_t	*dpmat = (sea_cell_t *)mat;

	START_BENCH(fill);		/** start of fill step */

	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);

	if(ALG == SW) {
		for(i = 0; i < amat; i++) { dpmat[ADDR(i, 0, amat)] = 0; }
		for(j = 0; j < bmat; j++) { dpmat[ADDR(0, j, amat)] = 0; }
	} else {
		for(i = 0; i < amat; i++) { dpmat[ADDR(i, 0, amat)] = i * g; }
		for(j = 0; j < bmat; j++) { dpmat[ADDR(0, j, amat)] = j * g; }
	}

	for(j = 1; j < bmat; j++) {
		FETCH(b, j-1);
		for(i = 1; i < amat; i++) {
			FETCH(a, i-1);
			d = dpmat[ADDR(i-1, j-1, amat)] + (COMPARE(a, b) ? m : x);
			t = dpmat[ADDR(i, j-1, amat)] + g;
			l = dpmat[ADDR(i-1, j, amat)] + g;
			d = MAX4(d, t, l, min);
			dpmat[ADDR(i, j, amat)] = d;
			if(ALG != NW && d >= max) { max = d; mi = i; mj = j; }
		}
	}
	if(ALG == NW) {
		mi = aln->alen; mj = aln->blen;
		aln->score = d;
	} else {
		if(max > (SEA_CELL_MAX - m)) { return SEA_ERROR_OVERFLOW; }
		if(max == 0) { mi = mj = 0; } 		/** wind back to (0, 0) if no meaningful score was found */
		aln->alen = mi;
		aln->blen = mj;
		aln->score = max;
	}

	END_BENCH(fill);		/** end of fill step */

	#ifdef DEBUG
	fprintf(stderr, "lfmat, %d, %d\n", amat, bmat);
	for(j = 0; j < bmat; j++) {
		for(i = 0; i < amat; i++) {
			fprintf(stderr, "%d, %d, %d\n", i, j, dpmat[ADDR(i, j, amat)]);
		}
	}
	fprintf(stderr, "\n");
	#endif

	if(aln->aln == NULL) {
		return SEA_SUCCESS;
	}

	START_BENCH(trace);		/** start of trace step */

	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);
	CLEAR_ALN(l, aln->aln, aln->len);
	FETCH(a, mi-1); FETCH(b, mj-1);
	while(mi > 0 && mj > 0) {
		sc = COMPARE(a, b) ? m : x;
		cell = dpmat[ADDR(mi, mj, amat)];
		if(cell == (dpmat[ADDR(mi-1, mj-1, amat)] + sc)) {
			mi--; FETCH(a, mi-1);
			mj--; FETCH(b, mj-1);
			PUSH(l, (sc == m) ? MATCH_CHAR : MISMATCH_CHAR);
			if(ALG == SW && cell <= sc) {
				aln->apos += mi; aln->bpos += mj;
				aln->alen -= mi; aln->blen -= mj;
				mi = mj = 0;
				break;
			}
		} else if(cell == (dpmat[ADDR(mi-1, mj, amat)] + g)) {
			mi--; FETCH(a, mi-1); PUSH(l, DELETION_CHAR);
		} else if(cell == (dpmat[ADDR(mi, mj-1, amat)] + g)) {
			mj--; FETCH(b, mj-1); PUSH(l, INSERTION_CHAR);
		} else {
			return SEA_ERROR_OUT_OF_BAND;
		}
	}
	while(mi > 0) { mi--; PUSH(l, DELETION_CHAR); }
	while(mj > 0) { mj--; PUSH(l, INSERTION_CHAR); }
	aln->len = LENGTH(l);
	REVERSE(l);

	END_BENCH(trace);		/** end of trace step */
	return SEA_SUCCESS;
}

/**
 * @fn naive_linear_full_matsize
 *
 * @brief returns the size of a matrix in bytes (for linear-gap cost full size matrix).
 *
 * @param[in] alen : the length of the input sequence a (in base pairs).
 * @param[in] blen : the length of the input sequence b (in base pairs).
 * @param[in] bandwidth : unused. give zero for practice.
 *
 * @return the size of a matrix in bytes.
 */
sea_int_t
DECLARE_FUNC_GLOBAL(naive_linear_full_matsize, SUFFIX)(
	sea_int_t alen,
	sea_int_t blen,
	sea_int_t bandwidth)
{
	return(sizeof(sea_int_t) * (alen+1) * (blen+1));
}

/**
 * @fn main
 *
 * @brief gets two ascii strings from stdin, align strings with naive_affine_full, and print the result.
 */
#ifdef MAIN

int main(int argc, char *argv[])
{
	int matlen, alnlen;
	void *mat;
	struct sea_result aln;
	struct sea_params param;

	param.flags = 0;
	param.m = 2;
	param.x = -3;
	param.gi = -4;
	param.ge = -1;
	param.xdrop = 12;
	param.bandwidth = 32;

	aln.a = argv[1];
	aln.alen = strlen(aln.a);
	aln.apos = 0;
	aln.b = argv[2];
	aln.blen = strlen(aln.b);
	aln.bpos = 0;
	alnlen = aln.alen + aln.blen;
	aln.len = alnlen;

	alnlen = aln.alen + aln.blen;
	matlen = CALL_FUNC(naive_linear_full_matsize, SUFFIX)(
		aln.alen, aln.blen, param.bandwidth);

	aln.aln = (void *)malloc(alnlen);
	mat = (void *)malloc(matlen);
	CALL_FUNC(naive_linear_full, SUFFIX)(&aln, param, mat);

	printf("%d, %d, %s\n", aln.score, aln.len, aln.aln);

	free(mat);
	free(aln.aln);
	return 0;
}

#endif /* #ifdef MAIN */

/**
 * unittest functions.
 *
 * give a definition of TEST to a compiler, e.g. -DTEST=1, to make a library for tests.
 */
#ifdef TEST
#if SEQ == ascii && ALN == ascii

/**
 * @fn test_0_naive_linear_full
 *
 * @brief a unittest function of naive_linear_full.
 *
 * @detail
 * This function is an aggregation of simple fixed ascii queries.
 * In this function, a sea_assert_align macro in tester.h is called. This macro
 * calls sea_align function with given context, checks if the score and the alignment
 * string is the same as the given score and string. If both or one of the results
 * are different, the macro prints the failure status (filename, lines, input sequences,
 * and the content of sea_result) and dumps a content of dynamic programming matrix
 * to dumps.log.
 */
void
DECLARE_FUNC_GLOBAL(test_0_naive_linear_full, SUFFIX)(
	void)
{
	sea_int_t m = 2,
			  x = -3,
			  g = -4;
	struct sea_context *ctx;

	ctx = sea_init_fp(
		0,
		CALL_FUNC(naive_linear_full, SUFFIX),
		CALL_FUNC(naive_linear_full_matsize, SUFFIX),
		m, x, g, 0,			/** the default blast scoring scheme */
		10000);				/** xdrop threshold */

	/**
	 * when both sequences are empty
	 */
	sea_assert_align(ctx, "", 				"", 			0,			"");
	/**
	 * when one sequence is empty
	 */
	sea_assert_align(ctx, "AAA", 			"", 			0,			"");
	sea_assert_align(ctx, "TTTTTTT", 		"", 			0,			"");
	sea_assert_align(ctx, "", 				"AAA", 			0,			"");
	sea_assert_align(ctx, "", 				"TTTTTTT", 		0,			"");
	sea_assert_align(ctx, "CTAG",			"", 			0,			"");

	/**
	 * a match
	 */
	sea_assert_align(ctx, "A",				"A", 			m,			"M");
	sea_assert_align(ctx, "C", 				"C", 			m,			"M");
	sea_assert_align(ctx, "G", 				"G", 			m,			"M");
	sea_assert_align(ctx, "T", 				"T", 			m,			"M");

	/**
	 * a mismatch
	 */
	if(ALG == NW) {
		/**
		 * the Needleman-Wunsch algorithm
		 */
		sea_assert_align(ctx, "A", 				"C", 			x,			"X");
		sea_assert_align(ctx, "C", 				"A", 			x,			"X");
		sea_assert_align(ctx, "G", 				"T", 			x,			"X");
		sea_assert_align(ctx, "T", 				"A", 			x,			"X");
	} else if(ALG == SEA || ALG == SW) {
		/**
		 * the Smith-Waterman algorithm and the seed-and-extend algorithm
		 */
		sea_assert_align(ctx, "A", 				"C", 			0,			"");
		sea_assert_align(ctx, "C", 				"A", 			0,			"");
		sea_assert_align(ctx, "G", 				"T", 			0,			"");
		sea_assert_align(ctx, "T", 				"A", 			0,			"");
	}

	/**
	 * homopolymers with different lengths.
	 */
	if(ALG == NW) {
		/**
		 * the Needleman-Wunsch algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			g+m,			"IM");
		sea_assert_align(ctx, "A", 				"AAA", 			g+g+m,			"IIM");
		sea_assert_align(ctx, "AAAA", 			"AA", 			g+g+m+m,		"DDMM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	4*g+4*m,		"IIIIMMMM");
	} else if(ALG == SEA || ALG == SW) {
		/**
		 * the Smith-Waterman algorithm and the seed-and-extend algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			m,				"M");
		sea_assert_align(ctx, "A", 				"AAA", 			m,				"M");
		sea_assert_align(ctx, "AAAA", 			"AA", 			m+m,			"MM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	4*m,			"MMMM");
	}

	/**
	 * when mismatches occurs.
	 */
	sea_assert_align(ctx, "AAAAAAAA", 		"AAAATAAA", 	7*m+x,			"MMMMXMMM");
	sea_assert_align(ctx, "TTTTTTTT", 		"TTTCTTTT", 	7*m+x,			"MMMXMMMM");
	sea_assert_align(ctx, "CCCCCCCC", 		"CCTCCCCC", 	7*m+x,			"MMXMMMMM");
	sea_assert_align(ctx, "GGGGGGGG", 		"GGCGGTGG", 	6*m+2*x,		"MMXMMXMM");

	/**
	 * when gaps with a base occurs on seq a (insertion).
	 */
	sea_assert_align(ctx, "AAAAATTTT", 		"AAAAAGTTTT", 	9*m+g,			"MMMMMIMMMM");
	sea_assert_align(ctx, "TTTTCCCCC", 		"TTTTACCCCC", 	9*m+g,			"MMMMIMMMMM");
	sea_assert_align(ctx, "CCCGGGGGG", 		"CCCTGGGGGG", 	9*m+g,			"MMMIMMMMMM");
	sea_assert_align(ctx, "GGGAATTT", 		"GGGCAAGTTT", 	8*m+2*g,		"MMMIMMIMMM");

	/**
	 * when gaps with a base occurs on seq b (deletion).
	 */
	sea_assert_align(ctx, "AAAAAGTTTT", 	"AAAAATTTT", 	9*m+g,			"MMMMMDMMMM");
	sea_assert_align(ctx, "TTTTACCCCC", 	"TTTTCCCCC", 	9*m+g,			"MMMMDMMMMM");
	sea_assert_align(ctx, "CCCTGGGGGG", 	"CCCGGGGGG", 	9*m+g,			"MMMDMMMMMM");
	sea_assert_align(ctx, "GGGCAAGTTT", 	"GGGAATTT", 	8*m+2*g,		"MMMDMMDMMM");

	/**
	 * when a gap longer than two bases occurs on seq a.
	 */
	sea_assert_align(ctx, "AAAAATTTTT", 	"AAAAAGGTTTTT", 10*m+2*g,		"MMMMMIIMMMMM");
	sea_assert_align(ctx, "TTTAAAAGGG", 	"TTTCAAAACGGG", 10*m+2*g,		"MMMIMMMMIMMM");

	/**
	 * when a gap longer than two bases occurs on seq b.
	 */
	sea_assert_align(ctx, "AAAAAGGTTTTT",	"AAAAATTTTT", 	10*m+2*g,		"MMMMMDDMMMMM");
	sea_assert_align(ctx, "TTTCAAAACGGG", 	"TTTAAAAGGG", 	10*m+2*g,	 	"MMMDMMMMDMMM");

	/**
	 * when outer gaps occurs.
	 */
	if(ALG == NW) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m+4*x, 		"XXMMMMMMMMXX");
	} else if(ALG == SEA) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m+2*x, 		"XXMMMMMMMM");
	} else if(ALG == SW) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m,	 		"MMMMMMMM");
	}

	sea_clean(ctx);
	return;
}

#endif	/* #if SEQ == ascii && ALN == ascii */
#endif	/* #ifdef TEST */

/**
 * end of naive_linear_full.c
 */
