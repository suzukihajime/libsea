
/**
 * @file naive_affine_full.c
 *
 * @brief the simplest implementation of the affine-gap cost full DP (DP with triple-layered rectangular matrix).
 *
 * @detail
 * This file implements the Gotoh's affine-gap cost (triple-layered) algorithm.
 * The implementatoin serves as the reference implementation of the affine-gap cost, full DP algorithm
 * in the other implementations of the affine-gap cost algorithms.
 */
#ifdef DEBUG
	#include <stdio.h>
#endif
#ifdef MAIN
	#include <stdio.h>
	#include <string.h>
#endif
#ifdef TEST
 	#include "../util/tester.h"
#endif

#include <stdlib.h>						/* for definition of the NULL */
#include "../include/sea.h"				/* for API definitions, flag definitions, and structs */
#include "../util/util.h"				/* for internal utility functions */
#include "../seqreader/seqreader.h"		/* for input sequence reader and output alignment writer */
#include "../util/bench.h"				/* for benchmarking */

#define	ADDR(x, y, len)			( (x)+(len)*(y) )

/**
 * in the full-size matrix implementation, the X-drop termination is not
 * implemented for now. the XSEA algorithm (the seed-and-extend alignment
 * algorithm with the X-drop termination) is interpreted as the SEA algorithm
 * (the seed-and-extend alignment algorithm WITHOUT X-drop termination.)
 */
#if ALG == XSEA
 	#undef  ALG
 	#define ALG 	SEA
#endif

/**
 * @fn naive_linear_full
 *
 * @brief a non-SIMD affine-gap cost full (rectangular) dynamic programming function. 
 *
 * @param[ref] aln : a pointer to sea_result structure. aln must NOT be NULL. (a structure which contains an alignment result.)
 * @param[in] aln->a : a pointer to query sequence a.
 * @param[inout] aln->apos : (input) the start position of the search section on sequence a. (0 <= apos < length(sequence a)) (output) the start position of the alignment.
 * @param[inout] aln->alen : (input) the length of the search section on sequence a. (0 < alen) (output) the length of the alignment.
 * @param[in] aln->b : a pointer to query sequence b.
 * @param[inout] aln->bpos : same as apos.
 * @param[inout] aln->blen : same as alen.
 *
 * @param[out] aln->aln : a pointer to the alignment string.
 * @param[inout] aln->len : (input) the reserved length of the aln->aln. (output) the length of the alignment string.
 * @param[out] aln->score : an alignment score.
 *
 * @param[in] scmat : a pointer to the DP cost array. see also: sea_context, sea_init.
 * @param[in] xdrop : the xdrop threshold.
 * @param[in] bandwidth : the band width of the DP array.
 * @param[in] mat : a pointer to the DP matrix.
 * @param[in] matsize : the size of the DP matrix.
 *
 * @return status. zero means success. see sea.h for the other error code.
 *
 * @detail
 * this function implements the linear-gap cost, full dynamic programming (DP) algorithms.
 * this one function serves as the common three alignment algorithms,
 * the Smith-Waterman (local), the Needleman-Wunsch (global), and the seed-and-extend alignment.
 * the selection of the algorithm is done by giving proper definitions under compilation,
 * or giving flag such as -DALG=SW or equivalent to the compiler yields an object
 * which contains the Smith-Waterman alginment algorithm.
 *
 * the following define macros MUST BE given to the compiler to generate correct objects.
 *     ALG : algorithm switch. see wscript for more details.
 *     SEQ : input sequence format. see seqreader for more details.
 *     ALN : output alignment format. see seqreader for more details.
 */
sea_int_t
DECLARE_FUNC_GLOBAL(naive_affine_full, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	void *mat)
{
	/**
	 * a composition of the matrix: see naive_linear_full.c
	 */
	sea_int_t i, j, mi = 0, mj = 0;
	sea_int_t ee, ei, fe, fi, h = 0, max = 0;
	sea_int_t cell, sc;
	sea_cell_t const min = (ALG == SW) ? 0 : SEA_CELL_MIN;		/** the lower limit of the scores */

	DECLARE_SEQ(a);
	DECLARE_SEQ(b);
	DECLARE_ALN(l);
	DECLARE_BENCH(fill);
	DECLARE_BENCH(search);
	DECLARE_BENCH(trace);
	sea_int_t	m = param.m,
				x = param.x,
				gi = param.gi,
				ge = param.ge;
	sea_int_t	amat = aln->alen+1,
				bmat = aln->blen+1;
	sea_cell_t	*dpmat = (sea_cell_t *)mat;
	sea_cell_t 	*emat = dpmat+ADDR(0, bmat, amat),
				*fmat = dpmat+ADDR(0, 2*bmat, amat);

	START_BENCH(fill);		/** start of fill step */

	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);

	dpmat[ADDR(0, 0, amat)] = 0;
	if(ALG == SW) {
		for(i = 1; i < amat; i++) {
			dpmat[ADDR(i, 0, amat)] = fmat[ADDR(i, 0, amat)] = 0;
		}
		for(j = 1; j < bmat; j++) {
			dpmat[ADDR(0, j, amat)] = emat[ADDR(0, j, amat)] = 0;
		}
	} else {
		for(i = 1; i < amat; i++) {
			dpmat[ADDR(i, 0, amat)] = gi + (i-1) * ge;
			fmat[ADDR(i, 0, amat)] = gi + i * ge;
		}
		for(j = 1; j < bmat; j++) {
			dpmat[ADDR(0, j, amat)] = gi + (j-1) * ge;
			emat[ADDR(0, j, amat)] = gi + j * ge;
		}
	}

	for(j = 1; j < bmat; j++) {
		FETCH(b, j-1);
		for(i = 1; i < amat; i++) {
			FETCH(a, i-1);
			ee = emat[ADDR(i-1, j, amat)] + ge;
			ei = dpmat[ADDR(i-1, j, amat)] + gi;
			ee = MAX2(ee, ei);
			emat[ADDR(i, j, amat)] = ee;

			fe = fmat[ADDR(i, j-1, amat)] + ge;
			fi = dpmat[ADDR(i, j-1, amat)] + gi;
			fe = MAX2(fe, fi);
			fmat[ADDR(i, j, amat)] = fe;

			h = dpmat[ADDR(i-1, j-1, amat)] + (COMPARE(a, b) ? m : x);
			h = MAX4(ee, fe, h, min);
			dpmat[ADDR(i, j, amat)] = h;

			if(ALG != NW && h >= max) { max = h; mi = i; mj = j; }
		}
	}
	if(ALG == NW) {
		mi = aln->alen; mj = aln->blen;
		aln->score = h;
	} else {
		if(max > (SEA_CELL_MAX - m)) { return SEA_ERROR_OVERFLOW; }
		if(max == 0) { mi = mj = 0; } 		/** wind back to (0, 0) if no meaningful score was found */
		aln->alen = mi;
		aln->blen = mj;
		aln->score = max;
	}

	END_BENCH(fill);		/** end of fill step */

	#ifdef DEBUG
	fprintf(stderr, "afmat, %d, %d\n", amat, bmat);
	for(j = 0; j < bmat; j++) {
		for(i = 0; i < amat; i++) {
			fprintf(stderr, "%d, %d, %d, %d, %d\n", i, j,
				dpmat[ADDR(i, j, amat)], fmat[ADDR(i, j, amat)], emat[ADDR(i, j, amat)]);
		}
	}
	fprintf(stderr, "\n");
	#endif

	if(aln->aln == NULL) { return SEA_SUCCESS; }

	START_BENCH(trace);		/** start of trace step */

	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);
	CLEAR_ALN(l, aln->aln, aln->len);
	FETCH(a, mi-1); FETCH(b, mj-1);
	while(mi > 0 && mj > 0) {
		sc = COMPARE(a, b) ? m : x;
		cell = dpmat[ADDR(mi, mj, amat)];
		if(cell == (dpmat[ADDR(mi-1, mj-1, amat)] + sc)) {
			mi--; FETCH(a, mi-1);
			mj--; FETCH(b, mj-1);
			PUSH(l, (sc == m) ? MATCH_CHAR : MISMATCH_CHAR);
			if(ALG == SW && cell <= sc) {
				aln->apos += mi; aln->bpos += mj;
				aln->alen -= mi; aln->blen -= mj;
				mi = mj = 0;
				break;
			}
		} else if(cell == emat[ADDR(mi, mj, amat)]) {
			while(mi > 0 && emat[ADDR(mi, mj, amat)] == emat[ADDR(mi-1, mj, amat)] + ge) {
				mi--; PUSH(l, DELETION_CHAR);
			}
			mi--; PUSH(l, DELETION_CHAR);
			FETCH(a, mi-1);
		} else if(cell == fmat[ADDR(mi, mj, amat)]) {
			while(mj > 0 && fmat[ADDR(mi, mj, amat)] == fmat[ADDR(mi, mj-1, amat)] + ge) {
				mj--; PUSH(l, INSERTION_CHAR);
			}
			mj--; PUSH(l, INSERTION_CHAR);
			FETCH(b, mj-1);
		} else {
			return SEA_ERROR_OUT_OF_BAND;
		}
	}
	while(mi > 0) { mi--; PUSH(l, DELETION_CHAR); }
	while(mj > 0) { mj--; PUSH(l, INSERTION_CHAR); }
	aln->len = LENGTH(l);
	REVERSE(l);

	END_BENCH(trace);		/** end of trace step */
	return SEA_SUCCESS;
}

/**
 * @fn naive_affine_full_matsize
 *
 * @brief returns the size of a matrix in bytes (affine-gap cost full size matrix).
 *
 * @params the same as 'naive_linear_full_matsize'. see above.
 */
sea_int_t 
DECLARE_FUNC_GLOBAL(naive_affine_full_matsize, SUFFIX)(
	sea_int_t alen,
	sea_int_t blen,
	sea_int_t bandwidth)
{
	return(sizeof(sea_int_t) * 3 * (alen+1) * (blen+1));
}

/**
 * @fn main
 *
 * @brief gets two ascii strings from stdin, align strings with naive_affine_full, and print the result.
 */
#ifdef MAIN

int main(int argc, char *argv[])
{
	int matlen, alnlen;
	void *mat;
	struct sea_result aln;
	struct sea_params param;

	param.flags = 0;
	param.m = 2;
	param.x = -3;
	param.gi = -4;
	param.ge = -1;
	param.xdrop = 12;
	param.bandwidth = 32;

	aln.a = argv[1];
	aln.alen = strlen(aln.a);
	aln.apos = 0;
	aln.b = argv[2];
	aln.blen = strlen(aln.b);
	aln.bpos = 0;
	alnlen = aln.alen + aln.blen;
	aln.len = alnlen;

	alnlen = aln.alen + aln.blen;
	matlen = CALL_FUNC(naive_affine_full_matsize, SUFFIX)(
		aln.alen, aln.blen, param.bandwidth);

	aln.aln = (void *)malloc(alnlen);
	mat = (void *)malloc(matlen);
	CALL_FUNC(naive_affine_full, SUFFIX)(&aln, param, mat);

	printf("%d, %d, %s\n", aln.score, aln.len, aln.aln);

	free(mat);
	free(aln.aln);
	return 0;
}

#endif

/**
 * unittest functions.
 *
 * give a definition of TEST to a compiler, e.g. -DTEST=1, to make a library for tests.
 */
#ifdef TEST
#if SEQ == ascii && ALN == ascii

/**
 * @fn test_0_naive_affine_full
 *
 * @brief a unittest function of naive_affine_full.
 *
 * @detail
 * This function is an aggregation of simple fixed ascii queries.
 * In this function, a sea_assert_align macro in tester.h is called. This macro
 * calls sea_align function with given context, checks if the score and the alignment
 * string is the same as the given score and string. If both or one of the results
 * are different, the macro prints the failure status (filename, lines, input sequences,
 * and the content of sea_result) and dumps a content of dynamic programming matrix
 * to dumps.log.
 */
void
DECLARE_FUNC_GLOBAL(test_0_naive_affine_full, SUFFIX)(
	void)
{
	sea_int_t m = 2,
			  x = -3,
			  gi = -4,
			  ge = -1;
	struct sea_context *ctx;

	ctx = sea_init_fp(
		0,
		CALL_FUNC(naive_affine_full, SUFFIX),
		CALL_FUNC(naive_affine_full_matsize, SUFFIX),
		m, x, gi, ge,		/** the default blast scoring scheme */
		10000);				/** xdrop threshold */

	/**
	 * when both sequences are empty
	 */
	sea_assert_align(ctx, "", 				"", 			0,			"");
	/**
	 * when one sequence is empty
	 */
	sea_assert_align(ctx, "AAA", 			"", 			0,			"");
	sea_assert_align(ctx, "TTTTTTT", 		"", 			0,			"");
	sea_assert_align(ctx, "", 				"AAA", 			0,			"");
	sea_assert_align(ctx, "", 				"TTTTTTT", 		0,			"");
	sea_assert_align(ctx, "CTAG",			"", 			0,			"");

	/**
	 * a match
	 */
	sea_assert_align(ctx, "A",				"A", 			m,			"M");
	sea_assert_align(ctx, "C", 				"C", 			m,			"M");
	sea_assert_align(ctx, "G", 				"G", 			m,			"M");
	sea_assert_align(ctx, "T", 				"T", 			m,			"M");

	/**
	 * a mismatch
	 */
	if(ALG == NW) {
		/**
		 * the Needleman-Wunsch algorithm
		 */
		sea_assert_align(ctx, "A", 				"C", 			x,			"X");
		sea_assert_align(ctx, "C", 				"A", 			x,			"X");
		sea_assert_align(ctx, "G", 				"T", 			x,			"X");
		sea_assert_align(ctx, "T", 				"A", 			x,			"X");
	} else if(ALG == SEA || ALG == SW) {
		/**
		 * the Smith-Waterman algorithm and the seed-and-extend algorithm
		 */
		sea_assert_align(ctx, "A", 				"C", 			0,			"");
		sea_assert_align(ctx, "C", 				"A", 			0,			"");
		sea_assert_align(ctx, "G", 				"T", 			0,			"");
		sea_assert_align(ctx, "T", 				"A", 			0,			"");
	}

	/**
	 * homopolymers with different lengths.
	 */
	if(ALG == NW) {
		/**
		 * the Needleman-Wunsch algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			gi+m,			"IM");
		sea_assert_align(ctx, "A", 				"AAA", 			gi+ge+m,		"IIM");
		sea_assert_align(ctx, "AAAA", 			"AA", 			gi+ge+m+m,		"DDMM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	gi+3*ge+4*m,	"IIIIMMMM");
	} else if(ALG == SEA || ALG == SW) {
		/**
		 * the Smith-Waterman algorithm and the seed-and-extend algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			m,				"M");
		sea_assert_align(ctx, "A", 				"AAA", 			m,				"M");
		sea_assert_align(ctx, "AAAA", 			"AA", 			m+m,			"MM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	4*m,			"MMMM");
	}

	/**
	 * when mismatches occurs.
	 */
	sea_assert_align(ctx, "AAAAAAAA", 		"AAAATAAA", 	7*m+x,			"MMMMXMMM");
	sea_assert_align(ctx, "TTTTTTTT", 		"TTTCTTTT", 	7*m+x,			"MMMXMMMM");
	sea_assert_align(ctx, "CCCCCCCC", 		"CCTCCCCC", 	7*m+x,			"MMXMMMMM");
	sea_assert_align(ctx, "GGGGGGGG", 		"GGCGGTGG", 	6*m+2*x,		"MMXMMXMM");

	/**
	 * when gaps with a base occurs on seq a (insertion).
	 */
	sea_assert_align(ctx, "AAAAATTTT", 		"AAAAAGTTTT", 	9*m+gi,			"MMMMMIMMMM");
	sea_assert_align(ctx, "TTTTCCCCC", 		"TTTTACCCCC", 	9*m+gi,			"MMMMIMMMMM");
	sea_assert_align(ctx, "CCCGGGGGG", 		"CCCTGGGGGG", 	9*m+gi,			"MMMIMMMMMM");
	sea_assert_align(ctx, "GGGAATTT", 		"GGGCAAGTTT", 	8*m+2*gi,		"MMMIMMIMMM");

	/**
	 * when gaps with a base occurs on seq b (deletion).
	 */
	sea_assert_align(ctx, "AAAAAGTTTT", 	"AAAAATTTT", 	9*m+gi,			"MMMMMDMMMM");
	sea_assert_align(ctx, "TTTTACCCCC", 	"TTTTCCCCC", 	9*m+gi,			"MMMMDMMMMM");
	sea_assert_align(ctx, "CCCTGGGGGG", 	"CCCGGGGGG", 	9*m+gi,			"MMMDMMMMMM");
	sea_assert_align(ctx, "GGGCAAGTTT", 	"GGGAATTT", 	8*m+2*gi,		"MMMDMMDMMM");

	/**
	 * when a gap longer than two bases occurs on seq a.
	 */
	sea_assert_align(ctx, "AAAATTTT", 		"AAAAGGTTTT", 	8*m+gi+ge,		"MMMMIIMMMM");
	sea_assert_align(ctx, "GGGGCCCC", 		"GGGGTTTCCCC", 	8*m+gi+2*ge,	"MMMMIIIMMMM");
	sea_assert_align(ctx, "GGGGGCCCCC", 	"GGGGGTTTTCCCCC",10*m+gi+3*ge,	"MMMMMIIIIMMMMM");
	sea_assert_align(ctx, "TTTTAAGGGG", 	"TTTTCCAACCGGGG",10*m+2*gi+2*ge,"MMMMIIMMIIMMMM");

	/**
	 * when a gap longer than two bases occurs on seq b.
	 */
	sea_assert_align(ctx, "AAAAGGTTTT",	 	"AAAATTTT", 	8*m+gi+ge,		"MMMMDDMMMM");
	sea_assert_align(ctx, "GGGGTTTCCCC",	"GGGGCCCC", 	8*m+gi+2*ge,	"MMMMDDDMMMM");
	sea_assert_align(ctx, "GGGGGTTTTCCCCC",	"GGGGGCCCCC", 	10*m+gi+3*ge,	"MMMMMDDDDMMMMM");
	sea_assert_align(ctx, "TTTTCCAACCGGGG", "TTTTAAGGGG", 	10*m+2*gi+2*ge, "MMMMDDMMDDMMMM");

	/**
	 * when outer gaps occurs.
	 */
	if(ALG == NW) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m+4*x, 		"XXMMMMMMMMXX");
	} else if(ALG == SEA) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m+2*x, 		"XXMMMMMMMM");
	} else if(ALG == SW) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m,	 		"MMMMMMMM");
	}

	sea_clean(ctx);
	return;
}

#endif	/* #if SEQ == ascii && ALN == ascii */
#endif	/* #ifdef TEST */

/**
 * end of naive_affine_full.c
 */
