

libsea -- a SIMD-based ultrafast sequence alignment library
===========================================================


Newer version (libgaba, semi-global alignment only) is available at [https://github.com/ocxtal/libgaba](https://github.com/ocxtal/libgaba).


## Overview

Libsea is a fast nucleotide-sequence alignment library in the field of the
bioinformatics. This library contains implementations of the local alignment
algorithm (the Smith-Waterman algorithm), the global alignment algorithm
(the Needleman-Wunsch algorithm), and the seed-and-extend alignment (or the
X-drop alignment) algorithms. The dynamic programming calculations are
parallelized at data level, with single-instruction-multiple-data (SIMD)
instructions.


## Features

* **Fast**: About 10 times faster than conventional libraries, e.g. SeqAn.
* **General-purpose**: Simple APIs cover basic algorithms on nucleotide alignments.
* **Portable**: The APIs work well on both SIMD and non-SIMD environments.
* **Easy-to-use license**: Apache v2 license enables it easy to embed this library into your programs.

## Variants

Following algorithmic, matrix, and cost variants are provied.
File names of the implementations are listed in the table below.

### Algorithmic variants

* **Global alignment** the Needleman-Wunsch algorithm, where both ends of
alignments are fixed.
* **Local alignment** the Smith-Waterman algorithm, where both ends are
free.
* **Seed-and-extend alignment** or the semi-global alignment, or so-called
X-drop alignment. Both with/without X-drop termination are supported.

### Matrix variants

* **Full-size matrix** only naive implementation supports full-size matrix.
* **Banded matrix** naive and SIMD-vectorized implementations are provided.
* **Dynamic banded matrix** the direction of the extension is deterimined
dynamically.

### Cost variants

* **Linear-gap cost** the original Smith-Waterman algorithm and its variants.
* **Affine-gap cost** the Gotoh's triple-layered algorithm and its variants.
* **Unit cost** the Myers' bit-parallel algorithm and its banded variant
(Kimura's bit-parallel algorithm). This algorithm does not support local 
alignment.

![table](https://bitbucket.org/suzukihajime/libsea/raw/master/misc/table.png)


## Benchmarks

### vs. SeqAn.Align

The libsea is around **over 10 times faster** than the SeqAn sequence
analysis library.

| Query length |    100 |    300 |   1000 |   3000 |  10000 |  30000 |
|:------------:|-------:|-------:|-------:|-------:|-------:|-------:|
|     SeqAn    |    145 |    473 |   1593 |   4794 |  15945 |  47778 |
|    libsea    |     12 |     33 |    111 |    657 |   1044 |   4311 |

Total computation time of a thousand of affine-gap-cost banded local
alignment queries in milliseconds, run on Xeon-X5650 with SL6.5 machine.
The benchmarking program, located at misc/bench_seqan, is compiled with
'-O3' flags with gcc-4.4.4. In this benchmark, the libsea uses the 
SIMD-vectorized-with-difference-recurrence variant. See comments in the
source file for more details of the benchmark and the algorithms.

### Comparison of variants

The library contains various implementations, naive, SIMD-vectorized,
SIMD-vectorized with a difference recurrence, and bit-parallel. The fastest
implementation is the SIMD-vectorized with a difference recurrence, around
ten times faster than the naive implementations.

![bench_aa50819](https://bitbucket.org/suzukihajime/libsea/raw/master/misc/bench_aa50819.png)

The N, B, and D in the legend represents the naive, the SIMD-vectorized,
and the SIMD-vectorized-with-difference-recurrence variants, respectively.
The following character, L or A, stands for the costs, linear or affine.
The following number is the cell width of the band in the banded dynamic
programming.

## Environments

### Architectures

* x86_64 (which supports SSE4.1 or AVX2 instructions)

### Operating systems and Compilers

* Linux
    * icc   (Intel C Compiler, tested on 12.1.3 and 14.0.1 on SL6.5)
    * gcc   (4.4.4 on SL6.5, 4.6.3 on Ubuntu 12.04)
    * clang (3.0 on Ubuntu 12.04)

* Mac OS X
    * clang (3.5 on OS X 10.9.5)
    * gcc   (4.9.0 on OS X 10.9.5)

* Windows
    * gcc (on cygwin)
    * clang (on cygwin)

### Other dependencies

* Python (2.7 or 3.3)
* coreutils (the 'nm' command is required in the build step.)


## Installation

The libsea uses waf as a build system. To build and install the library,
type folloing commands on a terminal.

    $ git clone https://suzukihajime@bitbucket.org/suzukihajime/libsea.git
    $ cd libsea
    $ python waf configure
    $ python waf build
    $ python waf install (or sudo python waf install)

### Files to be installed

The 'waf install' command installs following files on your machine.

* sea.h      (at $PREFIX/include)
* libsea.so  (at $PREFIX/lib)
* libsea.a   (at $PREFIX/lib)


## Usage

### C

The libsea library provides only four APIs below. The 'sea\_init' function
generates an alignment context, the 'sea\_align' function calculates alignments,
and the other functions clean up the no-longer-needed objects.

* **sea\_init**: initialize an alignment context.
* **sea\_align**: calculate alignment.
* **sea\_aln\_free**: clean up unnecessary alignment results.
* **sea\_clean**: clean up the alignment context.

A C sample code and the result are shown below.

sea_sample.c:

```c
#include <stdio.h>
#include <sea.h>

int main(void) {

	char const *a = "ATCTATCATCTGTCGCTCGATCGA";
	char const *b = "ATCTATCATCTGTCGCTCGATCGA";

	/**
	 * initialize an alignment context.
	 */
	struct sea_context *ctx = sea_init(
		SEA_NW |				/** the Smith-Waterman algorithm */
		SEA_AFFINE_GAP_COST |	/** the affine-gap cost */
		SEA_BANDED_DP |			/** the banded dynamic programming */
		SEA_BANDWIDTH_32 |		/** bandwidth : 32 */
		SEA_SEQ_ASCII |			/** input sequence format : ASCII */
		SEA_ALN_ASCII,			/** output format : ASCII */
		10000,					/** maximum input sequence length : 10000 */
		2, -3, -5, -1,			/** match, mismatch, gap open, and gap extension cost */
		100);					/** the xdrop threshold */
		
	/**
	 * do alignment
	 */
	struct sea_result *aln = sea_align(
		ctx,					/** the alignment context */
		a,						/** a pointer to the sequence a */
		0,						/** a start position on sequence a */
		strlen(a),				/** the length of the sequence a */
		b,						/** a pointer to the sequence b */
		0,						/** a start position on sequence a */
		strlen(b));				/** the length of the sequence a */

	/*
	 * print results. the sea_result structure has following members.
	 * position, length, and score: aln.pos, aln.len, aln.score
	 * the alignment string:        aln.aln
	 * the information on queries:  aln.a aln.apos, aln.alen
	 *                              aln.b aln.bpos, aln.blen
	 */
	printf("%s\n", aln.aln);	/** yields MMMMMM... */
		
	/** free unnecessary object */
	sea_aln_free(ctx, aln);

	/** free context */
	sea_clean(ctx);

	return 0;
}
```

Results:

```sh
$ gcc -Wall -o sea_sample sea_sample.c -lsea
$ ./sea_sample
MMMMMMMMMMMMMMMMMMMMMMMM
$
```

### C++

Wrapper class of the sea\_init and the sea\_align functions are available
when sea.h is included in the C++ source code. Here is an example of
object-oriented APIs.

```cpp
#include <iostream>
#include <string>
#include <sea.h>

using namespace sea;
int main(void) {
	std::string a("AAAA");
	std::string b("AAAA");

	AlignmentContext ctx(
		SEA_SW | SEA_AFFINE_GAP_COST |
		SEA_BANDED_DP | SEA_BANDWIDTH_32 |
		SEA_SEQ_ASCII | SEA_ALN_ASCII,
		50000,
		2, -3, -5, -1,
		0);

	AlignmentResult aln = ctx.align(
		a, 0, a.length(),
		b, 0, b.length());

	std::cout << aln.aln() << std::endl;
	return 0;
}
```

### Python

Python wrapper, based on the ctypes library, is available. Copy sea.py
in bindings/python/ into the directory of your source codes and import
like folloing.

```python
import sea

a = b'AAAA'
b = b'AAAA'

ctx = sea.AlignmentContext(
	# list of options
	['SW',						# the Smith-Waterman algorithm
	'AFFINE_GAP_COST',			# the affine-gap cost
	'FULL_DP'],					# use a full dynamic programming matrix
	1000,						# the length of query sequence
	2, -3, -5, -1,				# M, X, Gi, and Ge
	100)						# xdrop threshold

result = ctx.align(
	a, 0, len(a),				# a pointer to seq.a, start postion on a, and search length on a.
	b, 0, len(b))				# a pointer to seq.b, start postion on b, and search length on b.

print(result.aln.decode())		# for python3, decode ascii array to string.
```

### Java

JNA-based wrapper classes of the library is available. Pass '--enable-java'
option on configure and a jar archive is generated at build/bindings/java/.

```java
import libsea.*;

class Example {
	public static void main(String[] args) {
		String a = new String("AAAA");
		String b = new String("AAAA");

		AlignmentContext ctx = new AlignmentContext(
			SeaFlags.SW |
			SeaFlags.AFFINE_GAP_COST |
			SeaFlags.FULL_DP |
			SeaFlags.SEQ_ASCII |
			SeaFlags.ALN_ASCII,
			1000,
			2, -3, -5, -1,
			1000);

		AlignmentResult aln = ctx.align(
			a, 0, a.length(),
			b, 0, b.length());

		System.out.println(aln.aln);
		aln.clean();
		ctx.clean();
	}
}
```

### D

An object-oriented wrapper class is provided.

```D
import std.stdio;
import std.string;
import sea;

void main() {
	auto a = "AAAA";
	auto b = "AAAA";

	auto ctx = new AlignmentContext(
		SEA_NW |				/** the Smith-Waterman algorithm */
		SEA_AFFINE_GAP_COST |	/** the affine-gap cost */
		SEA_BANDED_DP |			/** the banded dynamic programming */
		SEA_BANDWIDTH_32 |		/** bandwidth : 32 */
		SEA_SEQ_ASCII |			/** input sequence format : ASCII */
		SEA_ALN_ASCII,			/** output format : ASCII */
		10000,					/** maximum input sequence length : 10000 */
		2, -3, -5, -1,			/** match, mismatch, gap open, and gap extension cost */
		100);

	auto aln = ctx.getAlignment(
		a, 0, a.length,
		b, 0, b.length);

	writeln(aln.alnstr);
	return;
}
```

