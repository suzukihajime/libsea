
/**
 * @file tester.c
 *
 * @brief unittest helper functions.
 */

#include <stdlib.h>
#include <string.h>

/**
 * global accumulators.
 */
int g_total;
int g_fail;
int g_succ;

/**
 * accumulator utils
 */
void tester_init(void)
{
	g_total = 0;
	g_fail = 0;
	g_succ = 0;
	return;
}

void tester_clean(void)
{
	g_total = 0;
	g_fail = 0;
	g_succ = 0;	
	return;
}

void add_fail(void)
{
	g_total++;
	g_fail++;
	return;
}

void add_succ(void)
{
	g_total++;
	g_succ++;
	return;
}

int get_total(void)
{
	return g_total;
}

int get_fail(void)
{
	return g_fail;
}

int get_succ(void)
{
	return g_succ;
}

/**
 * random sequence generator, modifier.
 * rseq generates random nucleotide sequence in ascii,
 * mseq takes ascii sequence, modifies the sequence in given rate.
 */
static char rbase(void)
{
	switch(rand() % 4) {
		case 0: return 'A';
		case 1: return 'C';
		case 2: return 'G';
		case 3: return 'T';
		default: return 'A';
	}
}

char *rseq(int len)
{
	int i;
	char *seq;
	seq = (char *)malloc(sizeof(char) * (len + 1));

	for(i = 0; i < len; i++) {
		seq[i] = rbase();
	}
	seq[len] = '\0';
	return(seq);
}

char *mseq(char const *seq, int x, int ins, int del)
{
	int i;
	int len = strlen(seq);
	char *mod, *ptr;
	mod = (char *)malloc(sizeof(char) * 2 * (len + 1));

	ptr = mod;
	for(i = 0; i < len; i++) {
		if(rand() % x == 0) { *ptr++ = rbase(); }
		else if(rand() % ins == 0) { *ptr++ = rbase(); i--; }
		else if(rand() % del == 0) { /* skip a base */ }
		else { *ptr++ = seq[i]; }
	}
	*ptr = '\0';
	return(mod);
}

/**
 * end of tester.c
 */
