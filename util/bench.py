#! /usr/bin/env python
# encoding: utf-8

"""
benchmark runner
"""

def runall(benchname, length = 1000, count = 1000):
	import subprocess

	arg = '%s -l %s -c %s' % (benchname, length, count)
	subprocess.call(arg, shell = True)

if __name__ == '__main__':
	runall('../build/benchmark')
