
/**
 * @file bench.h
 *
 * @brief declares variables to accumulate benchmark results.
 *
 * @detail
 * This is a collections of macros to take a benchmark. The four macros,
 * DECLARE_BENCH, CLEAR_BENCH, START_BENCH, and END_BENCH are enabled
 * when a BENCH macro is defined. The elapsed time between BENCH_START
 * and BENCH_END is accumulated to the _<name>_acc variable in nanoseconds.
 */

#ifndef _BENCH_H_INCLUDED
#define _BENCH_H_INCLUDED

#include "../build/config.h"

/**
 * @struct result
 * @brief a container of benchmark results.
 */
struct result {
	long fill;
	long search;
	long trace;
	long total;
};

/**
 * @fn benchmark
 *
 * @brief receive alignment context, issue queries, and summarize results.
 *
 * @param[in] ctx : a pointer to an alignment context.
 * @param[in] count : number of queries issued in the benchmark.
 * @param[in] pa : a pointer to an array of ascii query sequences a.
 * @param[in] pb : a pointer to an array of ascii query sequences b.
 *
 * @return a struct result which contains elapsed times of each section in nanoseconds.
 *
 * @detail
 * This function receives an alignment context, retrieves sequences from
 * pa and pb, issues a query with a pair of sequences retrieved. The
 * function issues a set of queries, from the beginning to the end of
 * the arrays <count> times, so the total number of queries issued becomes 
 * min(length(pa), length(pb)) * count.
 */
struct result benchmark(
	struct sea_context *ctx,
	sea_int_t count,
	char **pa,
	char **pb);

#ifdef BENCH
/**
 * When the BENCH macro is defined, the DECLARE_BENCH, CLEAR_BENCH, START_BENCH,
 * and the END_BENCH macros are repleced to proper benchmarking codes.
 */

/**
 * accumulators
 */
extern long _fill_acc,
			_search_acc,
			_trace_acc;

#if HAVE_CLOCK_GETTIME
/**
 * when run on linux, use clock_gettime for benchmarking
 */
#include <time.h>		/** for clock_gettime and struct timespec */

/**
 * @macro DECLARE_BENCH
 *
 * @brief declares variables to accumulate a result.
 */
#define DECLARE_BENCH(name)		struct timespec _##name##_tss, \
 												_##name##_tse;

/**
 * @macro CLEAR_BENCH
 *
 * @brief clears the accumulator.
 */
#define CLEAR_BENCH(name) 		{ _##name##_acc = 0; }

/**
 * @macro START_BENCH
 *
 * @brief records a start time in nanosec.
 */
#define START_BENCH(name)		{ clock_gettime(CLOCK_REALTIME, &_##name##_tss); }

/**
 * @macro END_BENCH
 *
 * @brief add (current_time - start_time) to the accumulator.
 */
#define END_BENCH(name) 		{ clock_gettime(CLOCK_REALTIME, &_##name##_tse); \
								  _##name##_acc += ((_##name##_tse.tv_sec  - _##name##_tss.tv_sec) * 1000000000 \
												  + (_##name##_tse.tv_nsec - _##name##_tss.tv_nsec)); }

#elif HAVE_SYS_TIME_H
/**
 * when run on the other systems, use gettimeofday instead
 */
#include <sys/time.h>

/**
 * @macro DECLARE_BENCH
 *
 * @brief declares variables to accumulate a result.
 */
#define DECLARE_BENCH(name)		struct timeval _##name##_tvs, \
 											   _##name##_tve;
/**
 * @macro CLEAR_BENCH
 *
 * @brief clears the accumulator.
 */
#define CLEAR_BENCH(name) 		{ _##name##_acc = 0; }

/**
 * @macro START_BENCH
 *
 * @brief records a start time in nanosec.
 */
#define START_BENCH(name)		{ gettimeofday(&_##name##_tvs, NULL); }

/**
 * @macro END_BENCH
 *
 * @brief add (current_time - start_time) to the accumulator.
 */
#define END_BENCH(name) 		{ gettimeofday(&_##name##_tve, NULL); \
								  _##name##_acc += ((_##name##_tve.tv_sec  - _##name##_tvs.tv_sec)  * 1000000000 \
												  + (_##name##_tve.tv_usec - _##name##_tvs.tv_usec) * 1000); }

#else /** #if HAVE_CLOCK_GETTIME */
/**
 * when the system does not have both sys/time.h and clock_gettime function,
 * (mainly on Windows) disable benchmark macros.
 */

#define DECLARE_BENCH(name) 	;
#define CLEAR_BENCH(name)		;
#define START_BENCH(name)		;
#define END_BENCH(name)			;

#endif /** #if HAVE_CLOCK_GETTIME */

#else /** #ifdef BENCH */

/**
 * when the BENCH macro is not defined, the four macros are replaced by blanks.
 */
#define DECLARE_BENCH(name)		;
#define CLEAR_BENCH(name)		;
#define START_BENCH(name)		;
#define END_BENCH(name)			;

#endif /** #ifdef BENCH */
#endif /** #ifdef _BENCH_H_INCLUDED */

/**
 * end of bench.h
 */
