
/**
 * @file bench.c
 *
 * @brief a benchmarking utilities.
 *
 * @detail
 * This file contains some functions and the global variables for benchmarking.
 * The usage of the global variables are detailed in bench.h.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/sea.h"
#include "../util/util.h"
#include "../util/bench.h"
#include "../build/config.h"
#if HAVE_CLOCK_GETTIME
 	#include <time.h>			/** for struct timespec */
#elif HAVE_SYS_TIME_H
 	#include <sys/time.h>		/** for struct timeval */
#endif

/**
 * global variables
 * These variables will be accessed from each algorithms through macros
 * in bench.h.
 */
long _fill_acc = 0,
	 _search_acc = 0,
	 _trace_acc = 0,
	 _total_acc = 0;

/**
 * @fn benchmark
 *
 * @brief receive alignment context, issue queries, and summarize results.
 *
 * @param[in] ctx : a pointer to an alignment context.
 * @param[in] count : number of queries issued in the benchmark.
 * @param[in] pa : a pointer to an array of ascii query sequences a.
 * @param[in] pb : a pointer to an array of ascii query sequences b.
 *
 * @return a struct result which contains elapsed times of each section in nanoseconds.
 *
 * @detail
 * This function receives an alignment context, retrieves sequences from
 * pa and pb, issues a query with a pair of sequences retrieved. The
 * function issues a set of queries, from the beginning to the end of
 * the arrays <count> times, so the total number of queries issued becomes 
 * min(length(pa), length(pb)) * count.
 */
struct result benchmark(
	struct sea_context *ctx,
	sea_int_t count,
	char **pa,
	char **pb)
{
	sea_int_t i;
	char **ca,		/** a pointer to a sequence, temporary */
		 **cb;
	struct sea_result *aln;
	struct result res;

	unsigned long _total_stime;
	#ifdef HAVE_CLOCK_GETTIME
		struct timespec _total_tss, _total_tse;
	#elif HAVE_SYS_TIME_H
		struct timeval _total_tvs, _total_tve;
	#endif

	/**
	 * clear accumulators.
	 */
	CLEAR_BENCH(fill);
	CLEAR_BENCH(search);
	CLEAR_BENCH(trace);
	CLEAR_BENCH(total);

	START_BENCH(total);
	for(i = 0; i < count; i++) {
		ca = pa; cb = pb;
		while(*ca != NULL && *cb != NULL) {
			/** issue a query */
			aln = sea_align(ctx,
				*ca, 0, strlen(*ca),
				*cb, 0, strlen(*cb));
			/** check if the alignment is valid */
			if(aln->aln == NULL) { goto _bench_error_handler; }
			/** free alignment */
			sea_aln_free(aln);
			ca++; cb++;
		}
	}
	END_BENCH(total);

	/**
	 * retrieve accumulators and store them to res.
	 */
	res.fill = _fill_acc;
	res.search = _search_acc;
	res.trace = _trace_acc;
	res.total = _total_acc;
	return(res);

_bench_error_handler:
	res.fill = res.search = res.trace = -1;
	res.total = aln->score;
	sea_aln_free(aln);
	return(res);
}

/**
 * end of bench.c
 */
