#! /usr/bin/env python
# -*- coding: utf-8 -*-

#
# libsea unittest runner generator
#

def generate(libname, filename):
	import sys
	import subprocess
	import os.path
	
	prefix = 'test_'		# default prefix

	(base, ext) = os.path.splitext(libname)
	if (sys.platform == 'darwin' or sys.platform.startswith('linux')) and ext != '.a':
		ext = '.a'

	libname = base + ext
	symbols = [e.split(' ')[2].lstrip('_') for e
		in subprocess.check_output(['nm', '-g', libname]).decode('utf-8').split('\n')
		if len(e.split(' ')) == 3 and e.split(' ')[1] == 'T']
	symbols.sort()
	decls = ['void {0}(void);'.format(sym) for sym in symbols if sym.startswith(prefix)]
	calls = ['printf("Run test: {0}\\n"); {0}();'.format(sym) for sym in symbols if sym.startswith(prefix)]

	program = '''
	#include <stdio.h>
	#include "../util/tester.h"
	{0}
	int main(void) {{
		tester_init();
		{1}
		printf("\\x1b[3%cmSummary: total %d assertions in {2} tests, %d success, %d fail.\\x1b[39m\\n",
			get_fail() == 0 ? '2' : '1',
			get_total(), get_succ(), get_fail());
		tester_clean();
		return 0;
	}}
	'''.format('\n'.join(decls), '\n'.join(calls), len(decls))

	if filename is not None:
		with open(filename, 'w') as f:
			f.write(program)

	return program

# run all
if __name__ == '__main__':
	import sys

	print(generate(sys.argv[1], None))

