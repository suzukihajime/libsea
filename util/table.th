
/**
 * @header table.th
 *
 * @brief a template for table.h
 *
 * @detail
 * This file consists of some macros and a template of a function pointer table.
 *
 * This file will be preprocessed by a C compiler (gcc, clang, ...) with
 * '-E' (only preprocess) option and some '-D' (define) options.
 * The definitions of 'PREFIX' and 'SUFFIX' must be specified with '-D' flags,
 * the 'PREFIX' is a prefix of functions, typically the file name of
 * the C source code without extension which includes the implementation
 * of the algorithm. The 'SUFFIX' will be used to identify the
 * implementation-specific options. If suffix is not empty string,
 * the suffix string must begins with the '_' (an underscore).
 *
 * example:
 * $ gcc -E -xc -o sample_table.h table.th -DPREFIX=sample
 *
 * The template of the table (and the corresponding constants, DIM_SEQ,
 * DIM_ALN, and DIM_ALG) must be consistent with the definitions in
 * seqreader.h, function argument list defined in sea.h, and build recipes
 * in 'wscript's.
 */

/**
 * macros to generate function name.
 */
#define JOIN3(i,j,k)					i##j##k
#define JOIN4(i,j,k,l)					i##j##k##l

#define JOIN_TABLE_NAME(a,b,c)			JOIN3(a,b,c)
#define FUNC_TABLE(pfx,opt)				JOIN_TABLE_NAME(pfx, opt, _table)

#define JOIN_FUNC_NAME4(a,b,c,d)		JOIN4(a,b,c,d)
#define JOIN_FUNC_NAME3(a,b,c)			JOIN3(a,b,c)
#define TABLE_FUNC_NAMES(pfx,type,opt)	{JOIN_FUNC_NAME4(pfx, _matsize, type, opt), JOIN_FUNC_NAME3(pfx, type, opt)}

/**
 * function argument list: must be consistent with sea.h
 */
/*
#define FUNC_PARAM_LIST					struct sea_result *aln, \
 										struct sea_params param, \
										void *mat
*/
/**
 * dimension of the table. DIM_SEQ and DIM_ALN must be the same with
 * the definition in seqreader.h
 */
#define DIM_ALG				( 4 )
#define DIM_SEQ				( 6 )
#define DIM_ALN				( 2 )

/**
 * table definition
 */
static struct sea_funcs
FUNC_TABLE(PREFIX, SUFFIX)[DIM_ALG][DIM_SEQ][DIM_ALN] = {
	/**
	 * index: 0 (SEA_SW = 1<<SEA_FLAGS_POS_ALG in sea.h)
	 * the Smith-Waterman algorithm without X-dropoff termination.
	 */
	{
		{
			TABLE_FUNC_NAMES(PREFIX, _ascii_ascii_sw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _ascii_cigar_sw, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq4_ascii_sw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq4_cigar_sw, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq2_ascii_sw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq2_cigar_sw, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq4p8_ascii_sw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq4p8_cigar_sw, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq2p8_ascii_sw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq2p8_cigar_sw, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq1p64_ascii_sw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq1p64_cigar_sw, SUFFIX),
		}
	},
	/**
	 * index: 1 (SEA_SEA = 2<<SEA_FLAGS_POS_ALG in sea.h)
	 * the seed-and-extend alignment algorithm without X-dropoff termination.
	 */
	{
		{
			TABLE_FUNC_NAMES(PREFIX, _ascii_ascii_sea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _ascii_cigar_sea, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq4_ascii_sea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq4_cigar_sea, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq2_ascii_sea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq2_cigar_sea, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq4p8_ascii_sea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq4p8_cigar_sea, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq2p8_ascii_sea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq2p8_cigar_sea, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq1p64_ascii_sea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq1p64_cigar_sea, SUFFIX),
		}
	},
	/**
	 * index: 2 (SEA_XSEA = 3<<SEA_FLAGS_POS_ALG in sea.h)
	 * the seed-and-extend alignment algorithm with X-dropoff termination.
	 */
	{
		{
			TABLE_FUNC_NAMES(PREFIX, _ascii_ascii_xsea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _ascii_cigar_xsea, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq4_ascii_xsea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq4_cigar_xsea, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq2_ascii_xsea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq2_cigar_xsea, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq4p8_ascii_xsea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq4p8_cigar_xsea, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq2p8_ascii_xsea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq2p8_cigar_xsea, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq1p64_ascii_xsea, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq1p64_cigar_xsea, SUFFIX),
		}
	},
	/**
	 * index: 3 (SEA_NW = 4<<SEA_FLAGS_POS_ALG in sea.h)
	 * the Needleman-Wunsch algorithm. (it's nonsense to combine this algorithm with the X-dropoff test)
	 */
	{
		{
			TABLE_FUNC_NAMES(PREFIX, _ascii_ascii_nw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _ascii_cigar_nw, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq4_ascii_nw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq4_cigar_nw, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq2_ascii_nw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq2_cigar_nw, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq4p8_ascii_nw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq4p8_cigar_nw, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq2p8_ascii_nw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq2p8_cigar_nw, SUFFIX),
		},
		{
			TABLE_FUNC_NAMES(PREFIX, _seq1p64_ascii_nw, SUFFIX),
			TABLE_FUNC_NAMES(PREFIX, _seq1p64_cigar_nw, SUFFIX),
		}
	}
};

#undef FUNC_PARAM_LIST
/**
 * end of table.th
 */

