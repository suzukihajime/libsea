
/**
 * @file tester.h
 *
 * @brief includes function declarations and macros for unittests.
 *
 * @detail
 * Composition of unittest runner
 * 
 * In this library, all of the unittest functions for the algorithms are named
 * in the form of 'test_<type of test>_<filename>'. These test functions will
 * be included into a shared library when a 'TEST' macro is defined true.
 * 
 * The python script 'tester.py' searches into the shared library and runs
 * functions begin with 'test_'. The helper functions in the 'tester.c' are
 * called from 'sea_assert' macros embedded in the test functions, summarizes
 * the results of the tests.
 *
 * Typing './waf test' at the top directory, you can build the library with
 * the test functions included and run all the tests.
 */
#ifndef _TESTER_H_INCLUDED
#define _TESTER_H_INCLUDED

#include <stdio.h>
#include <string.h>
#include "util.h"

/**
 * @macro sea_assert
 *
 * @brief a macro to evaluate the given expression and summarize the result.
 *
 * @param x : expression to be evaluated.
 * @param fmt : debug output printed when assertion failed. follows the string formatting of sprintf.
 * @param ... : params of fmt.
 */
#define sea_assert(x, fmt, ...) { \
	if(x) { /* suc */ \
		add_succ(); \
	} else { \
		fprintf(stderr, RED("\tassertion failed: file: %s (%d)\t%s \"%s\"\n"), \
			__FILE__, \
			__LINE__, \
			__func__, \
			#x); \
		fprintf(stderr, fmt, __VA_ARGS__); \
		fprintf(stderr, "\n"); \
		add_fail(); \
	} \
}

/**
 * @macro sea_assert_align
 *
 * @brief run alignment function, evaluates the score and alignment results.
 *
 * @param ctx : an alignment context.
 * @param a : input sequence a.
 * @param b : input sequence b.
 * @param _score : expected score.
 * @param _aln : expected alignment string.
 */
#define sea_assert_align(ctx, a, b, _score, _aln) { \
	struct sea_result *_res = \
		sea_align(ctx, \
			a, 0, strlen(a), \
			b, 0, strlen(b)); \
	sea_assert(_res->score == _score, "%s\n%s\n%d, %d", a, b, _res->score, _score); \
	sea_assert(strcmp(_res->aln, _aln) == 0, "%s\n%s\n%s\n%s", a, b, (char *)(_res->aln), _aln); \
	sea_aln_free(_res); \
}

/**
 * @macro sea_assert_cross
 *
 * @brief run two alignment funcitions, check the equality of the results.
 *
 * @param ctx1 : alignment context 1.
 * @param ctx2 : alignment context 2.
 * @param a : input sequence a.
 * @param b : input sequence b.
 */
#define sea_assert_cross(ctx1, ctx2, a, b) { \
 	struct sea_result *_res1 = \
 		sea_align(ctx1, \
			a, 0, strlen(a), \
			b, 0, strlen(b)); \
 	struct sea_result *_res2 = \
 		sea_align(ctx2, \
			a, 0, strlen(a), \
			b, 0, strlen(b)); \
 	sea_assert(_res1->score == _res2->score, "score inconsistent:\n%s\n%s\n%d, %d", a, b, _res1->score, _res2->score); \
	sea_assert(_res1->len == _res2->len, "alignment length inconsistent:\n%s\n%s\n%d, %d", a, b, _res1->score, _res2->score); \
	sea_assert(_res1->alen == _res2->alen, "alen inconsistent:\n%s\n%s\n%d, %d", a, b, _res1->score, _res2->score); \
	sea_assert(_res1->blen == _res2->blen, "blen inconsistent:\n%s\n%s\n%d, %d", a, b, _res1->score, _res2->score); \
	sea_assert(strcmp(_res1->aln, _res2->aln) == 0, "alignment string inconsistent:\n%s\n%s\n%s\n%s", a, b, (char *)(_res1->aln), (char *)(_res2->aln)); \
	sea_aln_free(_res1); \
	sea_aln_free(_res2); \
}

/**
 * function declarations for the unittest runner.
 *
 * These functions are called from tester.py with ctypes wrappers.
 */
void tester_init(void);
void tester_clean(void);
void add_fail(void);
void add_succ(void);
int get_total(void);
int get_fail(void);
int get_succ(void);

/**
 * random sequence generator and modifier.
 */
char *rseq(int len);
char *mseq(char const *seq, int x, int i, int d);


#endif /* #ifdef _TESTER_H_INCLUDED */

/**
 * end of tester.h
 */
