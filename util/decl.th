
/**
 * @header decl.th
 *
 * @brief a template for decl.h
 *
 * @detail
 * This file consists of some macros and templates of function declaraions.
 *
 * This file will be preprocessed by a C compiler (gcc, clang, ...) with
 * '-E' (only preprocess) option and some '-D' (define) options.
 * The definitions of 'PREFIX' and 'SUFFIX' must be specified with '-D' flags,
 * the 'PREFIX' is a prefix of functions, typically the file name of
 * the C source code without extension which includes the implementation
 * of the algorithm. The 'SUFFIX' will be used to identify the
 * implementation-specific options. If suffix is not empty string,
 * the suffix string must begins with the '_' (an underscore).
 *
 * example:
 * $ gcc -E -xc -o sample_decl.h decl.th -DPREFIX=sample
 *
 * The template of the table (and the corresponding constants, DIM_SEQ,
 * DIM_ALN, and DIM_ALG) must be consistent with the definitions in
 * seqreader.h, function argument list defined in sea.h, and build recipes
 * in 'wscript's.
 */

/**
 * macros to generate function name.
 */
/*
#define JOIN3(i,j,k)					i##j##k
#define JOIN_TABLE_NAME(a,b,c)			JOIN3(a,b,c)
#define FUNC_TABLE(pfx,opt)				JOIN_TABLE_NAME(pfx, opt, _table)
*/
#define JOIN3(i,j,k)					i##j##k
#define JOIN4(i,j,k,l)					i##j##k##l

#define JOIN_FUNC_NAME4(a,b,c,d)		JOIN4(a,b,c,d)
#define JOIN_FUNC_NAME3(a,b,c)			JOIN3(a,b,c)

#define DECLARE_FUNCS(pfx,type,opt) \
	sea_int_t JOIN_FUNC_NAME3(pfx, type, opt)( \
		struct sea_result *aln, \
		struct sea_params param, \
		void *mat); \
	sea_int_t JOIN_FUNC_NAME4(pfx, _matsize, type, opt)( \
		sea_int_t alen, \
		sea_int_t blen, \
		sea_int_t bandwidth);

/**
 * function argument list: must be consistent with sea.h
 */
/*
#define FUNC_PARAM_LIST					struct sea_result *aln, \
										sea_int_t const scmat[4], \
										sea_int_t xdrop, \
										sea_int_t bandwidth, \
										void *mat, \
										sea_int_t matsize
*/
/**
 * function declaraions
 * a naive expansion of a product of {sw, sea, nw},
 * {ascii, seq4, seq2, seq4p8, seq2p8, seq1p64}, and {ascii, cigar}.
 *
 * the Smith-Waterman algorithm
 */
DECLARE_FUNCS(PREFIX, _ascii_ascii_sw, SUFFIX);
DECLARE_FUNCS(PREFIX, _ascii_cigar_sw, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq4_ascii_sw, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq4_cigar_sw, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq2_ascii_sw, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq2_cigar_sw, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq4p8_ascii_sw, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq4p8_cigar_sw, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq2p8_ascii_sw, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq2p8_cigar_sw, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq1p64_ascii_sw, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq1p64_cigar_sw, SUFFIX);

/*
 * the seed-and-extend alignment algorithm without xdrop termination
 */
DECLARE_FUNCS(PREFIX, _ascii_ascii_sea, SUFFIX);
DECLARE_FUNCS(PREFIX, _ascii_cigar_sea, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq4_ascii_sea, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq4_cigar_sea, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq2_ascii_sea, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq2_cigar_sea, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq4p8_ascii_sea, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq4p8_cigar_sea, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq2p8_ascii_sea, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq2p8_cigar_sea, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq1p64_ascii_sea, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq1p64_cigar_sea, SUFFIX);

/*
 * the seed-and-extend alignment algorithm with xdrop termination
 */
DECLARE_FUNCS(PREFIX, _ascii_ascii_xsea, SUFFIX);
DECLARE_FUNCS(PREFIX, _ascii_cigar_xsea, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq4_ascii_xsea, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq4_cigar_xsea, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq2_ascii_xsea, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq2_cigar_xsea, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq4p8_ascii_xsea, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq4p8_cigar_xsea, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq2p8_ascii_xsea, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq2p8_cigar_xsea, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq1p64_ascii_xsea, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq1p64_cigar_xsea, SUFFIX);

/*
 * the Needleman-Wunsch algorithm
 */
DECLARE_FUNCS(PREFIX, _ascii_ascii_nw, SUFFIX);
DECLARE_FUNCS(PREFIX, _ascii_cigar_nw, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq4_ascii_nw, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq4_cigar_nw, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq2_ascii_nw, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq2_cigar_nw, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq4p8_ascii_nw, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq4p8_cigar_nw, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq2p8_ascii_nw, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq2p8_cigar_nw, SUFFIX);

DECLARE_FUNCS(PREFIX, _seq1p64_ascii_nw, SUFFIX);
DECLARE_FUNCS(PREFIX, _seq1p64_cigar_nw, SUFFIX);

#undef FUNC_PARAM_LIST
/**
 * end of decl.th
 */
