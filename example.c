
/**
 * @file example.c
 *
 * @brief a simple example of libsea C API
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sea.h>

char *generate_random_sequence(int len);
char *generate_mutated_sequence(char *seq, double ratio);

int main(int argc, char *argv[]) {
	int const len = 100;
	unsigned long seed = 0;
	char *a, *b;

	if(argc != 3) {
		/**
		 * if #arguments is not 3, generate random sequence.
		 */
		seed = time(NULL);
		printf("seed: %lu\n", seed);
		srand(seed);
		a = generate_random_sequence(len);			/** generate random sequence of {'A, 'C', 'G', 'T'} */
		b = generate_mutated_sequence(a, 0.1);		/** mutate sequence and store it in a new array */
	} else {
		/**
		 * when input sequences are given.
		 */
		a = argv[1]; b = argv[2];
	}	
	/**
	 * initialize an alignment context.
	 * 
	 */
	struct sea_context *ctx = sea_init(
		SEA_SW |				/** the Smith-Waterman algorithm */
		SEA_AFFINE_GAP_COST |	/** the affine-gap cost */
		SEA_BANDED_DP |			/** the banded dynamic programming */
		SEA_BANDWIDTH_32 |		/** bandwidth : 32 */
		SEA_SEQ_ASCII |			/** input sequence format : ASCII */
		SEA_ALN_ASCII,			/** output format : ASCII */
		10000,					/** maximum input sequence length : 10000 */
		2, -3, -5, -1,			/** match, mismatch, gap open, and gap extension cost */
		100);					/** the xdrop threshold */

	printf("%p, %d\n", ctx, ctx->error_label);
	
	/**
	 * do alignment
	 */
	struct sea_result *aln = sea_align(
		ctx,					/** the alignment context */
		a,						/** a pointer to the sequence a */
		0,						/** a start position on sequence a */
		strlen(a),				/** the length of the sequence a */
		b,						/** a pointer to the sequence b */
		0,						/** a start position on sequence a */
		strlen(b));				/** the length of the sequence a */

	/**
	 * print results. the sea_result structure has following members.
	 * position, length, and score: aln.pos, aln.len, aln.score
	 * the alignment string:        aln.aln
	 * the information on queries:  aln.a aln.apos, aln.alen
	 *                              aln.b aln.bpos, aln.blen
	 */
	printf("%s\n%s\n", a, b);
	printf("%s\n", (char *)aln->aln);
	printf("score:  %d\nlength: %d\n", aln->score, aln->len);
	printf("apos:   %d\nalen:   %d\n", aln->apos, aln->alen);
	printf("bpos:   %d\nblen:   %d\n", aln->bpos, aln->blen);

	/**
	 * clean context
	 */
	sea_aln_free(aln);
	sea_clean(ctx);
	if(seed != 0) {
		free(a); free(b);
	}
	return 0;
}

/**
 * @fn random_base
 * @brief return a character randomly from {'A', 'C', 'G', 'T'}.
 */
char random_base(void)
{
	switch(rand() % 4) {
		case 0: return 'A';
		case 1: return 'C';
		case 2: return 'G';
		case 3: return 'T';
		default: return 'A';
	}
}

/**
 * @brief mutate_base
 * @brief mutate the given base c at the probability of 1/p.
 */
char mutate_base(char c, int p)
{
	if((rand() % p) == 0) {
		return(random_base());
	} else {
		return(c);
	}
}

/**
 * @fn generate_random_sequence
 * @brief malloc memory of size <len>, then fill it randomely with {'A', 'C', 'G', 'T'}.
 * @param[in] len : the length of sequence to generate.
 * @return a pointer to the generated sequence.
 */
char *generate_random_sequence(int len)
{
	int i;
	char *seq;		/** a pointer to sequence */
	seq = (char *)malloc(sizeof(char) * (len + 1));
	if(seq == NULL) { return NULL; }
	for(i = 0; i < len; i++) {
		seq[i] = random_base();
	}
	seq[len] = '\0';
	return seq;
}

/**
 * @fn generate_mutated_sequence
 * @brief take a DNA sequence represented in ASCII, mutate the sequence at the given probability.
 */
char *generate_mutated_sequence(char *seq, double ratio)
{
	int i;
	int mod = (int)(0.5 + 1.0 / ratio);
	int len;
	char *mutated_seq;
	if(seq == NULL) { return NULL; }
	len = strlen(seq);
	mutated_seq = (char *)malloc(sizeof(char) * (len + 1));
	if(mutated_seq == NULL) { return NULL; }
	for(i = 0; i < len; i++) {
		mutated_seq[i] = mutate_base(seq[i], mod);
	}
	mutated_seq[len] = '\0';
	return mutated_seq;

}
