#! /usr/bin/env python
# encoding: utf-8

APPNAME = 'libsea'
VERSION = '1.0.0'

srcdir = '.'
blddir = 'build'
includedir = 'include'

def options(opt):
	opt.load('compiler_c')
	
	# add library variant options to opt.

	lopt = opt.add_option_group('Library variant options')
	# generic options
	lopt.add_option('', '--enable-all',
		action = 'store_true',
		dest = 'enable_all',
		help = 'enable all the variants')
	
	# variant enabler
	lopt.add_option('', '--enable-seq',
		action = 'append',
		dest = 'seq_list',
		help = 'input sequence format variants (ascii, seq2, seq2p8, etc) [ascii]')
	lopt.add_option('', '--enable-aln',
		action = 'append',
		dest = 'aln_list',
		help = 'output format variants (ascii, cigar, etc) [ascii]')
	lopt.add_option('', '--enable-alg',
		action = 'append',
		dest = 'alg_list',
		help = 'algorithm variants [sw, nw, sea]')
	lopt.add_option('', '--enable-dp',
		action = 'append',
		dest = 'dp_list',
		help = 'dp matrix type variants [full, banded]')
	lopt.add_option('', '--enable-simd',
		action = 'append',
		dest = 'simd_list',
		help = 'SIMD variants [bit, (sse4), (avx2)]')
	lopt.add_option('', '--enable-variant',
		action = 'append',
		dest = 'variant_list',
		help = 'variants to be included in the object (naive_linear_banded, etc)')
	lopt.add_option('', '--enable-binding',
		action = 'append',
		dest = 'binding_list',
		help = 'bindings to be build (python, ruby, java, etc)')

	# Examples, unittests, and benchmarks
	eopt = opt.add_option_group('Benchmark and example options')
	eopt.add_option('', '--enable-bench',
		action = 'store_true',
		dest = 'enable_bench',
		help = 'build benchmarking program (default)')
	eopt.add_option('', '--enable-unittest',
		action = 'store_true',
		dest = 'enable_unittest',
		help = 'build unittest program (default)')
	eopt.add_option('', '--enable-example',
		action = 'store_true',
		dest = 'enable_example',
		help = 'build example program (default)')

	# bindings
	bopt = opt.add_option_group('Binding library build options')
	bopt.add_option('', '--enable-bindings',
		action = 'store_true',
		dest = 'enable_bindings',
		help = 'build all bindings (default)')
	bopt.add_option('', '--enable-java',
		action = 'store_true',
		dest = 'enable_java',
		help = 'build libsea.jar')

	# variant disabler
	from optparse import SUPPRESS_HELP
	lopt.add_option('', '--disable-seq',
		action = 'append',
		dest = 'seq_disable_list',
		help = SUPPRESS_HELP)
	lopt.add_option('', '--disable-aln',
		action = 'append',
		dest = 'aln_disable_list',
		help = SUPPRESS_HELP)
	lopt.add_option('', '--disable-alg',
		action = 'append',
		dest = 'alg_disable_list',
		help = SUPPRESS_HELP)
	lopt.add_option('', '--disable-dp',
		action = 'append',
		dest = 'dp_disable_list',
		help = SUPPRESS_HELP)
	lopt.add_option('', '--disable-simd',
		action = 'append',
		dest = 'simd_disable_list',
		help = SUPPRESS_HELP)
	lopt.add_option('', '--disable-variant',
		action = 'append',
		dest = 'variant_disable_list',
		help = SUPPRESS_HELP)
	lopt.add_option('', '--disable-binding',
		action = 'append',
		dest = 'binding_disable_list',
		help = SUPPRESS_HELP)

	# Examples, unittests, and benchmarks
	eopt.add_option('', '--disable-bench',
		action = 'store_false',
		dest = 'enable_bench',
		help = SUPPRESS_HELP)
	eopt.add_option('', '--disable-unittest',
		action = 'store_false',
		dest = 'enable_unittest',
		help = SUPPRESS_HELP)
	eopt.add_option('', '--disable-example',
		action = 'store_true',
		dest = 'enable_example',
		help = SUPPRESS_HELP)

	# bindings
	bopt.add_option('', '--disable-bindings',
		action = 'store_false',
		dest = 'enable_bindings',
		help = SUPPRESS_HELP)
	bopt.add_option('', '--disable-java',
		action = 'store_false',
		dest = 'enable_java',
		help = SUPPRESS_HELP)

def configure(conf):
	from util import alnbld
	from waflib.Tools.compiler_c import c_compiler
	# compiler settings
	# change priority
	c_compiler['darwin'] = ['icc', 'clang', 'gcc']
	c_compiler['linux'] = ['icc', 'gcc', 'clang']
	c_compiler['windows'] = ['cl', 'gcc', 'clang']

	conf.load('compiler_c')

	conf.env.append_value('CFLAGS', '-g')
	conf.env.append_value('CFLAGS', '-Wall')
	conf.env.append_value('CFLAGS', '-O3')
	conf.env.append_value('CFLAGS', '-std=c99')
	conf.env.append_value('CFLAGS', '-D_POSIX_C_SOURCE=200112L')	# for posix_memalign and clock_gettime
	conf.env.append_value('CFLAGS', '-fPIC')

	if conf.env.CC_NAME == 'icc':
		# FIXME: dirty hack to pass '-diag-disable remark', current waf does not support space-separated options
		# note: https://groups.google.com/forum/#!topic/waf-users/TJKhe04HGQc
		conf.env.append_value('CFLAGS', '-diag-disable')
		conf.env.append_value('CFLAGS', 'remark')
	elif conf.env.CC_NAME == 'gcc':
		conf.env.append_value('CFLAGS', '-Wno-unused-variable')
		conf.env.append_value('CFLAGS', '-Wno-unused-but-set-variable')
		conf.env.append_value('CFLAGS', '-Wno-unused-result')
	elif conf.env.CC_NAME == 'clang':
		conf.env.append_value('CFLAGS', '-Wno-unused-variable')
	else:
		pass

	# check if clock_gettime is available on the machine (for benchmarking)
	conf.check_cc(header_name = 'time.h',
		define_name = 'HAVE_TIME_H',
		mandatory = False)
	conf.check_cc(header_name = 'sys/time.h',
		define_name = 'HAVE_SYS_TIME_H',
		mandatory = False)
	conf.check_cc(header_name = 'alloca.h',
		define_name = 'HAVE_ALLOCA',
		mandatory = False)
	conf.check_cc(lib = 'rt',
		uselib_store = 'RT',
		mandatory = False)
	conf.check_cc(fragment = '''
		#include <time.h>
		int main(void) {
			struct timespec ts;
			clock_gettime(CLOCK_REALTIME, &ts);
			return 0;
		}
		''',
		execute = True,
		lib = 'rt',
		define_name = 'HAVE_CLOCK_GETTIME',
		mandatory = False,
		msg = 'Checking for function clock_gettime')

	# check if alloca and pthread_attr_getstacksize are available

	# construct variant list from given options
	# first parse option lines into lists
	seq_list = []
	if conf.options.seq_list is not None:
		seq_list.extend([i.lower() for l in [e.split(',') for e in conf.options.seq_list] for i in l])
	else:
		seq_list.extend(['ascii'])
	aln_list = []
	if conf.options.aln_list is not None:
		aln_list.extend([i.lower() for l in [e.split(',') for e in conf.options.seq_list] for i in l])
	else:
		aln_list.extend(['ascii'])
	alg_list = []
	if conf.options.alg_list is not None:
		alg_list.extend([i.upper() for l in [e.split(',') for e in conf.options.alg_list] for i in l])
	else:
		alg_list.extend(['SW', 'NW', 'SEA'])

	# check if misc program can be build
	if conf.options.enable_bench is False:
		conf.env.ENABLE_BENCH = 'False'
	else:
		if conf.options.enable_bench is True:
			alg_list.append('SEA')
			conf.env.ENABLE_BENCH = 'True'
		elif 'SEA' in alg_list:
			conf.env.ENABLE_BENCH = 'True'

	if conf.options.enable_example is False:
		conf.env.ENABLE_EXAMPLE = 'False'
	else:
		if conf.options.enable_example is True:
			alg_list.append('SW')
			conf.env.ENABLE_EXAMPLE = 'True'
		elif 'SW' in alg_list:
			conf.env.ENABLE_EXAMPLE = 'True'

	if conf.options.enable_unittest is False:
		conf.env.ENABLE_UNITTEST = 'False'
	else:
		conf.env.ENABLE_UNITTEST = 'True'

	if conf.env.ENABLE_UNITTEST == 'True' or conf.env.ENABLE_BENCH == 'True':
		conf.env.ENABLE_TEST_OBJECTS = 'True'
	else:
		conf.env.ENABLE_TEST_OBJECTS = 'False'

	# make variant list
	conf.env.append_value('VARIANTS',
		alnbld.generate_variant_list([['SEQ', seq_list], ['ALN', aln_list], ['ALG', alg_list]]))

	conf.recurse('scalar')
	conf.recurse('bit')
	conf.recurse('simd')
	conf.recurse('bindings')
	conf.recurse('bench')

	if not conf.find_program('nm', mandatory = False):
		conf.env.ENABLE_TEST_OBJECTS = 'False'

	conf.write_config_header('config.h')

def build(bld):
	from util import alnbld

	bld.recurse('scalar')
	bld.recurse('bit')
	bld.recurse('simd')
	bld.recurse('bindings')
	bld.recurse('bench')

	# build shared library (to be installed).
	bld.shlib(
		source = ['sea.c'],
		target = 'sea',
		use = bld.env.OBJECTS)

	# build static library for tests and benchmarking.
	if bld.env.ENABLE_TEST_OBJECTS == 'True':
		bld.stlib(
			source = ['sea.c', 'util/tester.c', 'util/bench.c'],
			target = 'test',
			use = bld.env.TEST_OBJECTS,
			defines = 'BENCH=1')

	# generate unittest program source and compile it.
	if bld.env.ENABLE_UNITTEST == 'True':
		def testgen(task):
			from util.tester import generate
			generate(task.inputs[0].abspath(), task.outputs[0].abspath())
			return 0

		bld(rule = testgen,
			source = bld.path.find_or_declare('libtest.a'),
			target = 'unittest.c')
		bld.program(
			source = ['unittest.c'],
			target = 'unittest',
			use = 'test',
			lib = bld.env.LIB_RT,
			install_path = './build')

	# build benchmarking program and example program if enable flag is set.
	if bld.env.ENABLE_EXAMPLE == 'True':
		bld.program(
			source = ['example.c'],
			target = 'example',
			use = 'sea',
			includes = 'include',
			install_path = './build')

	bld.install_files('${PREFIX}/include', 'include/sea.h')

def test(self):
	from waflib import Options
	lst = ['build', 'runtest']
	Options.commands = lst + Options.commands

def runtest(self):
	import subprocess
	subprocess.call('build/unittest')

def bench(self):
	from waflib import Options
	lst = ['configure', 'build', 'runbench']
	Options.commands = lst + Options.commands

def runbench(self):
	import subprocess
	subprocess.call('build/bench/benchmark')

def shutdown(ctx):
	pass
