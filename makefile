#!/usr/bin/make -f
# Waf Makefile wrapper
WAF_HOME=/Users/suzukihajime/Documents/aln

all:
	@/Users/suzukihajime/Documents/aln/waf build

all-debug:
	@/Users/suzukihajime/Documents/aln/waf -v build

all-progress:
	@/Users/suzukihajime/Documents/aln/waf -p build

install:
	/Users/suzukihajime/Documents/aln/waf install --yes;

uninstall:
	/Users/suzukihajime/Documents/aln/waf uninstall

clean:
	@/Users/suzukihajime/Documents/aln/waf clean

distclean:
	@/Users/suzukihajime/Documents/aln/waf distclean
	@-rm -rf build
	@-rm -f Makefile

check:
	@/Users/suzukihajime/Documents/aln/waf check

dist:
	@/Users/suzukihajime/Documents/aln/waf dist

test:
	@/Users/suzukihajime/Documents/aln/waf test

.PHONY: clean dist distclean check uninstall install all test

