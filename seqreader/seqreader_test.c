
#include <stdlib.h>
#include <time.h>
#include "../assert.h"
#include "../util.h"
#include "../sea_test.h"
#include "func.h"


#define SEQREADER_TEST
#include "seqreader.h"


void DECLARE_FUNC_GLOBAL(test, seqreader)(void)
{
	int i, j;
	int qpos, qstep, tpos, tstep;
	char c, tmp;
	char seq[TEST_SEQ_LEN] = TEST_SEQ;
	char encoded_seq[TEST_SEQ_LEN] = TEST_ENCODED_SEQ;
	int const len = TEST_SEQ_LEN;


	for(i = 0; i < 5; i++) {

		/* test forward reader */
#if SEQ_DIRQ==fwd
		qpos = i;
		qstep = 1;
#else
		qpos = len - i - 1;
		qstep = -1;
#endif

#if SEQ_DIRT==fwd
		tpos = i;
		tstep = 1;
#else
		tpos = len - i - 1;
		tstep = -1;
#endif
		INIT_SEQREADER(encoded_seq, qpos, encoded_seq, tpos);

		for(j = i; j < len; j++) {
			READQ_SEQ(c);
			INT_ENCODE_BASE(tmp, seq[qpos]);
//			#ifdef PARWORD
//			printf("%d, %d, %d, %d, %08lx, %08lx\n", i, j, c, tmp, _qbase[(_qpos/32) & (~0x01L)], _qbase[(_qpos/32) | (0x01L)]);
//			#endif
			assert(c == tmp);
			qpos += qstep;

			READT_SEQ(c);
			INT_ENCODE_BASE(tmp, seq[tpos]);
			assert(c == tmp);
			tpos += tstep;
		}
	}

	for(i = 0; i < 5; i++) {
		/* test reverse reader */
#if SEQ_DIRQ==fwd
		qpos = len - i - 1;
		qstep = -1;
#else
		qpos = i;
		qstep = 1;
#endif

#if SEQ_DIRT==fwd
		tpos = len - i - 1;
		tstep = -1;
#else
		tpos = i;
		tstep = 1;
#endif
		INIT_SEQREADER(encoded_seq, qpos, encoded_seq, tpos);

		for(j = i; j < len; j++) {
			READQ_SEQ_REV(c);
			INT_ENCODE_BASE(tmp, seq[qpos]);
			assert(c == tmp);
			qpos += qstep;

			READT_SEQ_REV(c);
			INT_ENCODE_BASE(tmp, seq[tpos]);
			assert(c == tmp);
			tpos += tstep;
		}
	}
	return;
}


void DECLARE_FUNC_GLOBAL(test, alnwriter)(void)
{
	return;
}

