

/* vector operation test */

#include <stdlib.h>
#include "../assert.h"
#include "../util.h"

#include "func.h"

#if BIT_WIDTH==4
	#include "diff_b4.h"
	#include "diag_b8.h"
#elif BIT_WIDTH==8
	#include "diag_b8.h"
#elif BIT_WIDTH==16
	#include "diag_b16.h"
#endif


#if ARCH==sse
	#include "x86_64/sse.h"
#elif ARCH==avx
	#include "x86_64/avx.h"
#endif


#define CELL_MAX_SIGNED 		( (1L<<((sizeof(CELL_TYPE) * 8) - 1)) - 1 )
#define CELL_MIN_SIGNED 		( (-1L<<((sizeof(CELL_TYPE) * 8) - 1)) )

#define CELL_MAX_UNSIGNED		( (1L<<(sizeof(CELL_TYPE) * 8)) - 1 )
#define CELL_MIN_UNSIGNED		(0L)

#define CHAR_MAX_UNSIGNED		(255)
#define CHAR_MIN_UNSIGNED		(0)

//#define DECLARE_FUNC_GLOBAL(test, a)	test##ARCH##_b##BIT_WIDTH##_c##BAND_WIDTH##_vec_##a
//#define CALL_TEST_FUNC(a)		test##ARCH##_b##BIT_WIDTH##_c##BAND_WIDTH##_vec_##a


#define PREPARE_CELL_TEST_VAR(a,b,c)	char *a, *b, *c, *p##a, *p##b, *p##c; \
										a = (char *)aligned_malloc(BYTES_PER_LINE, BYTES_PER_LINE); \
										b = (char *)aligned_malloc(BYTES_PER_LINE, BYTES_PER_LINE); \
										c = (char *)aligned_malloc(BYTES_PER_LINE, BYTES_PER_LINE); \
										DECLARE_VEC_CELL_REG(a##v); \
										DECLARE_VEC_CELL_REG(b##v); \
										DECLARE_VEC_CELL_REG(c##v); \
										INIT_PTR(a, b, c); \
										CELL_RAND_PATTERN(p##a); \
										CELL_RAND_PATTERN(p##b); \
										CELL_RAND_PATTERN(p##c);


#define PREPARE_CHAR_TEST_VAR(a,b,c)	char *a, *b, *c, *p##a, *p##b, *p##c; \
										a = (char *)aligned_malloc(BYTES_PER_LINE, BYTES_PER_LINE); \
										b = (char *)aligned_malloc(BYTES_PER_LINE, BYTES_PER_LINE); \
										c = (char *)aligned_malloc(BYTES_PER_LINE, BYTES_PER_LINE); \
										DECLARE_VEC_CHAR_REG(a##v); \
										DECLARE_VEC_CHAR_REG(b##v); \
										DECLARE_VEC_CHAR_REG(c##v); \
										INIT_PTR(a, b, c); \
										CHAR_RAND_PATTERN(p##a); \
										CHAR_RAND_PATTERN(p##b); \
										CHAR_RAND_PATTERN(p##c);


#define CLEAN_TEST_VAR(a,b,c)			free(a); \
										free(b); \
										free(c);
#define INIT_PTR(a, b, c)				{ p##a = a; p##b = b; p##c = c; }
#define INCR_PTR(pa, pb, pc)			{ pa += BYTES_PER_CELL; pb += BYTES_PER_CELL; pc += BYTES_PER_CELL; }

#define CELL_TEST_PATTERN(p,s,st)		{ for(int i = 0; i < BAND_WIDTH; i++) { \
										  SCORE(p) = ((unsigned long)((s) + (i) * (st)) % ((unsigned long)CELL_MAX_UNSIGNED + 1UL)); \
										  p += BYTES_PER_CELL; } }
#define CHAR_TEST_PATTERN(p,s,st)		{ for(int i = 0; i < BAND_WIDTH; i++) { \
										  *p++ = ((unsigned char)((s) + (i) * (st)) % ((unsigned long)CHAR_MAX_UNSIGNED + 1UL)); } }

#define CELL_RAND_PATTERN(p)			{ for(int i = 0; i < BAND_WIDTH; i++) { \
										  SCORE(p) = (rand() % CELL_MAX_UNSIGNED); \
										  p += BYTES_PER_CELL; } }
#define CHAR_RAND_PATTERN(p)			{ for(int i = 0; i < BAND_WIDTH; i++) { \
										  *p++ = (rand() % CHAR_MAX_UNSIGNED); } }

#define PRINT_VEC(p)					{ for(int i = 0; i < BAND_WIDTH; i++) { \
										  printf("%d\n", SCORE(p)); p += BYTES_PER_CELL; } }

int DECLARE_FUNC_GLOBAL(check, cell_equal)(char *a, char *b)
{
	int i;
	int eq = 0;
	char *dummy;
	for(int i = 0; i < BAND_WIDTH; i++) {
		if(SCORE(a) != SCORE(b)) { eq = 1; }
		INCR_PTR(a, b, dummy);
	}
	return(!eq);
}

int DECLARE_FUNC_GLOBAL(check, char_equal)(char *a, char *b)
{
	int i;
	for(int i = 0; i < BAND_WIDTH; i++) {
		if(*a++ != *b++) { return 0; }
	}
	return 1;
}

void DECLARE_FUNC_GLOBAL(test, store)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_STORE(pc, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

#ifdef DIFF_CAPABLE

void DECLARE_FUNC_GLOBAL(test, store_packed)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	for(int i = 0; i < BAND_WIDTH; i++) {
		SCORE(pa) = (SCORE(pa) & 0x0f);
		SCORE(pb) = (SCORE(pb) & 0x0f);
		INCR_PTR(pa, pb, pc);
	}

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_LOAD(pb, bv);
	VEC_STORE_PACKED(pc, av, bv);

	INIT_PTR(a, b, c);
	for(int i = 0; i < BAND_WIDTH; i++) {
		SCORE(pa) = SCORE(pa) | (SCORE(pb)<<4);
		INCR_PTR(pa, pb, pc);
	}

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

#endif

void DECLARE_FUNC_GLOBAL(test, assign)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_ASSIGN(cv, av);
	VEC_STORE(pc, cv);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, set)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	CELL_TEST_PATTERN(pa, 20, 0);
	INIT_PTR(a, b, c);
	VEC_SET(cv, 20);
	VEC_STORE(pc, cv);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, setzero)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	CELL_TEST_PATTERN(pa, 0, 0);
	INIT_PTR(a, b, c);
	VEC_SETZERO(cv);
	VEC_STORE(pc, cv);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, setones)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	CELL_TEST_PATTERN(pa, CELL_MAX_UNSIGNED, 0);
	INIT_PTR(a, b, c);
	VEC_SETONES(cv);
	VEC_STORE(pc, cv);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, set_uhalf)(void)
{
	int n;
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	n = rand() % CELL_MAX_UNSIGNED;
	for(int i = 0; i < BAND_WIDTH/2; i++) {
		SCORE(pa) = 0; pa += BYTES_PER_CELL;
	}
	for(int i = 0; i < BAND_WIDTH/2; i++) {
		SCORE(pa) = n; pa += BYTES_PER_CELL;
	}
	INIT_PTR(a, b, c);
	VEC_SET_UHALF(cv, n);
	VEC_STORE(pc, cv);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, set_lhalf)(void)
{
	int n;
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	n = rand() % CELL_MAX_UNSIGNED;
	for(int i = 0; i < BAND_WIDTH/2; i++) {
		SCORE(pa) = n; pa += BYTES_PER_CELL;
	}
	for(int i = 0; i < BAND_WIDTH/2; i++) {
		SCORE(pa) = 0; pa += BYTES_PER_CELL;
	}
	INIT_PTR(a, b, c);
	VEC_SET_LHALF(cv, n);
	VEC_STORE(pc, cv);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

#ifdef DIAG_CAPABLE

void DECLARE_FUNC_GLOBAL(test, init_v)(void)
{
	int m, g;
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	m = rand() % 4;
	g = -1 * (rand() % 4);
	for(int i = 0; i < BAND_WIDTH/2; i++) {
		SCORE(pa) = g + (BAND_WIDTH/2-i-1)*(2*g - m);
		pa += BYTES_PER_CELL;
	}
	for(int i = 0; i < BAND_WIDTH/2; i++) {
		SCORE(pa) = g + i*(2*g - m);
		pa += BYTES_PER_CELL;
	}
	INIT_PTR(a, b, c);
	VEC_INIT_V(cv, m, g);
	VEC_STORE(pc, cv);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, init_pv)(void)
{
	int m, g;
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	m = rand() % 4;
	g = -1 * (rand() % 4);
	for(int i = 0; i < BAND_WIDTH/2; i++) {
		SCORE(pa) = g + (BAND_WIDTH/2-i)*(2*g - m);
		pa += BYTES_PER_CELL;
	}
	SCORE(pa) = 0; pa += BYTES_PER_CELL;
	for(int i = 0; i < BAND_WIDTH/2-1; i++) {
		SCORE(pa) = g + (i + 1)*(2*g - m);
		pa += BYTES_PER_CELL;
	}
	INIT_PTR(a, b, c);
	VEC_INIT_PV(cv, m, g);
	VEC_STORE(pc, cv);
	
	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

#endif

void DECLARE_FUNC_GLOBAL(test, insert_msb)(void)
{
	int n;
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	n = rand() % CELL_MAX_UNSIGNED;
	VEC_LOAD(pa, av);
	VEC_INSERT_MSB(av, n);
	VEC_STORE(pc, av);
	SCORE(a + (BAND_WIDTH-1) * BYTES_PER_CELL) = n;

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, insert_lsb)(void)
{
	int n;
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	n = rand() % CELL_MAX_UNSIGNED;
	VEC_LOAD(pa, av);
	VEC_INSERT_LSB(av, n);
	VEC_STORE(pc, av);
	SCORE(a) = n;

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, msb)(void)
{
	int n;
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	n = VEC_MSB(av);
	INIT_PTR(a, b, c);
	assert(n == (unsigned CELL_TYPE)SCORE(a + (BAND_WIDTH-1) * BYTES_PER_CELL));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, lsb)(void)
{
	int n;
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	n = VEC_LSB(av);
	INIT_PTR(a, b, c);
	assert(n == (unsigned CELL_TYPE)SCORE(a));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, or)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	for(int i = 0; i < BAND_WIDTH; i++) {
		SCORE(pc) = SCORE(pa) | SCORE(pb);
		INCR_PTR(pa, pb, pc);
	}

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_LOAD(pb, bv);
	VEC_OR(av, av, bv);
	INIT_PTR(a, b, c);
	VEC_STORE(pa, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, add)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	for(int i = 0; i < BAND_WIDTH; i++) {
		SCORE(pc) = SCORE(pa) + SCORE(pb);
		INCR_PTR(pa, pb, pc);
	}

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_LOAD(pb, bv);
	VEC_ADD(av, av, bv);
	INIT_PTR(a, b, c);
	VEC_STORE(pa, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}


void DECLARE_FUNC_GLOBAL(test, adds)(void)
{
	unsigned long res;
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	for(int i = 0; i < BAND_WIDTH; i++) {
		res = ((unsigned long)SCORE(pa) & CELL_MAX_UNSIGNED)
			+ ((unsigned long)SCORE(pb) & CELL_MAX_UNSIGNED);
		if(res > CELL_MAX_UNSIGNED) {
			res = CELL_MAX_UNSIGNED;
		}
		SCORE(pc) = (CELL_TYPE)res;
		INCR_PTR(pa, pb, pc);
	}

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_LOAD(pb, bv);
	VEC_ADDS(av, av, bv);
	INIT_PTR(a, b, c);
	VEC_STORE(pa, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, sub)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	for(int i = 0; i < BAND_WIDTH; i++) {
		SCORE(pc) = SCORE(pa) - SCORE(pb);
		INCR_PTR(pa, pb, pc);
	}

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_LOAD(pb, bv);
	VEC_SUB(av, av, bv);
	INIT_PTR(a, b, c);
	VEC_STORE(pa, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, subs)(void)
{
	long res;
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	for(int i = 0; i < BAND_WIDTH; i++) {
		res = ((unsigned long)SCORE(pa) & CELL_MAX_UNSIGNED)
			- ((unsigned long)SCORE(pb) & CELL_MAX_UNSIGNED);
		if(res < CELL_MIN_UNSIGNED) {
			res = CELL_MIN_UNSIGNED;
		}
		SCORE(pc) = (CELL_TYPE)res;
		INCR_PTR(pa, pb, pc);
	}

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_LOAD(pb, bv);
	VEC_SUBS(av, av, bv);
	INIT_PTR(a, b, c);
	VEC_STORE(pa, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, max)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	for(int i = 0; i < BAND_WIDTH; i++) {
		SCORE(pc) = (SCORE(pa) > SCORE(pb)) ? SCORE(pa) : SCORE(pb);
		INCR_PTR(pa, pb, pc);
	}

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_LOAD(pb, bv);
	VEC_MAX(av, av, bv);
	INIT_PTR(a, b, c);
	VEC_STORE(pa, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, min)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	for(int i = 0; i < BAND_WIDTH; i++) {
		SCORE(pc) = (SCORE(pa) < SCORE(pb)) ? SCORE(pa) : SCORE(pb);
		INCR_PTR(pa, pb, pc);
	}

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_LOAD(pb, bv);
	VEC_MIN(av, av, bv);
	INIT_PTR(a, b, c);
	VEC_STORE(pa, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, shift_r)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	for(int i = 0; i < BAND_WIDTH-1; i++) {
		pa += BYTES_PER_CELL;
		SCORE(pc) = SCORE(pa);
		pb += BYTES_PER_CELL;
		pc += BYTES_PER_CELL;
	}
	SCORE(pc) = 0;

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_LOAD(pb, bv);
	VEC_SHIFT_R(av);
	INIT_PTR(a, b, c);
	VEC_STORE(pa, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, shift_l)(void)
{
	PREPARE_CELL_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	SCORE(pc) = 0;
	for(int i = 0; i < BAND_WIDTH-1; i++) {
		pc += BYTES_PER_CELL;
		SCORE(pc) = SCORE(pa);
		pa += BYTES_PER_CELL;
		pb += BYTES_PER_CELL;
	}

	INIT_PTR(a, b, c);
	VEC_LOAD(pa, av);
	VEC_LOAD(pb, bv);
	VEC_SHIFT_L(av);
	INIT_PTR(a, b, c);
	VEC_STORE(pa, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, cell_equal)(a, c));
	assert(0 == CALL_FUNC(check, cell_equal)(a, b));
	assert(0 == CALL_FUNC(check, cell_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, char_shift_r)(void)
{
	PREPARE_CHAR_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	for(int i = 0; i < BAND_WIDTH-1; i++) {
		*pc++ = *++pa;
	}
	*pc = 0;

	INIT_PTR(a, b, c);
	VEC_CHAR_LOAD(pa, av);
	VEC_CHAR_LOAD(pb, bv);
	VEC_CHAR_SHIFT_R(av);
	INIT_PTR(a, b, c);
	VEC_CHAR_STORE(pa, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, char_equal)(a, c));
	assert(0 == CALL_FUNC(check, char_equal)(a, b));
	assert(0 == CALL_FUNC(check, char_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, char_shift_l)(void)
{
	PREPARE_CHAR_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	*pc = 0;
	for(int i = 0; i < BAND_WIDTH-1; i++) {
		*++pc = *pa++;
	}

	INIT_PTR(a, b, c);
	VEC_CHAR_LOAD(pa, av);
	VEC_CHAR_LOAD(pb, bv);
	VEC_CHAR_SHIFT_L(av);
	INIT_PTR(a, b, c);
	VEC_CHAR_STORE(pa, av);

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, char_equal)(a, c));
	assert(0 == CALL_FUNC(check, char_equal)(a, b));
	assert(0 == CALL_FUNC(check, char_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, char_insert_msb)(void)
{
	PREPARE_CHAR_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	VEC_CHAR_LOAD(pa, av);
	VEC_CHAR_INSERT_MSB(av, 10);
	VEC_CHAR_STORE(pc, av);
	a[BAND_WIDTH-1] = 10;

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, char_equal)(a, c));
	assert(0 == CALL_FUNC(check, char_equal)(a, b));
	assert(0 == CALL_FUNC(check, char_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, char_insert_lsb)(void)
{
	PREPARE_CHAR_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	VEC_CHAR_LOAD(pa, av);
	VEC_CHAR_INSERT_LSB(av, 10);
	VEC_CHAR_STORE(pc, av);
	*a = 10;

	INIT_PTR(a, b, c);
	assert(1 == CALL_FUNC(check, char_equal)(a, c));
	assert(0 == CALL_FUNC(check, char_equal)(a, b));
	assert(0 == CALL_FUNC(check, char_equal)(b, c));

	CLEAN_TEST_VAR(a, b, c);
	return;
}

void DECLARE_FUNC_GLOBAL(test, compare_select)(void)
{
	PREPARE_CELL_TEST_VAR(ca, cb, cc);
	PREPARE_CHAR_TEST_VAR(a, b, c);

	INIT_PTR(a, b, c);
	INIT_PTR(ca, cb, cc);
	for(int i = 0; i < BAND_WIDTH; i++) {
		SCORE(pca) = 10; SCORE(pcb) = 2;
		*pa = (i % 2) ? *pa : *pb;
		INCR_PTR(pca, pcb, pcc);
		pa++; pb++;
	}

	INIT_PTR(a, b, c);
	INIT_PTR(ca, cb, cc);
	VEC_CHAR_LOAD(pa, av);
	VEC_CHAR_LOAD(pb, bv);
	VEC_COMPARE(ccv, av, bv);

	VEC_LOAD(pca, cav);
	VEC_LOAD(pcb, cbv);
	VEC_SELECT(ccv, cav, cbv, ccv);
	INIT_PTR(ca, cb, cc);
	VEC_STORE(pcc, ccv);

	for(int i = 0; i < BAND_WIDTH; i++) {
		SCORE(pca) = (i % 2) ? 10 : 2;
		INCR_PTR(pca, pcb, pcc);
	}

	assert(1 == CALL_FUNC(check, cell_equal)(ca, cc));
	assert(0 == CALL_FUNC(check, cell_equal)(ca, cb));
	assert(0 == CALL_FUNC(check, cell_equal)(cb, cc));

	CLEAN_TEST_VAR(a, b, c);
	CLEAN_TEST_VAR(ca, cb, cc);
}

#if 0
int main(void)
{
	int i;

	for(int i = 0; i < 10000; i++) {
		CALL_TEST_FUNC(store)();
	#ifdef DIFF_CAPABLE
		CALL_TEST_FUNC(store_packed)();
	#endif
		CALL_TEST_FUNC(assign)();
		CALL_TEST_FUNC(set)();
		CALL_TEST_FUNC(setzero)();
		CALL_TEST_FUNC(setones)();
		CALL_TEST_FUNC(set_uhalf)();
		CALL_TEST_FUNC(set_lhalf)();
	#ifdef DIAG_CAPABLE
		CALL_TEST_FUNC(init_v)();
		CALL_TEST_FUNC(init_pv)();
	#endif
		CALL_TEST_FUNC(insert_msb)();
		CALL_TEST_FUNC(insert_lsb)();
		CALL_TEST_FUNC(or)();
		CALL_TEST_FUNC(add)();
		CALL_TEST_FUNC(adds)();
		CALL_TEST_FUNC(sub)();
		CALL_TEST_FUNC(subs)();
		CALL_TEST_FUNC(max)();
		CALL_TEST_FUNC(min)();
		CALL_TEST_FUNC(shift_r)();
		CALL_TEST_FUNC(shift_l)();
	//	CALL_TEST_FUNC(compare_select)();

		CALL_TEST_FUNC(char_shift_r)();
		CALL_TEST_FUNC(char_shift_l)();
		CALL_TEST_FUNC(char_insert_msb)();
		CALL_TEST_FUNC(char_insert_lsb)();
	}
	return 0;
}
#endif
