
/**
 * @file simd.h
 *
 * @brief a header for SIMD algorithms.
 *
 * @detail
 * the Diag and Diff algorithms:
 *
 * Diag algorithms are the SIMD-based banded alignment algorithms. Diff algorithms are
 * the difference-based variant of the diag algorithms. The difference between the two 
 * types of algorithms are an usage of memory, the overhead of maximum score search, and
 * the limitation of the length of the query sequence. Generally, the diff algorithms
 * use the same or less amount of memory than the diag algorithms, since the scores are
 * represented in the difference between adjacent cells. The diff algorithms have no 
 * limitation on the length of the query sequence, since the bit-width of the difference 
 * value is not determined by the length of the queries, but by the scoring schemes.
 * However, the disadvantage of the diff algorithms are an overhead of the maximum score
 * search, which imposes a constant amount of computation after the fill-in steps 
 * (or before the traceback steps).
 *
 * The diff algorithms are suitable in alignment of long queries, where the overhead of
 * the maximum score search step becomes relatively small in the total calculation.
 * The threshold of the query length are mentioned in the comment on simd_get_sea
 * function.
 */
#ifndef _SIMD_H_INCLUDED
#define _SIMD_H_INCLUDED

#include "../include/sea.h"

/**
 * @fn simd_get_sea
 * 
 * @brief retrieve a pointer to alignment function.
 *
 * @param[in] flags : option flag bitfield. see parameter of sea_init in sea.h
 * @param[in] len : the maximum length of the query sequence. (unused in this function.)
 * @param[in] alg_type : an algorithm index. see table.th and sea.h.
 * @param[in] seq_type : an input sequence type index. see table.th and sea.h.
 * @param[in] aln_type : an output alignment type index. see table.th and sea.h.
 * @param[in] scmat : the scores in the dynamic programming. scmat[0] = M, scmat[1] = X, scmat[2] = Gi, and scmat[3] = Ge. see sea.h.
 * @param[in] xdrop : the xdrop threshold in positive integer.
 * @param[out] sea_matsize : a pointer to a function which returns the size of a matrix.
 * @param[out] sea_sea : a pointer to a function which do an alignment.
 *
 * @return TRUE if proper pointer is set, FALSE if not.
 *
 * @detail
 * This function selects an appropriate alignment function from the variants of
 * diag and diff algorithms. The supported options are;
 * algorithms: the banded SW, SEA, and NW.
 * bandwidth: 16 or 32.
 * The other algorithms, such as full dynamic programming matrix, are not supported.
 * The selection of the algorithm is done based on the chart shown below.
 *
 * SIMD algorithm selection chart; A = min(1000, 32768 / M) for x86_64 architectures.
 * +-----------------------------------------------------+
 * |              |              algorithm               |
 * |              |     SW     |     SEA    |     NW     |
 * |--------------+------------+------------+------------+
 * |    len < A   |    diag    |    diag    |    diag    |
 * |--------------+------------+------------+------------+
 * |    A < len   |    diag    |    diff    |    diff    |
 * +--------------+------------+------------+------------+
 *
 * Notice:
 * The function selector for diag algorithms always use 16-bit wide algorithms. If you want to
 * use 8-bit wide cells, you have to initialize alignment context with sea_init_fp, giving
 * proper function pointer to 8-bit wide algorithm (e.g. diag_linear_banded_sse_8_32).
 */
sea_int_t simd_get_sea(
	struct sea_funcs *fp,
	struct sea_params param,
	sea_int_t len,
	sea_int_t alg_type, sea_int_t seq_type, sea_int_t aln_type);

#endif /* #ifndef _SIMD_H_INCLUDED */

/** 
 * end of simd.h
 */
