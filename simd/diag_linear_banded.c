
/**
 * @file diag_linear_banded.c
 *
 * @brief a linear-gap cost, banded SIMD implementation.
 *
 * @detail
 * This implementation is a vectorized variant of naive_linear_banded.c.
 * The consistency of the results are tested in the test_cross_diag_linear_banded
 * function.
 *
 * This file requires some constant definition to compile. See naive_linear_full.c
 * and table.th for details of required definitions.
 */
#ifdef DEBUG
 	#include <stdio.h>
#endif
#ifdef MAIN
 	#include <stdio.h>
 	#include <string.h>
#endif
#ifdef TEST 
 	#include "../util/tester.h"
#endif

#include <stdlib.h>						/* for definition of the NULL */
#include "../include/sea.h"				/* for API definitions, flag definitions, and structs */
#include "../util/util.h"				/* for internal utility functions */
#include "../seqreader/seqreader.h"		/* for input sequence reader and output alignment writer */
#include "../util/bench.h"				/* for benchmarking */
#include "diag_variant.h"

/**
 * function declarations
 */
struct mpos
DECLARE_FUNC(diag_linear_banded_fill, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	char *mat);

struct mpos
DECLARE_FUNC(diag_linear_banded_search, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	char *mat,
	struct mpos o);

sea_int_t
DECLARE_FUNC(diag_linear_banded_trace, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	char *mat,
	struct mpos o);

/**
 * @fn diag_linear_banded
 *
 * @brief a linear-gap cost banded SIMD implementation.
 *
 * @param[ref] aln : a pointer to a sea_result structure. aln must NOT be NULL. (a structure which contains an alignment result.)
 * @param[in] aln->a : a pointer to a query sequence a.
 * @param[inout] aln->apos : (input) the start position of the search section on sequence a. (0 <= apos < length(sequence a)) (output) the start position of the alignment.
 * @param[inout] aln->alen : (input) the length of the search section on sequence a. (0 < alen) (output) the length of the alignment.
 * @param[in] aln->b : a pointer to a query sequence b.
 * @param[inout] aln->bpos : same as apos.
 * @param[inout] aln->blen : same as alen.
 *
 * @param[out] aln->aln : a pointer to the alignment string.
 * @param[inout] aln->len : (input) the reserved length of the aln->aln. (output) the length of the alignment string.
 * @param[out] aln->score : an alignment score.
 *
 * @param[in] scmat : unused parameter.
 * @param[in] xdrop : unused parameter. 
 * @param[in] bandwidth : unused parameter.
 * @param[in] mat : a pointer to the DP matrix.
 * @param[in] matsize : the size of the DP matrix.
 *
 * @return status. zero means success. see sea.h for the other error code.
 */
sea_int_t
DECLARE_FUNC_GLOBAL(diag_linear_banded, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	void *mat)
{
	sea_int_t retval = SEA_ERROR;
	struct mpos o;
	DECLARE_BENCH(fill);
	DECLARE_BENCH(search);
	DECLARE_BENCH(trace);

	/**
	 * fill in a matrix
	 */
	START_BENCH(fill);
	o = CALL_FUNC(diag_linear_banded_fill, SUFFIX)(
		aln, param,
		(char *)mat + BYTES_PER_LINE);
	END_BENCH(fill);
	/**
	 * search maximum score position
	 */
	START_BENCH(search);
	o = CALL_FUNC(diag_linear_banded_search, SUFFIX)(
		aln, param,
		(char *)mat + BYTES_PER_LINE,
		o);
	END_BENCH(search);

#ifdef DEBUG
	fprintf(stderr, "ldmat, %d, %d\n", aln->alen+BAND_WIDTH, aln->blen+BAND_WIDTH);
	for(int i = 0; i < aln->alen+1; i++) {
		for(int j = 0; j < aln->blen+1; j++) {
			fprintf(stderr, "%d, %d, %d, %d, %d\n", i, j, COP(i, j), COQ(i, j), SCORE((char *)mat + ADDR(COP(i, j), COQ(i, j))));
		}
	}
	fprintf(stderr, "\n");
#endif
	/**
	 * if aln->aln is not NULL and score did not overflow, do traceback.
	 */
	if(o.m.p == -1) { return SEA_ERROR_OVERFLOW; }
	if(aln->aln == NULL) { return SEA_SUCCESS; }
	START_BENCH(trace);
	retval = CALL_FUNC(diag_linear_banded_trace, SUFFIX)(
		aln, param,
		(char *)mat + BYTES_PER_LINE,
		o);
	END_BENCH(trace);
	return(retval);
}

/**
 * @fn diag_linear_banded_fill
 *
 * @brief a matrix fill-in function.
 *
 * @param[ref] aln : a pointer to a sea_result struct.
 * @param[in] xdrop : currently unused.
 * @param[in] mat : a pointer to a matrix.
 * @param[in] matlen : the length of the p-direction of the matrix.
 *
 * @return an end position of the extension.
 */
struct mpos
DECLARE_FUNC(diag_linear_banded_fill, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	char *mat)
{
	sea_int_t const bw = BAND_WIDTH;
	sea_int_t i, j;
	sea_int_t m = param.m,
			  x = param.x,
			  g = param.gi,
			  xdrop = param.xdrop;
	sea_int_t apos = 0,				/** seq.a read position */
			  bpos = 0;				/** seq.b read position */
	sea_int_t alen = aln->alen,
			  blen = aln->blen;
	sea_int_t alim = alen + bw/2,
			  blim = blen + bw/2;
	struct mpos o = {{0, 0, 0, 0}, {0, 0, 0, 0}};

	/**
	 * declarations of SIMD registers.
	 */
	DECLARE_VEC_CELL_REG(mv);		/** (m, m, m, ..., m) */
	DECLARE_VEC_CELL_REG(xv);		/** (x, x, x, ..., x) */
	DECLARE_VEC_CELL_REG(gv);		/** (g, g, g, ..., g) */
	DECLARE_VEC_CHAR_REG(wq);		/** a buffer for seq.a */
	DECLARE_VEC_CHAR_REG(wt);		/** a buffer for seq.b */
	DECLARE_VEC_CELL_REG(v);		/** score vector */
	DECLARE_VEC_CELL_REG(pv);		/** previous score vector */
	DECLARE_VEC_CELL_REG(tmp1);		/** temporary */
	DECLARE_VEC_CELL_REG(tmp2);		/** temporary */
	DECLARE_VEC_CELL_REG(maxv);		/** a vector which holds maximum scores */
 	#if ALG == SW
	 	DECLARE_VEC_CELL_REG(zv);	/** zero vector for the SW algorithm */
	#endif

	/**
	 * seqreaders
	 */
	DECLARE_SEQ(a);
	DECLARE_SEQ(b);

	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);

	VEC_SET(mv, m);					/** initialize mv vector with m */
	VEC_SET(xv, x);					/** xv with x */
	VEC_SET(gv, g);					/** gv with g */
	VEC_SET(maxv, CELL_MIN);		/** max score vector */
	#if ALG == SW
		VEC_SET(zv, 0);				/** zero vector */
	#endif

	VEC_SET(pv, CELL_MIN);			/** phantom vector at p = -1 */
	VEC_INIT_PVN(v, m, g, i);		/** initialize vector at p = 0 */
	VEC_STORE(mat, v);
	VEC_MAX(maxv, maxv, v);

	/**
	 * pre feed of sequences
	 */
	VEC_CHAR_SETZERO(wq);			/** padding of the seq.a is 0 */
	VEC_CHAR_SETONES(wt);			/** padding of the seq.b is 0xff */
	for(apos = 0; apos < bw/2; apos++) {
		FETCH(a, apos); PUSHQ((apos < alen) ? DECODE(a) : 0,    wq);
	}
	for(bpos = 0; bpos < bw/2-1; bpos++) {
		FETCH(b, bpos); PUSHT((bpos < blen) ? DECODE(b) : 0xff, wt);
	}

	/**
	 * @macro VEC_SW_MAX
	 * @brief vector max operation enabled only when ALG == SW
	 */
	#if ALG == SW
	 	#define VEC_SW_MAX(a, b, c)		VEC_MAX(a, b, c);
	#elif ALG == NW || ALG == SEA || ALG == XSEA
	 	#define VEC_SW_MAX(a, b, c)		;
	#endif

	/**
	 * @macro VEC_SEA_MAX
	 * @brief vector max operation enabled only when ALG != NW
	 */
	#if ALG == SW || ALG == SEA || ALG == XSEA
	 	#define VEC_SEA_MAX(a, b, c)	VEC_MAX(a, b, c);
	#elif ALG == NW
	 	#define VEC_SEA_MAX(a, b, c)	;
	#endif
	
	/**
	 * @macro DIAG_LINEAR_BANDED_REGISTER_UPDATE
	 * @brief a set of band advancing operations.
	 */
	#define DIAG_LINEAR_BANDED_REGISTER_UPDATE() { \
	 	VEC_ADD(tmp1, v, gv); \
		VEC_ADD(tmp2, tmp2, gv); \
		VEC_MAX(tmp1, tmp1, tmp2); \
		VEC_COMPARE(tmp2, wq, wt); \
		VEC_SELECT(tmp2, xv, mv, tmp2); \
		VEC_ADD(tmp2, pv, tmp2); \
		VEC_SW_MAX(tmp1, zv, tmp1); \
		VEC_MAX(tmp1, tmp1, tmp2); \
		VEC_ASSIGN(pv, v); \
		VEC_ASSIGN(v, tmp1); \
		VEC_SEA_MAX(maxv, maxv, v); \
	}

	i = 0; j = 0;							/** the center cell of the init vector */
	while(i < alim && j < blim) {
		j += 1;								/** go downward in the former half*/
		VEC_ASSIGN(tmp2, v); VEC_SHIFT_R(tmp2); VEC_INSERT_MSB(tmp2, CELL_MIN);
		FETCH(b, bpos); PUSHT((bpos < blen) ? DECODE(b) : 0xff, wt); bpos++;
		DIAG_LINEAR_BANDED_REGISTER_UPDATE();
		VEC_STORE(mat, v);

		i += 1;								/** go rightward in the latter half */		
		VEC_ASSIGN(tmp2, v); VEC_SHIFT_L(tmp2); VEC_INSERT_LSB(tmp2, CELL_MIN);
		FETCH(a, apos); PUSHQ((apos < alen) ? DECODE(a) : 0, wq); apos++;
		DIAG_LINEAR_BANDED_REGISTER_UPDATE();
		VEC_STORE(mat, v);
		
		if(ALG == XSEA && VEC_CENTER(v) + xdrop - VEC_CENTER(maxv) < 0) { break; }
	}
	#undef VEC_SW_MAX
	#undef VEC_SEA_MAX
	#undef DIAG_LINEAR_BANDED_REGISTER_UPDATE
	
	aln->len = COP(alim, blim);
	o.e.i = i; o.e.j = j;
	o.e.p = COP(i, j); o.e.q = COQ(i, j);		/** o.e.q == 0; */
	if(ALG != NW) {
		VEC_STORE(mat, maxv);
		VEC_ASSIGN(tmp1, maxv);
		for(i = 1; i < bw; i++) {
			VEC_SHIFT_R(tmp1);
			VEC_MAX(maxv, tmp1, maxv);		/** extract maximum score in the maxv vector */
		}
		VEC_STORE(mat, maxv);				/** store max vector at the end of the memory */
	}
	return(o);
}

/**
 * @fn diag_linear_banded_search
 *
 * @brief search a cell with maximal score.
 *
 * @return a struct mpos which contains maximal score position.
 */
struct mpos
DECLARE_FUNC(diag_linear_banded_search, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	char *mat,
	struct mpos o)
{
	sea_int_t const bw = BAND_WIDTH,
					bc = BYTES_PER_CELL,
					bl = BYTES_PER_LINE;

	if(ALG == NW) {
		o.m.i = aln->alen; o.m.j = aln->blen;
		o.m.p = COP(aln->alen, aln->blen); o.m.q = COQ(aln->alen, aln->blen);
	} else { /** the Smith-Waterman or the seed-and-extend */
		sea_int_t p, q;
		char *lmat = mat + ADDR(o.e.p+1, -bw/2),
			 *tmat;					
		sea_int_t max = SCORE(lmat + bl);

		o.m.i = o.m.j = o.m.p = o.m.q = 0;
		if(max == CELL_MAX) { o.m.p = -1; return(o); }
		if(max == 0) { return(o); } /** wind back to (0, 0) if no meaningful score was found */
		for(q = -bw/2; q < bw/2; q++, lmat += bc) {
			if(SCORE(lmat) == max) {
				for(p = o.e.p, tmat = lmat-bl;
					p >= 0 && SCORE(tmat) != max;
					p--, tmat -= bl) {}
				if(p >= o.m.p) { o.m.i = COX(p, q); o.m.j = COY(p, q); o.m.p = p; o.m.q = q; }
			}
		}
		aln->alen = o.m.i;
		aln->blen = o.m.j;
	}
	aln->score = SCORE(mat + ADDR(o.m.p, o.m.q));
	return(o);
}

/**
 * @fn diag_linear_banded_trace
 *
 * @brief traceback function.
 *
 * @param aln : a pointer to struct sea_result.
 * @param mat : a pointer to dynamic programming matrix.
 * @param matlen : the length of the p-coordinate of the matrix.
 * @param mpos : the start position of the trace.
 */
sea_int_t
DECLARE_FUNC(diag_linear_banded_trace, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	char *mat,
	struct mpos o)
{
	sea_int_t mi = o.m.i,
			  mj = o.m.j,
			  mp = o.m.p,
			  mq = o.m.q;
	sea_int_t m = param.m,
			  x = param.x,
			  g = param.gi;
	char *tmat = (char *)mat + ADDR(mp, mq);
	sea_int_t score, cost, diag, v, h;

	DECLARE_SEQ(a);
	DECLARE_SEQ(b);
	DECLARE_ALN(l);

	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);
	CLEAR_ALN(l, aln->aln, aln->len);

	score = SCORE(tmat);
	FETCH(a, mi-1); FETCH(b, mj-1);
	while(mi > 0 && mj > 0) {
		diag = SCORE(tmat + TOPLEFT(mp, mq));
		cost = COMPARE(a, b) ? m : x;
		if(score == (diag + cost)) {
			tmat += TOPLEFT(mp, mq); mp -= 2;
			mi--; FETCH(a, mi-1);
			mj--; FETCH(b, mj-1);
			PUSH(l, (cost == m) ? MATCH_CHAR : MISMATCH_CHAR);
			if(ALG == SW && score <= cost) {
				aln->apos += mi; aln->bpos += mj;
				aln->alen -= mi; aln->blen -= mj;
				mi = mj = 0; break;
			}
			score = diag;
		} else if(score == ((h = SCORE(tmat + LEFT(mp, mq))) + g)) {
			tmat += LEFT(mp, mq); mq += LEFTQ(mp, mq); mp--; score = h;
			mi--; FETCH(a, mi-1);
			PUSH(l, DELETION_CHAR);
		} else if(score == ((v = SCORE(tmat + TOP(mp, mq))) + g)) {
			tmat += TOP(mp, mq); mq += TOPQ(mp, mq); mp--; score = v;
			mj--; FETCH(b, mj-1);
			PUSH(l, INSERTION_CHAR);
		} else {
			return SEA_ERROR_OUT_OF_BAND;
		}
	}
	while(mi > 0) { mi--; PUSH(l, DELETION_CHAR); }
	while(mj > 0) { mj--; PUSH(l, INSERTION_CHAR); }
	aln->len = LENGTH(l);
	REVERSE(l);
	return SEA_SUCCESS;
}

/**
 * @fn diag_linear_banded_matsize
 *
 * @brief returns the size of matrix for diag_linear_banded, in bytes.
 *
 * @param[in] alen : the length of the input sequence a (in base pairs).
 * @param[in] blen : the length of the input sequence b (in base pairs).
 * @param[in] bandwidth : unused. give zero for practice.
 *
 * @return the size of a matrix in bytes.
 */
sea_int_t
DECLARE_FUNC_GLOBAL(diag_linear_banded_matsize, SUFFIX)(
	sea_int_t alen,
	sea_int_t blen,
	sea_int_t bandwidth)
{
	/** barriar (1) + matrix (alen+blen+bw) + max vectors (2) */
	return((1 + alen + blen + bandwidth + 2) * BYTES_PER_LINE);
}

/**
 * @fn main
 *
 * @brief gets two ascii strings from stdin, align strings with naive_affine_full, and print the result.
 */
#ifdef MAIN

int main(int argc, char *argv[])
{
	int matlen, alnlen;
	void *mat;
	struct sea_result aln;
	struct sea_params param;

	param.flags = 0;
	param.m = 2;
	param.x = -3;
	param.gi = -4;
	param.ge = -1;
	param.xdrop = 12;
	param.bandwidth = 32;

	aln.a = argv[1];
	aln.alen = strlen(aln.a);
	aln.apos = 0;
	aln.b = argv[2];
	aln.blen = strlen(aln.b);
	aln.bpos = 0;
	alnlen = aln.alen + aln.blen;
	aln.len = alnlen;

	alnlen = aln.alen + aln.blen;
	matlen = CALL_FUNC(diag_linear_banded_matsize, SUFFIX)(
		aln.alen, aln.blen, param.bandwidth);

	aln.aln = (void *)malloc(alnlen);
	mat = (void *)malloc(matlen);
	CALL_FUNC(diag_linear_banded, SUFFIX)(&aln, param, mat);

	printf("%d, %d, %s\n", aln.score, aln.len, aln.aln);

	free(mat);
	free(aln.aln);
	return 0;
}

#endif /* #ifdef MAIN */

/**
 * unittest functions.
 *
 * give a definition of TEST to a compiler, e.g. -DTEST=1, to make a library for tests.
 */
#ifdef TEST
#if SEQ == ascii && ALN == ascii

extern sea_int_t
DECLARE_FUNC_GLOBAL(naive_linear_banded_matsize, DEFAULT_SUFFIX)(
	sea_int_t alen,
	sea_int_t blen,
	sea_int_t bandwidth);

extern sea_int_t
DECLARE_FUNC_GLOBAL(naive_linear_banded, DEFAULT_SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	void *mat);

/**
 * @fn test_2_diag_linear_banded
 *
 * @brief a unittest function of diag_linear_banded.
 *
 * @detail
 * This function is an aggregation of simple fixed ascii queries.
 * In this function, a sea_assert_align macro in tester.h is called. This macro
 * calls sea_align function with given context, checks if the score and the alignment
 * string is the same as the given score and string. If both or one of the results
 * are different, the macro prints the failure status (filename, lines, input sequences,
 * and the content of sea_result) and dumps a content of dynamic programming matrix
 * to dumps.log.
 */
void
DECLARE_FUNC_GLOBAL(test_2_diag_linear_banded, SUFFIX)(
	void)
{
	sea_int_t m = 2,
			  x = -3,
			  g = -4;
	struct sea_context *ctx;

	ctx = sea_init_fp(
		SEA_BANDWIDTH_64,
		CALL_FUNC(diag_linear_banded, SUFFIX),
		CALL_FUNC(diag_linear_banded_matsize, SUFFIX),
		m, x, g, 0,			/** the default blast scoring scheme */
		12);				/** xdrop threshold */

	/**
	 * when both sequences are empty
	 */
	sea_assert_align(ctx, "", 				"", 			0,			"");
	/**
	 * when one sequence is empty
	 */
	sea_assert_align(ctx, "AAA", 			"", 			0,			"");
	sea_assert_align(ctx, "TTTTTTT", 		"", 			0,			"");
	sea_assert_align(ctx, "", 				"AAA", 			0,			"");
	sea_assert_align(ctx, "", 				"TTTTTTT", 		0,			"");
	sea_assert_align(ctx, "CTAG",			"", 			0,			"");

	/**
	 * a match
	 */
	sea_assert_align(ctx, "A",				"A", 			m,			"M");
	sea_assert_align(ctx, "C", 				"C", 			m,			"M");
	sea_assert_align(ctx, "G", 				"G", 			m,			"M");
	sea_assert_align(ctx, "T", 				"T", 			m,			"M");

	/**
	 * a mismatch
	 */
	if(ALG == NW) {
		/**
		 * the Needleman-Wunsch algorithm
		 */
		sea_assert_align(ctx, "A", 				"C", 			x,			"X");
		sea_assert_align(ctx, "C", 				"A", 			x,			"X");
		sea_assert_align(ctx, "G", 				"T", 			x,			"X");
		sea_assert_align(ctx, "T", 				"A", 			x,			"X");
	} else if(ALG == SEA || ALG == SW || ALG == XSEA) {
		/**
		 * the Smith-Waterman algorithm and the seed-and-extend algorithm
		 */
		sea_assert_align(ctx, "A", 				"C", 			0,			"");
		sea_assert_align(ctx, "C", 				"A", 			0,			"");
		sea_assert_align(ctx, "G", 				"T", 			0,			"");
		sea_assert_align(ctx, "T", 				"A", 			0,			"");
	}

	/**
	 * homopolymers with different lengths.
	 */
	if(ALG == NW) {
		/**
		 * the Needleman-Wunsch algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			g+m,			"IM");
		sea_assert_align(ctx, "A", 				"AAA", 			g+g+m,			"IIM");
		sea_assert_align(ctx, "AAAA", 			"AA", 			g+g+m+m,		"DDMM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	4*g+4*m,		"IIIIMMMM");
	} else if(ALG == SEA || ALG == SW || ALG == XSEA) {
		/**
		 * the Smith-Waterman algorithm and the seed-and-extend algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			m,				"M");
		sea_assert_align(ctx, "A", 				"AAA", 			m,				"M");
		sea_assert_align(ctx, "AAAA", 			"AA", 			m+m,			"MM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	4*m,			"MMMM");
	}

	/**
	 * when mismatches occurs.
	 */
	sea_assert_align(ctx, "AAAAAAAA", 		"AAAATAAA", 	7*m+x,			"MMMMXMMM");
	sea_assert_align(ctx, "TTTTTTTT", 		"TTTCTTTT", 	7*m+x,			"MMMXMMMM");
	sea_assert_align(ctx, "CCCCCCCC", 		"CCTCCCCC", 	7*m+x,			"MMXMMMMM");
	sea_assert_align(ctx, "GGGGGGGG", 		"GGCGGTGG", 	6*m+2*x,		"MMXMMXMM");

	/**
	 * when gaps with a base occurs on seq a (insertion).
	 */
	sea_assert_align(ctx, "AAAAATTTT", 		"AAAAAGTTTT", 	9*m+g,			"MMMMMIMMMM");
	sea_assert_align(ctx, "TTTTCCCCC", 		"TTTTACCCCC", 	9*m+g,			"MMMMIMMMMM");
	sea_assert_align(ctx, "CCCGGGGGG", 		"CCCTGGGGGG", 	9*m+g,			"MMMIMMMMMM");
	sea_assert_align(ctx, "GGGAATTT", 		"GGGCAAGTTT", 	8*m+2*g,		"MMMIMMIMMM");

	/**
	 * when gaps with a base occurs on seq b (deletion).
	 */
	sea_assert_align(ctx, "AAAAAGTTTT", 	"AAAAATTTT", 	9*m+g,			"MMMMMDMMMM");
	sea_assert_align(ctx, "TTTTACCCCC", 	"TTTTCCCCC", 	9*m+g,			"MMMMDMMMMM");
	sea_assert_align(ctx, "CCCTGGGGGG", 	"CCCGGGGGG", 	9*m+g,			"MMMDMMMMMM");
	sea_assert_align(ctx, "GGGCAAGTTT", 	"GGGAATTT", 	8*m+2*g,		"MMMDMMDMMM");

	/**
	 * when a gap longer than two bases occurs on seq a.
	 */
	sea_assert_align(ctx, "AAAAATTTTT", 	"AAAAAGGTTTTT", 10*m+2*g,		"MMMMMIIMMMMM");
	sea_assert_align(ctx, "TTTAAAAGGG", 	"TTTCAAAACGGG", 10*m+2*g,		"MMMIMMMMIMMM");

	/**
	 * when a gap longer than two bases occurs on seq b.
	 */
	sea_assert_align(ctx, "AAAAAGGTTTTT",	"AAAAATTTTT", 	10*m+2*g,		"MMMMMDDMMMMM");
	sea_assert_align(ctx, "TTTCAAAACGGG", 	"TTTAAAAGGG", 	10*m+2*g,	 	"MMMDMMMMDMMM");

	/**
	 * when outer gaps occurs.
	 */
	if(ALG == NW) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m+4*x, 		"XXMMMMMMMMXX");
	} else if(ALG == SEA || ALG == XSEA) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m+2*x, 		"XXMMMMMMMM");
	} else if(ALG == SW) {
	 	sea_assert_align(ctx, "TTAAAAAAAATT", 	"CCAAAAAAAACC",	8*m,	 		"MMMMMMMM");
	}

	/**
	 * X-drop test (here xdrop threshold is 12)
	 */
	if(ALG == XSEA) {
		sea_assert_align(ctx, "TTTTTTAAAAAAAA",	"GGGGGGAAAAAAAA",	0, 			"");
		sea_assert_align(ctx, "TTTTTAAAAAAAAA",	"GGGGGAAAAAAAAA",	0, 			"");
		sea_assert_align(ctx, "TTTTAAAAAAAA",	"GGGGAAAAAAAA",		8*m+4*x, 	"XXXXMMMMMMMM");
		sea_assert_align(ctx, "TTTAAAAAAAAA",	"GGGAAAAAAAAA",		9*m+3*x, 	"XXXMMMMMMMMM");

		sea_assert_align(ctx, "AAAATTTTTAAAAAAA","AAAAGGGGGAAAAAAA",	4*m, 		"MMMM");
		sea_assert_align(ctx, "AAAATTTAAAAAAAAA","AAAAGGGAAAAAAAAA",	13*m+3*x, 	"MMMMXXXMMMMMMMMM");
	}

	sea_clean(ctx);
	return;
}

/**
 * @fn test_8_cross_diag_linear_banded
 *
 * @brief cross test between naive_linear_banded and diag_linear_banded
 */
#if HAVE_NAIVE_BANDED

void
DECLARE_FUNC_GLOBAL(test_8_cross_diag_linear_banded, SUFFIX)(
	void)
{
	int i;
	int const cnt = 5;
	sea_int_t m = 2,
			  x = -3,
			  g = -4;
	char *a, *b;
	struct sea_context *full, *band;

	#if BIT_WIDTH == 8
		int const len = 50;
	#elif BIT_WIDTH == 16
		int const len = 1000;
	#else
		#error "bit width must be 8 or 16."
	#endif

	full = sea_init_fp(
		SEA_BANDWIDTH_64,
		CALL_FUNC(naive_linear_banded, DEFAULT_SUFFIX),
		CALL_FUNC(naive_linear_banded_matsize, DEFAULT_SUFFIX),
		m, x, g, 0,
		10000);
	band = sea_init_fp(
		SEA_BANDWIDTH_64,
		CALL_FUNC(diag_linear_banded, SUFFIX),
		CALL_FUNC(diag_linear_banded_matsize, SUFFIX),
		m, x, g, 0,
		10000);


	for(i = 0; i < cnt; i++) {
		a = rseq(len);
		b = mseq(a, 20, 100, 100);
		sea_assert_cross(
			full, band, a, b);
		free(a); free(b);
	}

	sea_clean(full);
	sea_clean(band);
	return;
}

#endif /* #if HAVE_NAIVE_BANDED */

#endif	/* #if SEQ == ascii && ALN == ascii */
#endif	/* #ifdef TEST */

/**
 * end of diag_linear_banded.c
 */
