
/**
 * @file prim.h
 *
 * @brief a collection of primitive definitions of the SIMD algorithms.
 *
 * @detail
 * This header contains the definitions of the primitive macros, such as
 * the conversion of coordinates and the matrix load / store macros.
 * This header is included from diag_variant.h or diff_variant.h, and
 * the BAND_WIDTH constant must be defined when included.
 */
#ifndef _PRIM_H_INCLUDED
#define _PRIM_H_INCLUDED

/**
 * architecture flag definitions
 */
#define SSE 		( 1 )
#define AVX 		( 2 )

/**
 * @struct pos
 * @brief a struct containing a position
 */
struct pos {
	sea_int_t i, j, p, q;
};

/**
 * @struct mpos
 * @biref contains multiple position, max pos and end pos
 */
struct mpos {
	struct pos m;			/** max score pos */
	struct pos e;			/** end pos */
};

#define CALC_COMBINED_COORD(i, j)	( 2*(i) + (j) )
#define CALC_SEPARETE_COORD_I(ij)	( (ij)/2 )
#define CALC_SEPARETE_COORD_J(ij)	( (ij)&0x01 )

#define INCREMENT_LEN(i, j)			{ (i) += (j); (j) = ((j)+1) & 0x01; }
#define DECREMENT_LEN(i, j)			{ (j) = ((j)+1) & 0x01; (i) -= (j); }
#define CALC_COORDX(i, j, k)		( (i) - (k) + BAND_WIDTH/2 )
#define CALC_COORDY(i, j, k)		( (i) + (j) + (k) - BAND_WIDTH/2 )
#define CALC_COORDX_IJ(ij, k)		( CALC_SEPARETE_COORD_I(ij) - (k) + BAND_WIDTH/2 )
#define CALC_COORDY_IJ(ij, k)		( CALC_SEPARETE_COORD_I(ij) + CALC_SEPARETE_COORD_J(ij) + (k) - BAND_WIDTH/2 )
#define INSIDE_AREA(ij,k,alen,blen) ( (CALC_COORDX_IJ(ij, k) <= alen) && (CALC_COORDY_IJ(ij, k) <= blen) )
/*
#define CALC_COORDI(x, y)			( ((x) + (y)) / 2 )
#define CALC_COORDJ(x, y)			( ((x) + (y)) & 0x01 )
#define CALC_COORDIJ(x, y)			( (x) + (y) )
#define CALC_COORDK(x, y)			( ((y) - (x) + BAND_WIDTH) / 2)
*/
#define LOAD_POS(pos, i, j, k)		{ (i) = (pos)[0]; (j) = (pos)[1]; (k) = (pos)[2]; }
#define STORE_POS(pos, i, j, k)		{ (pos)[0] = (i); (pos)[1] = (j); (pos)[2] = (k); }
#define MOZ_LOAD_POS(pos,i,j,p,k)	{ (i) = (pos)[0]; (j) = (pos)[1]; (p) = (pos)[2]; (k) = (pos)[3]; }
#define MOZ_STORE_POS(pos,i,j,p,k)	{ (pos)[0] = (i); (pos)[1] = (j); (pos)[2] = (p); (pos)[3] = (k); }

#define LOAD_MAT(m,a,b,c)			{ (a) = (m)[0]; (b) = (m)[1]; (c) = (m)[2]; }
#define LOAD_AFFINE_MAT(m,a,b,c,d)	{ (a) = (m)[0]; (b) = (m)[1]; (c) = (m)[2]; (d) = (m)[3]; }

#define SWAP(a, b, tmp)				{ (tmp) = (a); (a) = (b); (b) = (tmp); }


#define PUSHQ(x, y)					{ VEC_CHAR_SHIFT_L(y); VEC_CHAR_INSERT_LSB(y, x); }
#define PUSHT(x, y)					{ VEC_CHAR_SHIFT_R(y); VEC_CHAR_INSERT_MSB(y, x); }

//#define READQ(x, y)					{ VEC_SHIFT_L(y); VEC_INSERT_LSB(y, x); }
//#define READT(x, y)					{ VEC_SHIFT_R(y); VEC_INSERT_MSB(y, x); }

//#define VEC_SETF_MSB(v)				{ VEC_SETZERO(v); VEC_INSERT_MSB(v, MSB_MARKER); }
//#define VEC_SETF_LSB(v)				{ VEC_SETZERO(v); VEC_INSERT_LSB(v, LSB_MARKER); }


#endif /* #ifndef _PRIM_H_INCLUDED */

/**
 * end of prim.h
 */
