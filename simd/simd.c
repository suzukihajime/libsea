
/**
 * @file simd.c
 *
 * @brief an implementation of matsize functions and get_sea function.
 *
 * @sa sea.c
 *
 * @detail
 * the Diag and Diff algorithms:
 *
 * Diag algorithms are the SIMD-based banded alignment algorithms. Diff algorithms are
 * the difference-based variant of the diag algorithms. The difference between the two 
 * types of algorithms are an usage of memory, the overhead of maximum score search, and
 * the limitation of the length of the query sequence. Generally, the diff algorithms
 * use the same or less amount of memory than the diag algorithms, since the scores are
 * represented in the difference between adjacent cells. The diff algorithms have no 
 * limitation on the length of the query sequence, since the bit-width of the difference 
 * value is not determined by the length of the queries, but by the scoring schemes.
 * However, the disadvantage of the diff algorithms are an overhead of the maximum score
 * search, which imposes a constant amount of computation after the fill-in steps 
 * (or before the traceback steps).
 *
 * The diff algorithms are suitable in alignment of long queries, where the overhead of
 * the maximum score search step becomes relatively small in the total calculation.
 * The threshold of the query length are mentioned in the comment on simd_get_sea
 * function.
 */
#include <stdlib.h>
#include "simd.h"
#include "../include/sea.h"
#include "../util/util.h"		/** internal util functions */
#include "../build/config.h"

#if HAVE_DIAG_BANDED
	#include "diag_linear_banded_decl.h"
	#include "diag_linear_banded_table.h"
	#include "diag_affine_banded_decl.h"
	#include "diag_affine_banded_table.h"

	#include "diag_linear_dynamic_banded_decl.h"
	#include "diag_linear_dynamic_banded_table.h"
	#include "diag_affine_dynamic_banded_decl.h"
	#include "diag_affine_dynamic_banded_table.h"
#endif

#if HAVE_DIFF_BANDED
	#include "diff_linear_banded_decl.h"
	#include "diff_linear_banded_table.h"
	#include "diff_affine_banded_decl.h"
	#include "diff_affine_banded_table.h"

	#include "diff_linear_dynamic_banded_decl.h"
	#include "diff_linear_dynamic_banded_table.h"
	#include "diff_affine_dynamic_banded_decl.h"
	#include "diff_affine_dynamic_banded_table.h"
#endif

/**
 * function declarations
 */
#if HAVE_DIAG_BANDED
sea_int_t diag_get_sea(
	struct sea_funcs *fp,
	struct sea_params param,
	sea_int_t len,
	sea_int_t alg_type, sea_int_t seq_type, sea_int_t aln_type);
#endif

#if HAVE_DIFF_BANDED
sea_int_t diff_get_sea(
	struct sea_funcs *fp,
	struct sea_params param,
	sea_int_t len,
	sea_int_t alg_type, sea_int_t seq_type, sea_int_t aln_type);
#endif

/**
 * @fn simd_get_sea
 * 
 * @brief retrieve a pointer to alignment function.
 *
 * @param[in] flags : option flag bitfield. see parameter of sea_init in sea.h
 * @param[in] len : the maximum length of the query sequence. (unused in this function.)
 * @param[in] alg_type : an algorithm index. see table.th and sea.h.
 * @param[in] seq_type : an input sequence type index. see table.th and sea.h.
 * @param[in] aln_type : an output alignment type index. see table.th and sea.h.
 * @param[in] scmat : the scores in the dynamic programming. scmat[0] = M, scmat[1] = X, scmat[2] = Gi, and scmat[3] = Ge. see sea.h.
 * @param[in] xdrop : the xdrop threshold in positive integer.
 * @param[out] sea_matsize : a pointer to a function which returns the size of a matrix.
 * @param[out] sea_sea : a pointer to a function which do an alignment.
 *
 * @return SEA_SUCCESS if proper pointer is set, SEA_ERROR_UNSUPPORTED_ALG if not.
 *
 * @detail
 * This function selects an appropriate alignment function from the variants of
 * diag and diff algorithms. The supported options are;
 * algorithms: the banded SW, SEA, and NW.
 * bandwidth: 16 or 32.
 * The other algorithms, such as full dynamic programming matrix, are not supported.
 * The selection of the algorithm is done based on the charts shown below.
 *
 * SIMD algorithm selection chart; A is defined as min(1000, 32768 / M) for x86_64 architectures.
 * +-----------------------------------------------------+
 * |              |              algorithm               |
 * |              |     SW     |     SEA    |     NW     |
 * |--------------+------------+------------+------------+
 * |    len < A   |    diag    |    diag    |    diag    |
 * |--------------+------------+------------+------------+
 * |    A < len   |    diag    |    diff    |    diff    |
 * +--------------+------------+------------+------------+
 *
 * Notice:
 * The function selector for diag algorithms always use 16-bit wide algorithms. If you want to
 * use 8-bit wide cells, you have to initialize alignment context with sea_init_fp, giving
 * proper function pointer to 8-bit wide algorithm (e.g. diag_linear_banded_sse_8_32).
 */
sea_int_t simd_get_sea(
	struct sea_funcs *fp,
	struct sea_params param,
	sea_int_t len,
	sea_int_t alg_type, sea_int_t seq_type, sea_int_t aln_type)
{
	sea_int_t const diag_max_score = 32768,
					len_thresh = 1000;
	fp->_sea_matsize = NULL;
	fp->_sea_sea = NULL;

	/**
	 * first check if algorithm and scoring scheme are capable of diff algorithms.
	 */
	if((param.flags & SEA_FLAGS_MASK_DP) == SEA_FULL_DP) {
		/** the full DP matrix is not supported in the SIMD algorithms */
		return SEA_ERROR_UNSUPPORTED_ALG;
	}

	/**
	 * the band width larger than 32 is not supported.
	 */
	if((param.flags & SEA_FLAGS_MASK_BANDWIDTH) == SEA_BANDWIDTH_64 ||
	   (param.flags & SEA_FLAGS_MASK_BANDWIDTH) == SEA_BANDWIDTH_128) {
		return SEA_ERROR_UNSUPPORTED_ALG;
	}

	/**
	 * banded DP
	 */
	if((param.flags & SEA_FLAGS_MASK_DP) == SEA_BANDED_DP
	|| (param.flags & SEA_FLAGS_MASK_DP) == SEA_DYNAMIC_BANDED_DP) {
		switch(param.flags & SEA_FLAGS_MASK_ALG) {
		#if HAVE_DIAG_BANDED && HAVE_DIFF_BANDED
			case SEA_SW:
				return(diag_get_sea(fp, param, len, alg_type, seq_type, aln_type));
			case SEA_SEA:
				if((param.flags & SEA_FLAGS_MASK_POLICY) == SEA_EXACT) {
					return(diag_get_sea(fp, param, len, alg_type, seq_type, aln_type));
				}
				/** fall through to check for length */
			case SEA_NW:
				if(len > MIN2(diag_max_score / param.m, len_thresh)) {
					return(diff_get_sea(fp, param, len, alg_type, seq_type, aln_type));
				} else {
					return(diag_get_sea(fp, param, len, alg_type, seq_type, aln_type));
				}
			default:
				return SEA_ERROR_UNSUPPORTED_ALG;

		#elif HAVE_DIAG_BANDED
			default:
				return(diag_get_sea(fp, param, len, alg_type, seq_type, aln_type));

		#elif HAVE_DIFF_BANDED
			default:
				return(diff_get_sea(fp, param, len, alg_type, seq_type, aln_type));

		#else
			default:
				return SEA_ERROR_UNSUPPORTED_ALG;
		#endif
		}
	}

	/**
	 * dynamic banded DP
	 */
	if((param.flags & SEA_FLAGS_MASK_DP) == SEA_DYNAMIC_BANDED_DP) {
		switch(param.flags & SEA_FLAGS_MASK_ALG) {
		#if HAVE_DIAG_BANDED && HAVE_DIFF_BANDED
			case SEA_SW:
				return(diag_get_sea(fp, param, len, alg_type, seq_type, aln_type));
			case SEA_SEA:
				if((param.flags & SEA_FLAGS_MASK_POLICY) == SEA_EXACT) {
					return(diag_get_sea(fp, param, len, alg_type, seq_type, aln_type));
				}
				/** fall through to check for length */
			case SEA_NW:
				if(len > MIN2(diag_max_score / param.m, len_thresh)) {
					return(diff_get_sea(fp, param, len, alg_type, seq_type, aln_type));
				} else {
					return(diag_get_sea(fp, param, len, alg_type, seq_type, aln_type));
				}
			default:
				return SEA_ERROR_UNSUPPORTED_ALG;

		#elif HAVE_DIAG_BANDED
			default:
				return(diag_get_sea(fp, param, len, alg_type, seq_type, aln_type));

		#elif HAVE_DIFF_BANDED
			default:
				return(diff_get_sea(fp, param, len, alg_type, seq_type, aln_type));

		#else
			default:
				return SEA_ERROR_UNSUPPORTED_ALG;
		#endif
		}
	}
	return SEA_ERROR_UNSUPPORTED_ALG;
}

#if HAVE_DIAG_BANDED
/**
 * @fn diag_get_sea
 *
 * @brief retrieve diag function
 */
sea_int_t diag_get_sea(
	struct sea_funcs *fp,
	struct sea_params param,
	sea_int_t len,
	sea_int_t alg_type, sea_int_t seq_type, sea_int_t aln_type)
{
	sea_int_t bit_index,
			  band_index;

	switch((param.flags & SEA_FLAGS_MASK_BANDWIDTH)) {
		case SEA_BANDWIDTH_16: 	band_index = 0; break;
		case SEA_BANDWIDTH_32:	band_index = 1; break;
		case SEA_BANDWIDTH_64:
		case SEA_BANDWIDTH_128:
		default:				band_index = 0; break;
	}
	bit_index = 2; 			/** always 16-bit wide */

	/**
	 * matsize and sea functions.
	 */
	#if HAVE_SSE4
	 	if((param.flags & SEA_FLAGS_MASK_DP) == SEA_BANDED_DP) {
		 	if((param.flags & SEA_FLAGS_MASK_COST) == SEA_LINEAR_GAP_COST) {
				*fp = diag_linear_banded_sse_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	} else {
				*fp = diag_affine_banded_sse_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	}
		} else if((param.flags & SEA_FLAGS_MASK_DP) == SEA_DYNAMIC_BANDED_DP) {
		 	if((param.flags & SEA_FLAGS_MASK_COST) == SEA_LINEAR_GAP_COST) {
				*fp = diag_linear_dynamic_banded_sse_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	} else {
				*fp = diag_affine_dynamic_banded_sse_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	}			
		}
	 	if(fp->_sea_sea != NULL && fp->_sea_matsize != NULL) {
	 		return SEA_SUCCESS;
	 	}
	#endif /* #ifdef HAVE_SSE4 */

	#if HAVE_AVX2
	 	if((param.flags & SEA_FLAGS_MASK_DP) == SEA_BANDED_DP) {
		 	if((param.flags & SEA_FLAGS_MASK_COST) == SEA_LINEAR_GAP_COST) {
				*fp = diag_linear_banded_avx_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	} else {
				*fp = diag_affine_banded_avx_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	}
	 	} else if((param.flags & SEA_FLAGS_MASK_DP) == SEA_DYNAMIC_BANDED_DP) {
		 	if((param.flags & SEA_FLAGS_MASK_COST) == SEA_LINEAR_GAP_COST) {
				*fp = diag_linear_dynamic_banded_avx_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	} else {
				*fp = diag_affine_dynamic_banded_avx_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	}			
		}
	 	if(fp->_sea_sea != NULL && fp->_sea_matsize != NULL) {
	 		return SEA_SUCCESS;
	 	}
	#endif 	/* #ifdef HAVE_AVX2 */
	
	return SEA_ERROR_UNSUPPORTED_ALG;
}
#endif /* #if HAVE_DIAG_BANDED */

#if HAVE_DIFF_BANDED
/**
 * @fn diff_get_sea
 *
 * @brief retrieve diag function
 */
sea_int_t diff_get_sea(
	struct sea_funcs *fp,
	struct sea_params param,
	sea_int_t len,
	sea_int_t alg_type, sea_int_t seq_type, sea_int_t aln_type)
{
	sea_int_t bit_index,
			  band_index;
	sea_int_t m = param.m,
			  x = param.x,
			  gi = param.gi,
			  ge = param.ge;

	switch((param.flags & SEA_FLAGS_MASK_BANDWIDTH)) {
		case SEA_BANDWIDTH_16: 	band_index = 0; break;
		case SEA_BANDWIDTH_32:	band_index = 1; break;
		case SEA_BANDWIDTH_64:
		case SEA_BANDWIDTH_128:
		default:				band_index = 0; break;
	}
	if(m - 2*gi < 16) {
		bit_index = 0; 			/** 4-bit packed algorithms */
	} else {
		bit_index = 1;			/** 8-bit packed algorithms */
	}
	
	/**
	 * matsize sea functions.
	 */
	#if HAVE_SSE4
	 	if((param.flags & SEA_FLAGS_MASK_DP) == SEA_BANDED_DP) {
		 	if((param.flags & SEA_FLAGS_MASK_COST) == SEA_LINEAR_GAP_COST) {
				*fp = diff_linear_banded_sse_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	} else {
				*fp = diff_affine_banded_sse_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	}
		} else if((param.flags & SEA_FLAGS_MASK_DP) == SEA_DYNAMIC_BANDED_DP) {
		 	if((param.flags & SEA_FLAGS_MASK_COST) == SEA_LINEAR_GAP_COST) {
				*fp = diff_linear_dynamic_banded_sse_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	} else {
				*fp = diff_affine_dynamic_banded_sse_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	}
		}
	 	if(fp->_sea_sea != NULL && fp->_sea_matsize != NULL) {
	 		return SEA_SUCCESS;
	 	}
	#endif /* #ifdef HAVE_SSE4 */

	#if HAVE_AVX2
	 	if((param.flags & SEA_FLAGS_MASK_DP) == SEA_BANDED_DP) {
		 	if((param.flags & SEA_FLAGS_MASK_COST) == SEA_LINEAR_GAP_COST) {
				*fp = diff_linear_banded_avx_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	} else {
				*fp = diff_affine_banded_avx_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	}
		} else if((param.flags & SEA_FLAGS_MASK_DP) == SEA_DYNAMIC_BANDED_DP) {
		 	if((param.flags & SEA_FLAGS_MASK_COST) == SEA_LINEAR_GAP_COST) {
				*fp = diff_linear_dynamic_banded_avx_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	} else {
				*fp = diff_affine_dynamic_banded_avx_table
					[bit_index][band_index][alg_type][seq_type][aln_type];
		 	}
		}
	 	if(fp->_sea_sea != NULL && fp->_sea_matsize != NULL) {
	 		return SEA_SUCCESS;
	 	}
	#endif 	/* #ifdef HAVE_AVX2 */

	return SEA_ERROR_UNSUPPORTED_ALG;
}
#endif /* #if HAVE_DIFF_BANDED */

/**
 * end of simd.c
 */
