
/**
 * @file edit_full.c
 *
 * @brief the bit-parallel full dynamic programming algorithm
 *
 * @detail
 * This is an implementation of the Myers' bit-parallel edit-distance algorithm.
 * The algorithm is an equivalent of the linear-gap cost full dynamic programming
 * algorithm (implemented in naive_linear_full.c) with a scoring scheme of 
 * match = 2, mismatch = 1, and gap = 0. This implementation returns a score in 
 * this alignment-oriented interpretation. The edit-distance-oriented scoring, 
 * with (M, X, G) = (0, -1, -1), is original form of the algorithm and described 
 * in the paper [1].
 * 
 * The conversion between the two scoring schemes is following:
 *
 *     score_aln = length + score_edit
 *         where score_aln is a positive score obtained from the linear-gap cost
 *             algorithm with (M, X, G) = (2, 1, 0) and score_edit is a negative
 *             score with (M, X, G) = (0, -1, -1). length is the alignment-path
 *             length.
 *
 * This implementation serves as the two algorithm, the global alignment algorithm
 * and the seed-and-extend alignment algorithm. The selection of the algorithm is 
 * done by giving proper define flags to the compiler, such as -DALG=SEA or -DALG=NW.
 * This file also requires the input sequence format flag and output alignment 
 * string format flag. The simplest choices are SEQ=ascii and ALN=ascii, the other
 * options and the details of seqreaders / alnwriters are described in the
 * seqreader/seqreader.h.
 *
 * References:
 *      Myers, G. (1999). A fast bit-vector algorithm for approximate string
 *          matching based on dynamic programming. Journal of the ACM 46(3),
 *          May 1999, pp 395-415. 
 */
#ifdef DEBUG
	#include <stdio.h>
#endif
#ifdef MAIN
	#include <string.h>
#endif
#ifdef TEST
 	#include "../util/tester.h"
#endif

#include <stdlib.h>						/* for definition of NULL */
#include <limits.h>						/* for definition of ULONG_MAX */
#include "../include/sea.h"				/* for API definitions, flag definitions, and structs */
#include "../util/util.h"				/* for internal utility functions */
#include "../seqreader/seqreader.h"		/* for input sequence reader and output alignment writer */
#include "../util/bench.h"				/* for benchmarking */

/**
 * PUSH macros. used in the fill-in step to push a fetched base
 * into sequence comparison buffer.
 */
#define PUSHR(reg, a)						{ (reg)<<=1; (reg)|=(a); }
#define PUSHL(reg, a)						{ (reg)>>=1; (reg)|=(((unsigned long)(a))<<(REG_BITWIDTH-1)); }

/**
 * @struct band
 * @brief a struct containing a set of deltas in a line.
 */
struct band {
	unsigned long pv, mv, ph, mh;
};

/**
 * @struct pos
 * @brief a struct containing a position
 */
struct pos {
	sea_int_t i, j, p, q;
};

/**
 * coordinate conversion macros.
 * see naive_linear_full.c for composition of coordinates.
 */
#define	ADDR(p, q)					( p )
#define ADDRI(x, y) 				( ADDR(COP(x, y), COQ(x, y)) )
#define COX(p, q)					( ((p)>>1) - (q) )
#define COY(p, q)					( (((p)+1)>>1) + (q) )
#define COP(x, y)					( (x) + (y) )
#define COQ(x, y) 					( ((y)-(x))>>1 )
#define TOPQ(p, q) 					( - !(p&0x01) )
#define LEFTQ(p, q) 				( (p&0x01) )

/**
 * delta extraction macros. returns one from {+1, 0, -1}.
 */
#define DV(ptr, p, q)				( (((ptr[p].pv)>>((q)+REG_BITWIDTH/2)) & 0x01) - (((ptr[p].mv)>>((q)+REG_BITWIDTH/2)) & 0x01) )
#define DH(ptr, p, q)				( (((ptr[p].ph)>>((q)+REG_BITWIDTH/2)) & 0x01) - (((ptr[p].mh)>>((q)+REG_BITWIDTH/2)) & 0x01) )

/**
 * internal function declarations.
 */
#if ALG == SEA || ALG == NW
struct pos
DECLARE_FUNC(edit_full_fill, SUFFIX)(
	struct sea_result *aln,
	void *mat);

struct pos
DECLARE_FUNC(edit_full_search, SUFFIX)(
	struct sea_result *aln,
	void *mat,
	struct pos e);

sea_int_t
DECLARE_FUNC(edit_full_trace, SUFFIX)(
	struct sea_result *aln,
	void *mat,
	struct pos m);
#endif /* #if ALG == SEA || ALG == NW */

/**
 * @fn edit_full
 *
 * @brief the Myers' bit-parallel edit-distance algorithm
 *
 * @param[ref] aln : a pointer to a sea_result structure. aln must NOT be NULL. (a structure which contains an alignment result.)
 * @param[in] aln->a : a pointer to a query sequence a.
 * @param[inout] aln->apos : (input) the start position of the search section on sequence a. (0 <= apos < length(sequence a)) (output) the start position of the alignment.
 * @param[inout] aln->alen : (input) the length of the search section on sequence a. (0 < alen) (output) the length of the alignment.
 * @param[in] aln->b : a pointer to a query sequence b.
 * @param[inout] aln->bpos : same as apos.
 * @param[inout] aln->blen : same as alen.
 *
 * @param[out] aln->aln : a pointer to the alignment string.
 * @param[inout] aln->len : (input) the reserved length of the aln->aln. (output) the length of the alignment string.
 * @param[out] aln->score : an alignment score.
 *
 * @param[in] scmat : unused parameter.
 * @param[in] xdrop : unused parameter. 
 * @param[in] bandwidth : unused parameter.
 * @param[in] mat : a pointer to the DP matrix.
 * @param[in] matsize : the size of the DP matrix.
 *
 * @return status. zero means success. see sea.h for the other error code.
 */
sea_int_t 
DECLARE_FUNC_GLOBAL(edit_full, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	void *mat)
{
	/**
	 * a matrix composition and coordinates
	 *
	 * In this algorithm, a dynamic programming matrix is represented as a stack of 'bars'.
	 * Each bar consists of a set of four difference bit vectors; pv, mv, ph, and mh.
	 * pv and mv are conbined to hold vertical differences of scores, and ph and mh hold
	 * horizontal differences. ('1' in a pv vector means a difference value is positive.)
	 *
	 * a coordinates of a matrix follow one in the 'naive_linear_full.c'. see comments
	 * in 'naive_linear_full.c' for its details.
	 */
	
	/**
	 * the Smith-Waterman algorithm is not available in the unit-cost algorithm.
	 */
	#if ALG == SW
	 	return 0;
	#elif ALG == NW || ALG == SEA
		struct pos e,		/** a termination point of fill-in step */
				   m;		/** a maximum score position */
		DECLARE_BENCH(fill);
		DECLARE_BENCH(search);
		DECLARE_BENCH(trace);
		
		/**
		 * fillin a matrix
		 */
		START_BENCH(fill);
		e = CALL_FUNC(edit_full_fill, SUFFIX)(
			aln, (struct band *)mat);
		END_BENCH(fill);
		/**
		 * search minimum score position
		 */
		START_BENCH(search);
		m = CALL_FUNC(edit_full_search, SUFFIX)(
			aln,
			(struct band *)mat,
			e);
		END_BENCH(search);

		if(aln->aln == NULL) {
			return(aln->score);
		}
		/**
		 * if aln->aln is not NULL, do traceback.
		 */
		START_BENCH(trace);
		CALL_FUNC(edit_full_trace, SUFFIX)(
			aln,
			(struct band *)mat,
			m);
		END_BENCH(trace);
		return(aln->score);
	#else
		#error "Invalid number of ALG. Check the definition flag of the ALG."
		return 0;
	#endif
}

#if ALG == SEA || ALG == NW

/**
 * @fn edit_full_fill
 *
 * @brief fill in a matrix
 *
 * @param[ref] aln : a pointer to a sea_result struct.
 * @param[in] xdrop : currently unused.
 * @param[in] mat : a pointer to a matrix.
 * @param[in] matlen : the length of the p-direction of the matrix.
 *
 * @return an end position of the extension.
 */
struct pos
DECLARE_FUNC(edit_full_fill, SUFFIX)(
	struct sea_result *aln,
	void *mat)
{
	sea_int_t i;
	sea_int_t alen = aln->alen,
			  blen = aln->blen;
	sea_int_t const bw = 8 * sizeof(unsigned long);
	sea_int_t bl = blen + 1;
	sea_int_t w = (alen + bw - 1) / bw;
	unsigned long *peq = (unsigned long *)mat;
	struct band   *prev  = peq + 4*w,
				  *store = prev + bl;
	struct pos e = {0, 0, 0, 0};

	DECLARE_SEQ(a);
	DECLARE_SEQ(b);

	/**
	 * prepare sequence
	 */
	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);

	for(i = 0; i < alen; i++) {
		FETCH(a, i);
		vl |= (DECODE_LOW(a)<<(i % bw));
		vh |= (DECODE_HIGH(a)<<(i % bw));
		if(((i + 1) % bw) == 0) {
			*peq++ = ~(vl | vh);		/** A */
			*peq++ = vl & ~vh;			/** C */
			*peq++ = ~vl & vh;			/** G */
			*peq++ = vl & vh;			/** T */
		}
	}

	memset(prev, 0, bl * sizeof(struct band));
	for(i = 0; i < w; i++) {
		/** ここに1行目の初期化を書く */
		for(j = 0; j < blen; j++) {
			FETCH(b);
			eq = peq[4*i + DECODE(b)];
			xv = (((eq & ph) + ph) ^ ph) | eq;
			pv = mh | ~(xv | ph); mv = ph & xv;
			pv<<=1; pv |= (((prev->pv)>>(bw-1))&0x1L);
			mv<<=1; mv |= (((prev->mv)>>(bw-1))&0x1L);
			xh = eq | mv;
			ph = mv | ~(xh | pv); mh = pv & xh;
			store->pv = pv; store->mv = mv;
			store->ph = ph; store->mh = mh;
		}
	}
	return(e);
}

/**
 * @fn edit_full_search
 *
 * @brief search traceback start postion.
 */
struct pos
DECLARE_FUNC_GLOBAL(edit_full_search, SUFFIX)(
	struct sea_result *aln,
	void *mat,
	struct pos e)
{
	sea_int_t i, j;
	sea_int_t score = 0,
			  max = 0;
	sea_int_t const bw = 8 * sizeof(unsigned long);
	sea_int_t bl = blen + 1;
	sea_int_t w = (alen + bw - 1) / bw;
	struct band *ptr = mat + 4*w + bl;
	struct pos m;

	if(ALG == NW) {
		m.i = aln->alen; m.j = aln->blen;
	} else {
		/** sum up deltas along with b coordinate */
		for(j = 0; j < blen; j++) {
			score += DV(ptr, 0, j+1, bl);
		}
		for(i = 0; i < alen; i++) {
			if(score >= max) { max = score; mi = i; mj = j; }
			score += DH(ptr, i+1, j, bl);
		}
		for(j = blen, ptr -= bl; j >= 0; j--) {
			if(score > max) { max = score; mi = i; mj = j; }
			score -= DV(ptr, i, j, bl);
		}
	}
	return(m);
}

/**
 * @fn edit_full_trace
 *
 * @brief traceback
 */
sea_int_t
DECLARE_FUNC_GLOBAL(edit_full_trace, SUFFIX)(
	struct sea_result *aln,
	void *mat,
	struct pos m)
{
	sea_int_t mi = m.i,
			  mj = m.j;
	sea_int_t di, diag, sc;

	DECLARE_SEQ(a);
	DECLARE_SEQ(b);
	DECLARE_ALN(l);

	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);
	CLEAR_ALN(l, aln->aln, aln->len);

	FETCH(a, mi-1); FETCH(b, mj-1);
	while(mi > 0 && mj > 0) {
		diag = (dh = DH(mat, mi, mj)) + DV(mat, mi-1, mj);
		sc = !COMPARE(a, b);
		if(sc == diag) {
			mi--; FETCH(a, mi-1);
			mj--; FETCH(b, mj-1);
			PUSH(l, (sc == 0) ? MATCH_CHAR : MISMATCH_CHAR);
		} else if(dh == 1) {
			mi--; FETCH(a, mi-1); PUSH(l, DELETION_CHAR);
		} else if(DV(mat, mi, mj) == 1) {
			mj--; FETCH(b, mj-1); PUSH(l, INSERTION_CHAR);
		} else {
			return SEA_ERROR_OUT_OF_BAND;
		}
	}
	while(mi > 0) { mi--; PUSH(l, DELETION_CHAR); }
	while(mj > 0) { mj--; PUSH(l, INSERTION_CHAR); }
	aln->len = LENGTH(l);
	REVERSE(l, mi);
	return SEA_SUCCESS;
}

/**
 * @fn edit_full_matsize
 *
 * @brief returns the size of a matrix for the edit_full algorithm in bytes.
 *
 * @param[in] alen : the length of the input sequence a (in base pairs).
 * @param[in] blen : the length of the input sequence b (in base pairs).
 * @param[in] bandwidth : the width of the band (in cells).
 *
 * @return the size of a matrix in bytes.
 */
sea_int_t 
DECLARE_FUNC_GLOBAL(edit_full_matsize, SUFFIX)(
	sea_int_t alen,
	sea_int_t blen,
	sea_int_t bandwidth)
{
	sea_int_t wordwidth = (bandwidth + sizeof(unsigned long) - 1) / sizeof(unsigned long);
	return(4 * (alen + blen + bandwidth + 1) * wordwidth);
}

/**
 * @fn main
 *
 * @brief gets two ascii strings from stdin, align strings with naive_affine_full, and print the result.
 */
#ifdef MAIN

int main(int argc, char *argv[])
{
	int matlen, alnlen;
	void *mat;
	struct sea_result aln;
	struct sea_params param;

	param.flags = 0;
	param.m = 2;
	param.x = -3;
	param.gi = -4;
	param.ge = -1;
	param.xdrop = 12;
	param.bandwidth = 32;

	aln.a = argv[1];
	aln.alen = strlen(aln.a);
	aln.apos = 0;
	aln.b = argv[2];
	aln.blen = strlen(aln.b);
	aln.bpos = 0;
	alnlen = aln.alen + aln.blen;
	aln.len = alnlen;

	alnlen = aln.alen + aln.blen;
	matlen = CALL_FUNC(edit_full_matsize, SUFFIX)(
		aln.alen, aln.blen, param.bandwidth);

	aln.aln = (void *)malloc(alnlen);
	mat = (void *)malloc(matlen);
	CALL_FUNC(edit_full, SUFFIX)(&aln, param, mat);

	printf("%d, %d, %s\n", aln.score, aln.len, aln.aln);

	free(mat);
	free(aln.aln);
	return 0;
}

#endif /* #ifdef MAIN */


/**
 * unittest functions.
 *
 * give a definition of TEST to a compiler, e.g. -DTEST=1, to make a library for tests.
 */
#ifdef TEST
#if SEQ == ascii && ALN == ascii

/**
 * @fn test_1_edit_full
 *
 * @brief a unittest function of edit_full.
 *
 * @detail
 * This function is an aggregation of simple fixed ascii queries.
 * In this function, a sea_assert_align macro in tester.h is called. This macro
 * calls sea_align function with given context, checks if the score and the alignment
 * string is the same as the given score and string. If both or one of the results
 * are different, the macro prints the failure status (filename, lines, input sequences,
 * and the content of sea_result) and dumps a content of dynamic programming matrix
 * to dumps.log.
 */
void
DECLARE_FUNC_GLOBAL(test_1_edit_full, SUFFIX)(
	void)
{
	sea_int_t m = 2,
			  x = 1,
			  g = 0;
	struct sea_context *ctx;

	ctx = sea_init_fp(
		SEA_BANDWIDTH_64,
		CALL_FUNC(edit_full, SUFFIX),
		CALL_FUNC(edit_full_matsize, SUFFIX),
		m, x, g, 0,			/** the default blast scoring scheme */
		10000);				/** xdrop threshold */

	/**
	 * when both sequences are empty
	 */
	sea_assert_align(ctx, "", 				"", 			0,			"");
	/**
	 * when one sequence is empty
	 */
	sea_assert_align(ctx, "AAA", 			"", 			0,			"");
	sea_assert_align(ctx, "TTTTTTT", 		"", 			0,			"");
	sea_assert_align(ctx, "", 				"AAA", 			0,			"");
	sea_assert_align(ctx, "", 				"TTTTTTT", 		0,			"");
	sea_assert_align(ctx, "CTAG",			"", 			0,			"");

	/**
	 * a match
	 */
	sea_assert_align(ctx, "A",				"A", 			m,			"M");
	sea_assert_align(ctx, "C", 				"C", 			m,			"M");
	sea_assert_align(ctx, "G", 				"G", 			m,			"M");
	sea_assert_align(ctx, "T", 				"T", 			m,			"M");

	/**
	 * a mismatch
	 */
	sea_assert_align(ctx, "A", 				"C", 			x,			"X");
	sea_assert_align(ctx, "C", 				"A", 			x,			"X");
	sea_assert_align(ctx, "G", 				"T", 			x,			"X");
	sea_assert_align(ctx, "T", 				"A", 			x,			"X");

	/**
	 * homopolymers with different lengths.
	 */
	#if ALG == NW
		/**
		 * the Needleman-Wunsch algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			g+m,			"IM");
		sea_assert_align(ctx, "A", 				"AAA", 			g+g+m,			"IIM");
		sea_assert_align(ctx, "AAAA", 			"AA", 			g+g+m+m,		"DDMM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	4*g+4*m,		"IIIIMMMM");
	#elif ALG == SEA || ALG == SW
		/**
		 * the Smith-Waterman algorithm and the seed-and-extend algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			m,				"M");
		sea_assert_align(ctx, "A", 				"AAA", 			m,				"M");
		sea_assert_align(ctx, "AAAA", 			"AA", 			m+m,			"MM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	4*m,			"MMMM");
	#endif

	/**
	 * when mismatches occurs.
	 */
	sea_assert_align(ctx, "AAAAAAAA", 		"AAAATAAA", 	7*m+x,			"MMMMXMMM");
	sea_assert_align(ctx, "TTTTTTTT", 		"TTTCTTTT", 	7*m+x,			"MMMXMMMM");
	sea_assert_align(ctx, "CCCCCCCC", 		"CCTCCCCC", 	7*m+x,			"MMXMMMMM");
	sea_assert_align(ctx, "GGGGGGGG", 		"GGCGGTGG", 	6*m+2*x,		"MMXMMXMM");

	/**
	 * when gaps with a base occurs on seq a (insertion).
	 */
	sea_assert_align(ctx, "AAAAATTTT", 		"AAAAAGTTTT", 	9*m+g,			"MMMMMIMMMM");
	sea_assert_align(ctx, "TTTTCCCCC", 		"TTTTACCCCC", 	9*m+g,			"MMMMIMMMMM");
	sea_assert_align(ctx, "CCCGGGGGG", 		"CCCTGGGGGG", 	9*m+g,			"MMMIMMMMMM");
	sea_assert_align(ctx, "GGGAATTT", 		"GGGCAAGTTT", 	8*m+2*g,		"MMMIMMIMMM");

	/**
	 * when gaps with a base occurs on seq b (deletion).
	 */
	sea_assert_align(ctx, "AAAAAGTTTT", 	"AAAAATTTT", 	9*m+g,			"MMMMMDMMMM");
	sea_assert_align(ctx, "TTTTACCCCC", 	"TTTTCCCCC", 	9*m+g,			"MMMMDMMMMM");
	sea_assert_align(ctx, "CCCTGGGGGG", 	"CCCGGGGGG", 	9*m+g,			"MMMDMMMMMM");
	sea_assert_align(ctx, "GGGCAAGTTT", 	"GGGAATTT", 	8*m+2*g,		"MMMDMMDMMM");

	/**
	 * when a gap longer than two bases occurs on seq a.
	 */
	sea_assert_align(ctx, "AAAAATTTTT", 	"AAAAAGGTTTTT", 10*m+2*g,		"MMMMMIIMMMMM");
	sea_assert_align(ctx, "TTTAAAAGGG", 	"TTTCAAAACGGG", 10*m+2*g,		"MMMIMMMMIMMM");

	/**
	 * when a gap longer than two bases occurs on seq b.
	 */
	sea_assert_align(ctx, "AAAAAGGTTTTT",	"AAAAATTTTT", 	10*m+2*g,		"MMMMMDDMMMMM");
	sea_assert_align(ctx, "TTTCAAAACGGG", 	"TTTAAAAGGG", 	10*m+2*g,	 	"MMMDMMMMDMMM");

	sea_clean(ctx);
	return;
}

#endif /* #if SEQ == ascii && ALN == ascii */
#endif /* #ifdef TEST */
#endif /* #if ALG == SEA || ALG == NW */

/**
 * end of edit_full.c
 */
#if 0
/*
 * myers bit parallel
 */
void myers_prep(char *a, int alen, char **ba, char *b, int blen, char **bb, unsigned long **peq)
{
	char al[256];
	int i, j, k, aln = 0;
	unsigned long vec;

	memset(al, 0, sizeof(char) * 256);
	for(i = 0; i < alen; i++) { al[(size_t)a[i]]++; }
	for(i = 0; i < blen; i++) { al[(size_t)b[i]]++; }

	for(i = 0; i < 256; i++) {
		if(al[i] != 0) {
			al[i] = (char)++aln;
//			print_debug("%c, %d\n", i, aln);
		}
	}
	*ba = (char *)malloc(sizeof(char) * (alen+63));
	*bb = (char *)malloc(sizeof(char) * blen);
	*peq = (unsigned long *)aligned_malloc(sizeof(unsigned long) * ((alen+63)/64) * aln, sizeof(unsigned long));
	for(i = 0; i < alen; i++) { (*ba)[i] = al[(size_t)a[i]]; }
	for(i = 0; i < blen; i++) { (*bb)[i] = al[(size_t)b[i]]; }
	for(i = 1; i < aln+1; i++) {
		for(j = 0; j < ((alen+63)/64); j++) {
			vec = 0UL;
			for(k = 0; k < 64; k++) {
				vec>>=1;
//				print_debug("%d, %d\n", (*ba)[j*64+k], i);
				if((*ba)[j*64+k] != (char)i) { vec |= (1UL<<63); }
			}
			(*peq)[i*((alen+63)/64)+j] = vec;
//			print_debug("(%d %d %d), %lx\n", i, j, k, vec);
		}
	}
	return;
}

int myers_fill(char *a, int alen, char *b, int blen, unsigned long *matrix, unsigned long *peq)
{
	int i, j, w;
	unsigned long ph = 0UL, mh = 0UL, xh, pv = 0UL, mv = 0UL, xv, eq;
	unsigned long *store = matrix, *prev = matrix;

	w = (alen+63)/64;
//	alen++; blen++;
	for(i = 0; i < 1; i++) {
		for(j = 0; j < blen; j++) {
			eq = peq[b[j]*w+i];
			xv = (((eq & ph) + ph) ^ ph) | eq;
			pv = mh | ~(xv | ph);
			mv = ph & xv;
			pv<<=1;
			mv<<=1;
			xh = eq | mv;
			ph = mv | ~(xh | pv);
			mh = pv & xh;
			*store++ = pv;
			*store++ = mv;
		}
	}
	for(i = 1; i < w; i++) {
		for(j = 0; j < blen; j++) {
			eq = peq[b[j]*w+i];
			xv = (((eq & ph) + ph) ^ ph) | eq;
			pv = mh | ~(xv | ph);
			mv = ph & xv;
			pv<<=1;
			pv |= (((*prev++)>>31)&0x1L);
			mv<<=1;
			mv |= (((*prev++)>>31)&0x1L);
			xh = eq | mv;
			ph = mv | ~(xh | pv);
			mh = pv & xh;
			*store++ = pv;
			*store++ = mv;
		}
	}
	return(0);
}

void myers_trace(char *a, int alen, char *b, int blen, unsigned long *matrix, unsigned long *peq)
{
	return;
}

/*
void myers_print_matrix(char *a, int alen, char *b, int blen, unsigned long *matrix, unsigned long *peq)
{
	int i, j, k;
	unsigned long p, m;
	unsigned long vecp, vecm;
	unsigned long *load = matrix;

	printf("    ");
	for(i = 0; i < alen; i++) {
		printf(" %c", a[i]);
	}
	printf("\n");
	for(i = 0; i < blen; i++) {
		printf("  %c ", b[i]);
		for(j = 0; j < ((alen+63)/64); j++) {
			vecp = *load++;
			vecm = *load++;
			for(k = 0; k < 64; k++) {
				p = vecp & (1UL<<k);
				m = vecm & (1UL<<k);
				printf(" %c", p ? '+' : (m ? '-' : '0'));
			}
		}
		printf("\n");
	}
	return;
}
*/

int myers_score(char *a, int alen, char *b, int blen, int *mat, int mlen)
{
//	int i, j, w, digit;
	unsigned long *matrix, *peq; //, *load;
//	unsigned long vecp, vecm;
	int score = 0, max = 0;
	char *ba, *bb;

	matrix = (unsigned long *)aligned_malloc(sizeof(unsigned long)*(((alen+63)/64)*(blen+1)+1)*2, sizeof(short)*8);
	if(matrix == NULL) { return -1; }
	myers_prep(a, alen, &ba, b, blen, &bb, &peq);
	score = myers_fill(ba, alen, bb, blen, matrix, peq);
	free(matrix);
	free(ba);
	free(bb);
	free(peq);
	return(max);
}

int myers_align(char *a, int alen, char *b, int blen, int *mat, int mlen)
{
	unsigned long *matrix, *peq;
	int score = 0;
	char *ba, *bb;

	matrix = (unsigned long *)aligned_malloc(sizeof(unsigned long)*(((alen+63)/64)*(blen+1)+1)*2, sizeof(short)*8);
	if(matrix == NULL) { return -1; }
	myers_prep(a, alen, &ba, b, blen, &bb, &peq);
	score = myers_fill(ba, alen, bb, blen, matrix, peq);
#ifdef DEBUG
	myers_print_matrix(a, alen, b, blen, matrix, peq);
#endif
	free(matrix);
	free(ba);
	free(bb);
	free(peq);
	return(score);
}

/**
 * @fn edit_full_matsize
 *
 * @brief returns the size of a matrix for the edit_full algorithm in bytes.
 *
 * @param[in] alen : the length of the input sequence a (in base pairs).
 * @param[in] blen : the length of the input sequence b (in base pairs).
 * @param[in] bandwidth : the width of the band (in cells).
 *
 * @return the size of a matrix in bytes.
 */
sea_int_t 
DECLARE_FUNC_GLOBAL(edit_full_matsize, SUFFIX)(
	sea_int_t alen,
	sea_int_t blen,
	sea_int_t bandwidth)
{
	sea_int_t wordwidth = (bandwidth + sizeof(unsigned long) - 1) / sizeof(unsigned long);
	return(4 * (alen + blen + bandwidth + 1) * wordwidth);
}

#endif
