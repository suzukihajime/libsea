
/**
 * @file bit.c
 *
 * @brief an implementation of interface to sea.c
 */
#include <stdlib.h>					/* for definition of NULL */
#include "bit.h"
#include "../include/sea.h"
#include "../util/util.h"
#include "../build/config.h"

#if HAVE_EDIT_FULL

#endif

#if HAVE_EDIT_BANDED
	#include "edit_banded_decl.h"	/** function declarations of Kimura's bit-parallel algorithms */
	#include "edit_banded_table.h"
#endif

#if HAVE_EDIT_FULL
#endif

#if HAVE_EDIT_BANDED

#endif

/**
 * @fn bit_get_sea
 *
 * @brief retrieve a pointer to alignment function.
 *
 * @param[in] flags : option flag bitfield. see parameter of sea_init in sea.h
 * @param[in] len : the maximum length of the query sequence. (unused in this function.)
 * @param[in] alg_type : an algorithm index. see table.th and sea.h.
 * @param[in] seq_type : an input sequence type index. see table.th and sea.h.
 * @param[in] aln_type : an output alignment type index. see table.th and sea.h.
 * @param[in] scmat : the scores in the dynamic programming. scmat[0] = M, scmat[1] = X, scmat[2] = Gi, and scmat[3] = Ge. see sea.h.
 * @param[in] xdrop : the xdrop threshold in positive integer.
 * @param[out] sea_matsize : a pointer to a function which returns the size of a matrix.
 * @param[out] sea_sea : a pointer to a function which do an alignment.
 *
 * @return SEA_SUCCESS if proper pointer is set, SEA_ERROR_UNSUPPORTED_ALG if not. 
 */
sea_int_t bit_get_sea(
	struct sea_funcs *fp,
	struct sea_params param,
	sea_int_t len,
	sea_int_t alg_type, sea_int_t seq_type, sea_int_t aln_type)
{
	switch(param.flags & (SEA_FLAGS_MASK_DP | SEA_FLAGS_MASK_COST)) {
	#if HAVE_EDIT_FULL
		case SEA_FULL_DP | SEA_UNIT_COST:
			/**
			 * the Myers' bit-parallel algorithm.
			 */
			fp->_sea_matsize = NULL;
			fp->_sea_sea = NULL;
			return SEA_ERROR_UNSUPPORTED_ALG;
	#endif

	#if HAVE_EDIT_BANDED
		case SEA_BANDED_DP | SEA_UNIT_COST:
			/**
			 * the Kimura's bit-parallel algorithm.
			 */
			*fp = edit_banded_table[alg_type][seq_type][aln_type];
			return SEA_SUCCESS;
	#endif

		default:
			fp->_sea_matsize = NULL;
			fp->_sea_sea = NULL;
			return SEA_ERROR_UNSUPPORTED_ALG;
	}
	return SEA_ERROR_UNSUPPORTED_ALG;
}
