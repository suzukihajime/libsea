
/**
 * @file bit.h
 * 
 * @brief a header for bit-parallel algorithms.
 *
 * @detail
 * This file does NOT include function declarations of individual alignment functions.
 * These declarations are automatically generated in the build step. See wscript
 * and util/table.th for details.
 */
#ifndef _BIT_H_INCLUDED
#define _BIT_H_INCLUDED

#include "../include/sea.h"

/**
 * @fn bit_get_sea
 *
 * @brief retrieve a pointer to alignment functions.
 *
 * @param[out] fp : a pointer to struct funcs (contains function pointers).
 * @param[in] param : dynamic programming parameters.
 * @param[in] len : the maximum length of the query sequence. (unused in this function.)
 * @param[in] alg_type : an algorithm index. see table.th and sea.h.
 * @param[in] seq_type : an input sequence type index. see table.th and sea.h.
 * @param[in] aln_type : an output alignment type index. see table.th and sea.h.
 *
 * @return TRUE if proper pointer is set, FALSE if not. 
 */
sea_int_t bit_get_sea(
	struct sea_funcs *fp,
	struct sea_params param,
	sea_int_t len,
	sea_int_t alg_type, sea_int_t seq_type, sea_int_t aln_type);

#endif /* #ifndef _BIT_H_INCLUDED */

/**
 * end of bit.h
 */