
/**
 * @file edit_banded.c
 *
 * @brief the bit-parallel banded alignment algorithm
 *
 * @detail
 * This file implements the Kimura's bit-parallel edit-distance algorithm, which is
 * equevalent to the linear-gap cost alignment algorithms with (M, X, G) = (2, 1, 0).
 * The detail of the algorithm is found in the paper in the reference below.
 *
 * This implementation serves as the two algorithm, the global alignment algorithm
 * and the seed-and-extend alignment algorithm. The selection of the algorithm is 
 * done by giving proper define flags to the compiler, such as -DALG=SEA or -DALG=NW.
 * This file also requires the input sequence format flag and output alignment 
 * string format flag. The simplest choices are SEQ=ascii and ALN=ascii, the other
 * options and the details of seqreaders / alnwriters are described in the
 * seqreader/seqreader.h.
 *
 * References:
 * 1. Kimura, K., Koike, A., and Nakai, K. (2012). A bit-parallel dynamic programming
 *        algorithm suitable for DNA sequence alignment. Journal of Bioinformatics and
 *        Computational Biology, 10(04).
 */
#ifdef DEBUG
	#include <stdio.h>
#endif
#ifdef MAIN
	#include <string.h>
#endif
#ifdef TEST
 	#include "../util/tester.h"
#endif

#include <stdlib.h>						/* for definition of NULL */
#include <limits.h>						/* for definition of ULONG_MAX */
#include "../include/sea.h"				/* for API definitions, flag definitions, and structs */
#include "../util/util.h"				/* for internal utility functions */
#include "../seqreader/seqreader.h"		/* for input sequence reader and output alignment writer */
#include "../util/bench.h"				/* for benchmarking */

/**
 * global util macros.
 */
#define	REG_BITWIDTH						( 8*sizeof(unsigned long) )
#define EXTRACT_BIT(reg, pos)				( ((reg)>>(pos) & 0x01UL) )
#define CENTER(x) 							( EXTRACT_BIT(x, REG_BITWIDTH/2) )
#define MSB(x) 								( EXTRACT_BIT(x, REG_BITWIDTH-1) )

/**
 * PUSH macros. used in the fill-in step to push a fetched base
 * into sequence comparison buffer.
 */
#define PUSHR(reg, a)						{ (reg)<<=1; (reg)|=(a); }
#define PUSHL(reg, a)						{ (reg)>>=1; (reg)|=(((unsigned long)(a))<<(REG_BITWIDTH-1)); }

/**
 * @struct band
 * @brief a struct containing a set of deltas in a line.
 */
struct band {
	unsigned long pv, mv, ph, mh;
};

/**
 * @struct pos
 * @brief a struct containing a position
 */
struct pos {
	sea_int_t i, j, p, q;
};

/**
 * coordinate conversion macros.
 * see naive_linear_banded.c for composition of coordinates.
 */
#define	ADDR(p, q)					( p )
#define ADDRI(x, y) 				( ADDR(COP(x, y), COQ(x, y)) )
#define COX(p, q)					( ((p)>>1) - (q) )
#define COY(p, q)					( (((p)+1)>>1) + (q) )
#define COP(x, y)					( (x) + (y) )
#define COQ(x, y) 					( ((y)-(x))>>1 )
#define TOPQ(p, q) 					( - !(p&0x01) )
#define LEFTQ(p, q) 				( (p&0x01) )

/**
 * delta extraction macros. returns one from {+1, 0, -1}.
 */
#define DV(ptr, p, q)				( (((ptr[p].pv)>>((q)+REG_BITWIDTH/2)) & 0x01) - (((ptr[p].mv)>>((q)+REG_BITWIDTH/2)) & 0x01) )
#define DH(ptr, p, q)				( (((ptr[p].ph)>>((q)+REG_BITWIDTH/2)) & 0x01) - (((ptr[p].mh)>>((q)+REG_BITWIDTH/2)) & 0x01) )

/**
 * direction macros. represents which direction the band came from. (from upward or leftward)
 */
#define DIR_TOP 					( 0x01 )
#define DIR_LEFT 					( 0 )
#define DIR(p)						( ((p) & 0x01) ? DIR_TOP : DIR_LEFT )

/**
 * vector load / store macros.
 * each vector must be represented in unsigned long.
 */
#define INCR_LOAD_MATRIX(m, w, x, y, z)		{ (w) = *((m)++); \
											  (x) = *((m)++); \
											  (y) = *((m)++); \
											  (z) = *((m)++); }
#define DECR_LOAD_MATRIX(m, w, x, y, z)		{ (z) = *(--(m)); \
											  (y) = *(--(m)); \
											  (x) = *(--(m)); \
											  (w) = *(--(m)); }
#define INCR_STORE_MATRIX(m, w, x, y, z)	{ *((m)++) = (w); \
											  *((m)++) = (x); \
											  *((m)++) = (y); \
											  *((m)++) = (z); }
#define DECR_STORE_MATRIX(m, w, x, y, z)	{ *(--(m)) = (z); \
											  *(--(m)) = (y); \
											  *(--(m)) = (x); \
											  *(--(m)) = (w); }

/**
 * internal function declarations.
 */
#if ALG == SEA || ALG == NW
struct pos
DECLARE_FUNC(edit_banded_fill, SUFFIX)(
	struct sea_result *aln,
	struct band *mat);

struct pos
DECLARE_FUNC(edit_banded_search, SUFFIX)(
	struct sea_result *aln,
	struct band *mat,
	struct pos e);

sea_int_t
DECLARE_FUNC(edit_banded_trace, SUFFIX)(
	struct sea_result *aln,
	struct band *mat,
	struct pos m);
#endif /* #if ALG == SEA || ALG == NW */

/**
 * @fn edit_banded
 *
 * @brief the Kimura's bit-parallel edit-distance algorithm
 *
 * @param[ref] aln : a pointer to a sea_result structure. aln must NOT be NULL. (a structure which contains an alignment result.)
 * @param[in] aln->a : a pointer to a query sequence a.
 * @param[inout] aln->apos : (input) the start position of the search section on sequence a. (0 <= apos < length(sequence a)) (output) the start position of the alignment.
 * @param[inout] aln->alen : (input) the length of the search section on sequence a. (0 < alen) (output) the length of the alignment.
 * @param[in] aln->b : a pointer to a query sequence b.
 * @param[inout] aln->bpos : same as apos.
 * @param[inout] aln->blen : same as alen.
 *
 * @param[out] aln->aln : a pointer to the alignment string.
 * @param[inout] aln->len : (input) the reserved length of the aln->aln. (output) the length of the alignment string.
 * @param[out] aln->score : an alignment score.
 *
 * @param[in] scmat : unused parameter.
 * @param[in] xdrop : unused parameter. 
 * @param[in] bandwidth : unused parameter.
 * @param[in] mat : a pointer to the DP matrix.
 * @param[in] matsize : the size of the DP matrix.
 *
 * @return status. zero means success. see sea.h for the other error code.
 */
sea_int_t 
DECLARE_FUNC_GLOBAL(edit_banded, SUFFIX)(
	struct sea_result *aln,
	struct sea_params param,
	void *mat)
{
	/**
	 * a matrix composition and coordinates
	 *
	 * In this algorithm, a dynamic programming matrix is represented as a stack of 'bars'.
	 * Each bar consists of a set of four difference bit vectors; pv, mv, ph, and mh.
	 * pv and mv are conbined to hold vertical differences of scores, and ph and mh hold
	 * horizontal differences. ('1' in a pv vector means a difference value is positive.)
	 *
	 * a coordinates of a matrix follow one in the 'naive_linear_banded.c'. see comments
	 * in 'naive_linear_banded.c' for its details.
	 */
	
	/**
	 * the Smith-Waterman algorithm is not available in the unit-cost algorithm.
	 */
	#if ALG == SW
	 	return 0;
	#elif ALG == NW || ALG == SEA
		sea_int_t retval = SEA_SUCCESS;
		struct pos e,		/** a termination point of fill-in step */
				   m;		/** a maximum score position */
		DECLARE_BENCH(fill);
		DECLARE_BENCH(search);
		DECLARE_BENCH(trace);

		/**
		 * fillin a matrix
		 */
		START_BENCH(fill);		/** start of fill step */
		e = CALL_FUNC(edit_banded_fill, SUFFIX)(
			aln, (struct band *)mat);
		END_BENCH(fill);
		/**
		 * search minimum score position
		 */
		START_BENCH(search);
		m = CALL_FUNC(edit_banded_search, SUFFIX)(
			aln,
			(struct band *)mat,
			e);
		END_BENCH(search);

		if(aln->aln == NULL) { return SEA_SUCCESS; }
		/**
		 * if aln->aln is not NULL, do traceback.
		 */
		START_BENCH(trace);
		retval = CALL_FUNC(edit_banded_trace, SUFFIX)(
			aln,
			(struct band *)mat,
			m);
		END_BENCH(trace);
		return(retval);
	#else
		#error "Invalid number of ALG. Check the definition flag of the ALG."
		return 0;
	#endif
}

#if ALG == SEA || ALG == NW

/**
 * @fn edit_banded_fill
 *
 * @brief fill in a matrix
 *
 * @param[ref] aln : a pointer to a sea_result struct.
 * @param[in] xdrop : currently unused.
 * @param[in] mat : a pointer to a matrix.
 * @param[in] matlen : the length of the p-direction of the matrix.
 *
 * @return an end position of the extension.
 */
struct pos
DECLARE_FUNC(edit_banded_fill, SUFFIX)(
	struct sea_result *aln,
	struct band *mat)
{
	/**
	 * Band advancing operation:
	 * The Kimura's bit-parallel edit-distance algorithm uses difference of scores
	 * between adjacent cells. In the matrix of edit-distance algorithms, the differences
	 * are limited to -1, 0, or +1. So in this algorithm the differences are stored in
	 * bit-vectors, with each bit represents a +1 or -1 from the upper or left cell.
	 *
	 * Band composition:
	 * Each band is represented in a set of four bit-vectors, pv, mv, ph, and mh.
	 * The set of vectors are handled with the struct band (declared above). Each bit
	 * in the pv (or mv) vector is set when the vertical difference of the cell is
	 * positive (or negative). In the same way, the ph and mh vectors are assigned
	 * to hold horizontal differences.
	 *
	 * Memory layout:
	 * We adopt both (i, j)-coordinates and (p, q)-coordinates as described in 
	 * naive_linear_banded.c. The difference values of a cell (the difference between
	 * upper and left cells) at (p, q) is stored at (struct band *)mat[p]. For example,
	 * the vertical difference between the cell a and the cell b in the figure is 
	 * stored in mat[1].pv and mat[1].mv. Similarly the difference between b and c is
	 * stored in mat[2].ph and mat[2].mh. The mat[0] contains nonsense vectors, since
	 * the upper and the left cells of the cell at (0, 0) do not exist.
	 *
	 *              input sequence a
	 *                 A  T  A  C  ..
	 *           O  0  1  2  3  4  ..             i
	 *            +-------------------------------->
	 *           0| a|  |  |  |  |
	 *            |--+--+--+--+--+--+
	 *  input  A 1| b| c|  |  |  |  |
	 * sequence   |--+--+--+--+--+--+--+
	 *    b    T 2|  |  |  |  |  |  |  |
	 *            |--+--+--+--+--+--+--+--+
	 *         C 3|  |  |  |  |  |  |  |  |
	 *            |--+--+--+--+--+--+--+--+--+
	 *         C 4|  |  |  |  |  |  |  |  |  |
	 *            |  +--+--+--+--+--+--+--+--+--+
	 *         : :|     |  |  |  |  |  |  |  |  |
	 *            |     +--+--+--+--+--+--+--+--+
	 *            |        |  |  |  |  |  |  |  |
	 *            |        +--+--+--+--+--+--+--+
	 *          j v
	 *
	 * Termination:
	 * The band advancing operation is terminated when no cells in the band represents
	 * valid cells (cells inside i <= alen and j <= blen), or the accumulation of
	 * mismatches exceeds the xdrop value. The score at the center cell of the
	 * last-calculated band is stored in aln->score.
	 */
	sea_int_t const bw = REG_BITWIDTH;				/** bandwidth */
	sea_int_t i, j;
	sea_int_t xcnt = 0;								/** center mismatch accumulator */
	unsigned long const ones = ULONG_MAX;			/** 0xffffffffffffffff */
	unsigned long register wql, wqh, wtl, wth;		/** sequence buffer. wql and wqh holds 64-base substring of seq.a, wtl and wth holds seq.b */
	unsigned long register pv, mv, mh, ph;			/** difference bit vectors */
	unsigned long register eq, xv, pv_, mv_, xh;	/** temporary bit vectors */
	unsigned long *matptr = (unsigned long *)mat;

	sea_int_t amat = aln->alen + bw/2,				/** size of matirx */
			  bmat = aln->blen + bw/2;
	struct pos e;

	DECLARE_SEQ(a);
	DECLARE_SEQ(b);

	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);

	pv = ones<<(bw/2+1); mv = ones>>(bw/2+1);	/** initial dv vector at (0, 0) */
	ph = ones>>(bw/2); mh = ones<<(bw/2);		/** initial dh vector at (0, 0) */
	INCR_STORE_MATRIX(matptr, pv, mv, ph, mh);	/** phantom, but not nonsense, vector: p = 1 */
	xcnt = 0;									/** initial score at (0, 0) */

	wql = wqh = 0;								/** padding for seq.a is 0 */
	wtl = wth = ones;							/** padding for seq.b is 1 */
	for(i = 0; i < bw/2; i++) {
		FETCH(a, i); PUSHR(wql, DECODE_LOW(a)); PUSHR(wqh, DECODE_HIGH(a));
	}
	for(j = 0; j < bw/2-1; j++) {
		FETCH(b, j); PUSHL(wtl, DECODE_LOW(b)); PUSHL(wth, DECODE_HIGH(b));
	}

	/**
	 * @macro KIMURA_REGISTER_UPDATE
	 * @brief a set of band advancing operations.
	 */
	#define KIMURA_REGISTER_UPDATE() { \
		eq = ~(wql ^ wtl) & ~(wqh ^ wth); \
		xv = eq | mv; pv_ = mh | ~(xv | ph); mv_ = ph & xv; \
		xh = eq | mh; ph = mv | ~(xh | pv); mh = pv & xh; \
		pv = pv_; mv = mv_; \
	}
	/** end of KIMURA_REGISTER_UPDATE */
	i = COX(0, 0); j = COY(0, 0);				/** i = 0 and j = 1 */
	while(i < amat && j < bmat) {
		pv >>= 1; mv >>= 1;
		FETCH(b, j + bw/2-1); PUSHL(wtl, DECODE_LOW(b)); PUSHL(wth, DECODE_HIGH(b));
		KIMURA_REGISTER_UPDATE();
		INCR_STORE_MATRIX(matptr, pv, mv, ph, mh);
		xcnt += (CENTER(pv) - CENTER(mv));
		j += 1;		/** go downward in the former half of the loop */

		ph <<= 1; mh <<= 1;
		FETCH(a, i + bw/2); PUSHR(wql, DECODE_LOW(a)); PUSHR(wqh, DECODE_HIGH(a));
		KIMURA_REGISTER_UPDATE();
		INCR_STORE_MATRIX(matptr, pv, mv, ph, mh);
		xcnt += (CENTER(ph) - CENTER(mh));
		i += 1;		/** go rightward in the latter half of the loop */ 
	}
	#undef KIMURA_REGISTER_UPDATE

	e.i = i; e.j = j;
	e.p = COP(i, j); e.q = COQ(i, j);			/** e.q == 0; */
	aln->score = xcnt;	/** save mismatch count on the center line, but score */
	return(e);
}

/**
 * @fn edit_banded_search
 *
 * @brief search a cell with minimum score
 *
 * @return a struct pos which contains minimum score position.
 */
struct pos
DECLARE_FUNC(edit_banded_search, SUFFIX)(
	struct sea_result *aln,
	struct band *mat,
	struct pos e)
{
	/**
	 * Minimum score search strategy:
	 * In the Needleman-Wunsch algorithm, the score we want to know is
	 * in the cell A. The aln->score contains the score of the center cell
	 * of the last-calculated band (D in the figure), we have to restore
	 * the score at A accumulating the differences between D and A.
	 * 
	 * In the seed-and-extend algorithm, we have to search the position of
	 * the cell which contains the minimum score in the bottom-right edge of
	 * the band (B -> A -> C in the figure). We first calculate the score
	 * in B with the same way with the Needleman-Wunsch algorithm, then
	 * calculate scores one by one along with the path from B through A to C.
	 *
	 *              input sequence a
	 *                 A  T  A  C  ..
	 *           O  0  1  2  3  4  ..           alen    i
	 *            +-------------------------------------->
	 *           0| 0| 1| 2| 3| 4|                 :
	 *            |--+--+--+--+--+--+              :
	 *  input  A 1| 1| 0| 1| 2| 3|  |              :
	 * sequence   |--+--+--+--+--+--+--+           :
	 *    b    T 2| 2| 1| 0| 1| 2|  |  |      (A)  :
	 *            |--+--+--+--+--+--+--+--+        : 
	 *         C 3| 3| 2| 1| 1| 2|  |  |  |        :
	 *            |--+--+--+--+--+--+--+--+--+     :
	 *         C 4|  | 3| 2| 2| 1|  |  |  |  |     :
	 *            |  +--+--+--+--+--+--+--+--+--+  :
	 *         : :|     |  |  |  |  |  |  |  |  |  :
	 *            |     +--+--+--+--+--+--+--+--+--+
	 *            |        |  |  |  |  |  |  |  | B|
	 *            |  (B)   +--+--+--+--+--+--+--+--+
	 *            |           |  |  |  |  |  |  | v|
	 *            |           +--+--+--+--+--+--+--+
	 *            |              |  |  |  |  |  | v|
	 *            |              +--+--+--+--+--+--+
	 *       blen |                 | C|<=|<=|<=| A|
	 *            |.................+--+--+--+--+--+
	 *            |                                  :
	 *            |                                +--+
	 *            |                            ... | D|
	 *            |                                +--+
	 *          j v
	 */
	sea_int_t const bw = REG_BITWIDTH;				/** bandwidth */
	struct pos m;
	sea_int_t ep = e.p,								/** end pos in the fill-in step */
			  eq = e.q,
			  mp, mq;
	sea_int_t xcnt = aln->score;					/** score at (ep, eq) */

	/**
	 * first assign a coordinate of A (or B in the global alignment) to (mp, mq)
	 */
	#if ALG == NW
	 	m.p = mp = COP(aln->alen, aln->blen);
	 	m.q = mq = COQ(aln->alen, aln->blen);
	#else
		sea_int_t p, q, mpa, mpb;
		sea_int_t mi, mj;
		sea_int_t max = 0;

	 	mi = MIN2(COX(e.p, e.q), aln->alen);		/** x-coordinate of the bottom-right corner */
	 	mj = MIN2(COY(e.p, e.q), aln->blen);		/** y-coordinate of the bottom-right corner */
		mpa = 2*(-bw/2 + mi);						/** p-coordinate of q = -bw/2 and x = mi */
		mpb = 2*(mj - (-bw/2));						/** p-coordinate of q = -bw/2 and y = mj */
		mp = MIN2(mpa, mpb); mq = -bw/2;			/** search start position B */
		if(COY(mp, mq) < 0) {						/** if start pos is out of matrix, set (mi, 0) instead */
			mp = COP(mi, 0); mq = COQ(mi, 0);
		}
	#endif

	/**
	 * restore xcnt of (mp, mq)
	 * first go along with the center line until ep > mp. then turn to the right angle toward mq.
	 */
	while(ep > mp) { xcnt -= (DIR(ep) == DIR_TOP) ? DV(mat, ep, 0) : DH(mat, ep, 0); ep--; }
	while(eq < mq) { xcnt += (DV(mat, ep, eq+1) - DH(mat, ep, eq)); eq++; }
	while(eq > mq) { xcnt += (DH(mat, ep, eq-1) - DV(mat, ep, eq)); eq--; }

	#if ALG == NW
		aln->score = mp - xcnt;						/** here we have obtained the score */
	#else
		p = mp; q = mq;
		/** path from B to A */
		while(COY(mp, mq) < mj && mq < bw/2) {
			if(mp - xcnt > max) { max = mp - xcnt; m.p = mp; m.q = mq; }
			mq += (mp & 0x01); mp++; xcnt += DV(mat, mp, mq);
		}
		/** path from A to C */
		while(COX(mp, mq) >= 0 && mq < bw/2) {
			if(mp - xcnt > max) { max = mp - xcnt; m.p = mp; m.q = mq; }
			/** if the same score occurs, choose one with the shortest alignment path. */
			if(mp - xcnt == max && mp < m.p) { m.p = mp; m.q = mq; }
			xcnt -= DH(mat, mp, mq); mq += (mp & 0x01); mp--;
		}
		aln->score = max;
		aln->alen = COX(m.p, m.q); aln->blen = COY(m.p, m.q);
	#endif
 	return(m);
}

/**
 * @fn edit_banded_trace
 *
 * @brief do traceback
 */
sea_int_t
DECLARE_FUNC(edit_banded_trace, SUFFIX)(
	struct sea_result *aln,
	struct band *mat,
	struct pos m)
{
	sea_int_t const bw = REG_BITWIDTH;				/** bandwidth */
	sea_int_t mi = COX(m.p, m.q),
			  mj = COY(m.p, m.q),
			  mp = m.p,
			  mq = m.q;
	sea_int_t dh, diag, sc;

	DECLARE_SEQ(a);
	DECLARE_SEQ(b);
	DECLARE_ALN(l);

	CLEAR_SEQ(a, aln->a, aln->apos, aln->alen);
	CLEAR_SEQ(b, aln->b, aln->bpos, aln->blen);
	CLEAR_ALN(l, aln->aln, aln->len);

	FETCH(a, mi-1); FETCH(b, mj-1);
	while(mi > 0 && mj > 0) {
		dh = DH(mat, mp, mq);
		diag = dh + DV(mat, mp-1, mq + LEFTQ(mp, mq));
		sc = !COMPARE(a, b);			/** READ(a) == READ(b) ? 0 : 1 */
		if(sc == diag) {
			mp -= 2;					/** mq does not change */
			mi--; FETCH(a, mi-1);
			mj--; FETCH(b, mj-1);
			PUSH(l, (sc == 0) ? MATCH_CHAR : MISMATCH_CHAR);
		} else if(mq != (bw/2-1) && dh == 1) {
			mq += LEFTQ(mp, mq); mp--;
			mi--; FETCH(a, mi-1); PUSH(l, DELETION_CHAR);
		} else if(mq != -bw/2 && DV(mat, mp, mq) == 1) {
			mq += TOPQ(mp, mq); mp--;
			mj--; FETCH(b, mj-1); PUSH(l, INSERTION_CHAR);
		} else {
			return SEA_ERROR_OUT_OF_BAND;
		}
	}
	while(mi > 0) { mi--; PUSH(l, DELETION_CHAR); }
	while(mj > 0) { mj--; PUSH(l, INSERTION_CHAR); }
	aln->len = LENGTH(l);
	REVERSE(l);
	return SEA_SUCCESS;
}

/**
 * @fn edit_banded_matsize
 *
 * @brief returns the size of a matrix for the edit_banded algorithm in bytes.
 *
 * @param[in] alen : the length of the input sequence a (in base pairs).
 * @param[in] blen : the length of the input sequence b (in base pairs).
 * @param[in] bandwidth : the width of the band (in cells).
 *
 * @return the size of a matrix in bytes.
 */
sea_int_t 
DECLARE_FUNC_GLOBAL(edit_banded_matsize, SUFFIX)(
	sea_int_t alen,
	sea_int_t blen,
	sea_int_t bandwidth)
{
	sea_int_t wordwidth = (bandwidth + sizeof(unsigned long) - 1) / sizeof(unsigned long);
	return(4 * (alen + blen + bandwidth + 1) * wordwidth);
}

/**
 * @fn main
 *
 * @brief gets two ascii strings from stdin, align strings with naive_affine_full, and print the result.
 */
#ifdef MAIN

int main(int argc, char *argv[])
{
	int matlen, alnlen;
	void *mat;
	struct sea_result aln;
	struct sea_params param;

	param.flags = 0;
	param.m = 2;
	param.x = -3;
	param.gi = -4;
	param.ge = -1;
	param.xdrop = 12;
	param.bandwidth = 32;

	aln.a = argv[1];
	aln.alen = strlen(aln.a);
	aln.apos = 0;
	aln.b = argv[2];
	aln.blen = strlen(aln.b);
	aln.bpos = 0;
	alnlen = aln.alen + aln.blen;
	aln.len = alnlen;

	alnlen = aln.alen + aln.blen;
	matlen = CALL_FUNC(edit_banded_matsize, SUFFIX)(
		aln.alen, aln.blen, param.bandwidth);

	aln.aln = (void *)malloc(alnlen);
	mat = (void *)malloc(matlen);
	CALL_FUNC(edit_banded, SUFFIX)(&aln, param, mat);

	printf("%d, %d, %s\n", aln.score, aln.len, aln.aln);

	free(mat);
	free(aln.aln);
	return 0;
}

#endif /* #ifdef MAIN */


/**
 * unittest functions.
 *
 * give a definition of TEST to a compiler, e.g. -DTEST=1, to make a library for tests.
 */
#ifdef TEST
#if SEQ == ascii && ALN == ascii

/**
 * @fn test_1_edit_banded
 *
 * @brief a unittest function of edit_banded.
 *
 * @detail
 * This function is an aggregation of simple fixed ascii queries.
 * In this function, a sea_assert_align macro in tester.h is called. This macro
 * calls sea_align function with given context, checks if the score and the alignment
 * string is the same as the given score and string. If both or one of the results
 * are different, the macro prints the failure status (filename, lines, input sequences,
 * and the content of sea_result) and dumps a content of dynamic programming matrix
 * to dumps.log.
 */
void
DECLARE_FUNC_GLOBAL(test_1_edit_banded, SUFFIX)(
	void)
{
	sea_int_t m = 2,
			  x = 1,
			  g = 0;
	struct sea_context *ctx;

	ctx = sea_init_fp(
		SEA_BANDWIDTH_64,
		CALL_FUNC(edit_banded, SUFFIX),
		CALL_FUNC(edit_banded_matsize, SUFFIX),
		m, x, g, 0,			/** the default blast scoring scheme */
		10000);				/** xdrop threshold */

	/**
	 * when both sequences are empty
	 */
	sea_assert_align(ctx, "", 				"", 			0,			"");
	/**
	 * when one sequence is empty
	 */
	sea_assert_align(ctx, "AAA", 			"", 			0,			"");
	sea_assert_align(ctx, "TTTTTTT", 		"", 			0,			"");
	sea_assert_align(ctx, "", 				"AAA", 			0,			"");
	sea_assert_align(ctx, "", 				"TTTTTTT", 		0,			"");
	sea_assert_align(ctx, "CTAG",			"", 			0,			"");

	/**
	 * a match
	 */
	sea_assert_align(ctx, "A",				"A", 			m,			"M");
	sea_assert_align(ctx, "C", 				"C", 			m,			"M");
	sea_assert_align(ctx, "G", 				"G", 			m,			"M");
	sea_assert_align(ctx, "T", 				"T", 			m,			"M");

	/**
	 * a mismatch
	 */
	sea_assert_align(ctx, "A", 				"C", 			x,			"X");
	sea_assert_align(ctx, "C", 				"A", 			x,			"X");
	sea_assert_align(ctx, "G", 				"T", 			x,			"X");
	sea_assert_align(ctx, "T", 				"A", 			x,			"X");

	/**
	 * homopolymers with different lengths.
	 */
	#if ALG == NW
		/**
		 * the Needleman-Wunsch algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			g+m,			"IM");
		sea_assert_align(ctx, "A", 				"AAA", 			g+g+m,			"IIM");
		sea_assert_align(ctx, "AAAA", 			"AA", 			g+g+m+m,		"DDMM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	4*g+4*m,		"IIIIMMMM");
	#elif ALG == SEA || ALG == SW
		/**
		 * the Smith-Waterman algorithm and the seed-and-extend algorithm
		 */
		sea_assert_align(ctx, "A", 				"AA", 			m,				"M");
		sea_assert_align(ctx, "A", 				"AAA", 			m,				"M");
		sea_assert_align(ctx, "AAAA", 			"AA", 			m+m,			"MM");
		sea_assert_align(ctx, "TTTT", 			"TTTTTTTT", 	4*m,			"MMMM");
	#endif

	/**
	 * when mismatches occurs.
	 */
	sea_assert_align(ctx, "AAAAAAAA", 		"AAAATAAA", 	7*m+x,			"MMMMXMMM");
	sea_assert_align(ctx, "TTTTTTTT", 		"TTTCTTTT", 	7*m+x,			"MMMXMMMM");
	sea_assert_align(ctx, "CCCCCCCC", 		"CCTCCCCC", 	7*m+x,			"MMXMMMMM");
	sea_assert_align(ctx, "GGGGGGGG", 		"GGCGGTGG", 	6*m+2*x,		"MMXMMXMM");

	/**
	 * when gaps with a base occurs on seq a (insertion).
	 */
	sea_assert_align(ctx, "AAAAATTTT", 		"AAAAAGTTTT", 	9*m+g,			"MMMMMIMMMM");
	sea_assert_align(ctx, "TTTTCCCCC", 		"TTTTACCCCC", 	9*m+g,			"MMMMIMMMMM");
	sea_assert_align(ctx, "CCCGGGGGG", 		"CCCTGGGGGG", 	9*m+g,			"MMMIMMMMMM");
	sea_assert_align(ctx, "GGGAATTT", 		"GGGCAAGTTT", 	8*m+2*g,		"MMMIMMIMMM");

	/**
	 * when gaps with a base occurs on seq b (deletion).
	 */
	sea_assert_align(ctx, "AAAAAGTTTT", 	"AAAAATTTT", 	9*m+g,			"MMMMMDMMMM");
	sea_assert_align(ctx, "TTTTACCCCC", 	"TTTTCCCCC", 	9*m+g,			"MMMMDMMMMM");
	sea_assert_align(ctx, "CCCTGGGGGG", 	"CCCGGGGGG", 	9*m+g,			"MMMDMMMMMM");
	sea_assert_align(ctx, "GGGCAAGTTT", 	"GGGAATTT", 	8*m+2*g,		"MMMDMMDMMM");

	/**
	 * when a gap longer than two bases occurs on seq a.
	 */
	sea_assert_align(ctx, "AAAAATTTTT", 	"AAAAAGGTTTTT", 10*m+2*g,		"MMMMMIIMMMMM");
	sea_assert_align(ctx, "TTTAAAAGGG", 	"TTTCAAAACGGG", 10*m+2*g,		"MMMIMMMMIMMM");

	/**
	 * when a gap longer than two bases occurs on seq b.
	 */
	sea_assert_align(ctx, "AAAAAGGTTTTT",	"AAAAATTTTT", 	10*m+2*g,		"MMMMMDDMMMMM");
	sea_assert_align(ctx, "TTTCAAAACGGG", 	"TTTAAAAGGG", 	10*m+2*g,	 	"MMMDMMMMDMMM");

	sea_clean(ctx);
	return;
}

#endif /* #if SEQ == ascii && ALN == ascii */
#endif /* #ifdef TEST */
#endif /* #if ALG == SEA || ALG == NW */

/**
 * end of edit_banded.c
 */
