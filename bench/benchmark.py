#! /usr/bin/env python
# coding: utf-8

# libsea benchmark runner
msg = '''
This is a benchmark runner script to generate tables and figures in the paper.
Running this script with an option 'fine', or
'$ python benchmark.py bench fine hoge.txt; python benchmark.py plot hoge.txt',
you can get the same benchmarking result as in the paper.

This script requires python 2.7 or 3.3 to run, matplotlib to generate figure,
and the python csv library to generate table.

Usage:
    ./benchmark.py <command>

Commands:
    bench <quality> <filename.txt>
        Run benchmark program and save the result to file. This command
        requires quality options.

        Quality options:
            rough:
                Run benchmarking program with -c 10 (measure ten times
                iteration). This option runs very fast but the result may
                be too rough to compare.
        
            defalut:
                Run benchmarking program with -c 1000. This takes a few
                minutes to finish.
        
            fine:
                Run program with -c 100000. This takes a few dozens of
                minutes to finish but the result may be very fine!

    plot <filename.txt>
        Plot figures and a table, which appear in the paper.

    accuracy
        Generates a table of search accuracy benchmark.
'''

def benchmark(len, cnt):

	import subprocess

	res_list = [l.split(', ') for l
		in subprocess.check_output(['../build/bench/benchmark',
			'-c', str(cnt), '-l', str(len)])
			.decode('utf-8').split('\n')]
	
	res_dict = {}
	for elem in res_list:
		res_dict[elem[0]] = [int(e) for e in elem[1:6]]
	return(res_dict)

def benchmark_all(cnt):
	result = {'cnt': cnt}
	len = list(range(10, 61, 5)) + list(range(100, 10101, 250)) + [10000]
	for l in len:
		result[l] = benchmark(l, cnt)
	return(result)

def plot_len(result):
	
	import pylab
	pylab.figure(figsize = (15, 9), dpi = 72)

	width = 1.5 	# linewidth
	size = 20 		# fontsize
	
	# gather benchmark results of 8-bit diag algorithms
	res = {'tick': [], 'NL16': [], 'DL16S8': [], 'BL16S8': [], 'NA16': [], 'DA16S8': [], 'BA16S8': []}
	for i in range(10, 61, 5):
		#r = benchmark(i, cnt)
		if i in result:
			r = result[i]
		else:
			r = result[10]
		if 'cnt' in result:
			cnt = result['cnt']
		else:
			cnt = 0
		res['tick'].append(i)
		# linear-gap cost
		res['NL16'].append(r['NL16'][3] / (1000.0 * cnt))		# append total elapsed time
		res['DL16S8'].append(r['DL16S8'][3] / (1000.0 * cnt))
		res['BL16S8'].append(r['BL16S8'][3] / (1000.0 * cnt))
		# affine-gap cost
		res['NA16'].append(r['NA16'][3] / (1000.0 * cnt))		# append total elapsed time
		res['DA16S8'].append(r['DA16S8'][3] / (1000.0 * cnt))
		res['BA16S8'].append(r['BA16S8'][3] / (1000.0 * cnt))

	# plot a figure of the linear-gap cost
	pylab.subplot(2, 2, 1)
	l0, = pylab.plot(res['tick'], res['DL16S8'], color = 'black', linewidth = width, linestyle = '-',  label = 'DL16S8')
	l1, = pylab.plot(res['tick'], res['BL16S8'], color = 'black', linewidth = width, linestyle = '--', label = 'BL16S8')
	l2, = pylab.plot(res['tick'], res['NL16'],   color = 'black', linewidth = width, linestyle = '-',  label = 'NL16')
	l2.set_dashes([6, 3, 2, 3, 2, 3])
	pylab.xlim(9, 61)
	pylab.xticks(fontsize = size)
	pylab.ylim(0, 10)
	pylab.yticks(fontsize = size)
	pylab.ylabel('Total elapsed time (us)', fontsize = size)
	pylab.title('(a) 16-cell wide linear-gap cost', fontsize = size)
	pylab.legend(loc = 'best', fontsize = 12)

	# plot a figure of the affine-gap cost
	pylab.subplot(2, 2, 2)
	l0, = pylab.plot(res['tick'], res['DA16S8'], color = 'black', linewidth = width, linestyle = '-',  label = 'DA16S8')
	l1, = pylab.plot(res['tick'], res['BA16S8'], color = 'black', linewidth = width, linestyle = '--', label = 'BA16S8')
	l2, = pylab.plot(res['tick'], res['NA16'],   color = 'black', linewidth = width, linestyle = '-',  label = 'NA16')
	l2.set_dashes([6, 3, 2, 3, 2, 3])
	pylab.xlim(9, 61)
	pylab.xticks(fontsize = size)
	pylab.ylim(0, 10)
	pylab.yticks(fontsize = size)
	pylab.title('(b) 16-cell wide affine-gap cost', fontsize = size)
	pylab.legend(loc = 'best', fontsize = 12)
	
	# gather benchmark results of 16-bit diag and diff algorithms
	res = {'tick': [], 'NL32': [], 'DL32S8': [], 'DL32S8E': [], 'BL32S16': [], 'NA32': [], 'DA32S8': [], 'DA32S8E': [], 'BA32S16': []}
	for i in range(100, 10101, 250):
		#r = benchmark(i, cnt)
		if i in result:
			r = result[i]
		else:
			r = result[10]
		if 'cnt' in result:
			cnt = result['cnt']
		else:
			cnt = 0

		res['tick'].append(i)
		# linear-gap cost
		res['NL32'].append(r['NL32'][3] / (1000.0 * cnt))		# append total elapsed time
		res['DL32S8'].append(r['DL32S8'][3] / (1000.0 * cnt))
		res['DL32S8E'].append(r['DL32S8E'][3] / (1000.0 * cnt))
		res['BL32S16'].append(r['BL32S16'][3] / (1000.0 * cnt))
		# affine-gap cost
		res['NA32'].append(r['NA32'][3] / (1000.0 * cnt))		# append total elapsed time
		res['DA32S8'].append(r['DA32S8'][3] / (1000.0 * cnt))
		res['DA32S8E'].append(r['DA32S8E'][3] / (1000.0 * cnt))
		res['BA32S16'].append(r['BA32S16'][3] / (1000.0 * cnt))

	# plot a figure of the linear-gap cost
	pylab.subplot(2, 2, 3)
	l0, = pylab.plot(res['tick'], res['DL32S8'],  color = 'black', linewidth = width, linestyle = '-',  label = 'DL32S8')
	l1, = pylab.plot(res['tick'], res['DL32S8E'], color = 'black', linewidth = width, linestyle = '--', label = 'DL32S8E')
	l2, = pylab.plot(res['tick'], res['BL32S16'], color = 'black', linewidth = width, linestyle = '-',  label = 'BL32S16')
	l3, = pylab.plot(res['tick'], res['NL32'],    color = 'black', linewidth = width, linestyle = '-',  label = 'NL32')
	l2.set_dashes([6, 3, 2, 3, 2, 3])
	l3.set_dashes([12, 3, 2, 3])
	pylab.xlim(-100, 10300)
	pylab.xticks(fontsize = size)
	pylab.xlabel('Query length (bp)', fontsize = size)
	pylab.ylim(0, 400)
	pylab.yticks(fontsize = size)
	pylab.ylabel('Total elapsed time (us)', fontsize = size)
	pylab.title('(c) 32-cell wide linear-gap cost', fontsize = size)
	pylab.legend(loc = 'best', fontsize = 12)

	# plot a figure of the affine-gap cost
	pylab.subplot(2, 2, 4)
	l0, = pylab.plot(res['tick'], res['DA32S8'],  color = 'black', linewidth = width, linestyle = '-',  label = 'DA32S8')
	l1, = pylab.plot(res['tick'], res['DA32S8E'], color = 'black', linewidth = width, linestyle = '--', label = 'DA32S8E')
	l2, = pylab.plot(res['tick'], res['BA32S16'], color = 'black', linewidth = width, linestyle = '-',  label = 'BA32S16')
	l3, = pylab.plot(res['tick'], res['NA32'],    color = 'black', linewidth = width, linestyle = '-',  label = 'NA32')
	l2.set_dashes([6, 3, 2, 3, 2, 3])
	l3.set_dashes([12, 3, 2, 3])
	pylab.xlim(-100, 10300)
	pylab.xticks(fontsize = size)
	pylab.xlabel('Query length (bp)', fontsize = size)
	pylab.ylim(0, 400)
	pylab.yticks(fontsize = size)
	pylab.title('(d) 32-cell wide affine-gap cost', fontsize = size)
	pylab.legend(loc = 'best', fontsize = 12)
	
	# save figure
	pylab.savefig("benchmark_len.eps", dpi = 72)
#	pylab.show()


def plot_table(result):
	#res = benchmark(1000, cnt)
	if 10000 in result:
		res = result[10000]
	else:
		res = result[10]
	if 'cnt' in result:
		cnt = result['cnt']
	else:
		cnt = 0

	import csv

	def get_result(name, base):
		def normalize(a, b, c, d):
			return (a/(1000.0 * cnt), b/(1000.0 * cnt), c/(1000.0 * cnt), d/(1000.0 * cnt))
		
		if name in res:
			return normalize(res[name][0]+res[name][1], res[name][3], res[base][0]+res[base][1], res[base][3])
		else:
			return normalize(0, 0, res[base][0] + res[base][1], res[base][3])

	def fmt(a):
		if a < 1.0:
			return '{0:.3f}'.format(a)
		else:
			return '{0:.1f}'.format(a)

	with open('benchmark_table.csv', 'w') as f:
		w = csv.writer(f)
		w.writerow(['',       '',         '',          '', 'Linear-gap cost',   '',           '',                  '',           '', 'Affine-gap cost',   '',           '',                  ''])
		w.writerow(['',       '',         '',          '', 'Score (fill + search)', '',       'Trace (fill + search + trace)','','', 'Score (fill + search)', '',       'Trace (fill + search + trace)', ''])
		w.writerow(['',       'Bitwidth', 'Bandwidth', '', 'Time (us)',         'Normalized', 'Time (us)',         'Normalized', '', 'Time (us)',         'Normalized', 'Time (us)',         'Normalized'])
		# Kimura's bit-parallel
		(lsc, ltr, lbsc, lbtr) = get_result('B64', 'NL64')
		w.writerow(['Kimura', '1',        '64',        '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', '',                  '',           '',                  ''])

		# Non-diff 16bit 32cell
		(lsc, ltr, lbsc, lbtr) = get_result('BL32S16', 'NL32')
		(asc, atr, absc, abtr) = get_result('BA32S16', 'NA32')
		w.writerow(['Non-diff', '16',     '32',        '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])
		# Non-diff 16bit 32cell (AVX2)
		(lsc, ltr, lbsc, lbtr) = get_result('BL32A16', 'NL32')
		(asc, atr, absc, abtr) = get_result('BA32A16', 'NA32')
		w.writerow(['Non-diff', '16',     '32',        '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])

		# diff 8bit 32cell
		(lsc, ltr, lbsc, lbtr) = get_result('DL32S8',  'NL32')
		(asc, atr, absc, abtr) = get_result('DA32S8',  'NA32')
		w.writerow(['Diff',   '8',        '32',        '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])
		# diff 8bit 32cell (exact)
		(lsc, ltr, lbsc, lbtr) = get_result('DL32S8E', 'NL32')
		(asc, atr, absc, abtr) = get_result('DA32S8E', 'NA32')
		w.writerow(['Diff (Exact)','8',   '32',        '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])


		# diff 8bit 32cell (AVX2)
		(lsc, ltr, lbsc, lbtr) = get_result('DL32A8',  'NL32')
		(asc, atr, absc, abtr) = get_result('DA32A8',  'NA32')
		w.writerow(['Diff',   '8',        '32 (AVX2)', '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])
		# diff 8bit 32cell (AVX2) (exact)
		(lsc, ltr, lbsc, lbtr) = get_result('DL32A8E', 'NL32')
		(asc, atr, absc, abtr) = get_result('DA32A8E', 'NA32')
		w.writerow(['Diff (Exact)','8',   '32 (AVX2)', '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])

		'''
		# diff 16bit 32cell
		(lsc, ltr, lbsc, lbtr) = get_result('DL32S16', 'NL32')
		(asc, atr, absc, abtr) = get_result('DA32S16', 'NA32')
		w.writerow(['Diff',   '16',       '32',        '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])
		# diff 16bit 32cell (AVX2)
		(lsc, ltr, lbsc, lbtr) = get_result('DL32A16', 'NL32')
		(asc, atr, absc, abtr) = get_result('DA32A16', 'NA32')
		w.writerow(['Diff',   '16',       '32 (AVX2)', '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])

		# diff 16bit 32cell (exact)
		(lsc, ltr, lbsc, lbtr) = get_result('DL32S16E','NL32')
		(asc, atr, absc, abtr) = get_result('DA32S16E','NA32')
		w.writerow(['Diff',   '16',       '32',        '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])
		# diff 16bit 32cell (AVX2) (exact)
		(lsc, ltr, lbsc, lbtr) = get_result('DL32A16E','NL32')
		(asc, atr, absc, abtr) = get_result('DA32A16E','NA32')
		w.writerow(['Diff',   '16',       '32 (AVX2)', '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])
		'''

		# naive 32
		(lsc, ltr, lbsc, lbtr) = get_result('NL32', 'NL32')
		(asc, atr, absc, abtr) = get_result('NA32', 'NA32')
		w.writerow(['Naive',  '32',       '32',        '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])
		# naive 64
		(lsc, ltr, lbsc, lbtr) = get_result('NL64', 'NL64')
		(asc, atr, absc, abtr) = get_result('NA64', 'NA64')
		w.writerow(['Naive',  '32',       '64',        '', fmt(lsc),            fmt(lsc/lbsc),fmt(ltr),            fmt(ltr/lbtr),'', fmt(asc),            fmt(asc/absc),fmt(atr),            fmt(atr/abtr)])

def plot_all(results):
	plot_len(results)
	plot_table(results)

# dump and load results
def dump(result, filename):
	with open(filename, 'w') as f:
		f.write(result.__str__())

def load(filename):
	with open(filename, 'r') as f:
		return(eval(f.read()))

# accuracy benchmark
def bench_accuracy():

	import subprocess
	import csv

	cnt = 100000
	len = 10000 		# default values

	res = [[subprocess.check_output(['../build/bench/bench_accuracy',
				'-c', str(cnt), '-l', str(len),
				'-k', str(k), '-m', str(m/100.0)])
				.decode('utf-8').split('\n')[0].split(', ')
				for m in range(100, 69, -5)]
				for k in range(1, 6)]

	with open('benchmark_accuracy.csv', 'w') as f:
		w = csv.writer(f)
		w.writerow(['Identity',  '1.00',  '0.95',  '0.90',  '0.85',  '0.80',  '0.75',  '0.70'])
		for k, r in zip(range(1, 6), res):
			w.writerow(['k = ' + str(k)] + [e[4] for e in r])
			w.writerow([''] + [int(e[5])/1000000 for e in r])
			w.writerow([''] + ['{0:.1f}'.format(100*int(e[5])/int(e[6])) for e in r])


if __name__ == '__main__':

	import sys

	if len(sys.argv) == 1:
		print(msg)
		exit(1)
	elif sys.argv[1] == 'bench':
		if len(sys.argv) < 4:
			print(msg)
			exit(1)

		if sys.argv[2] == 'fine':
			cnt = 1000
		elif sys.argv[2] == 'default':
			cnt = 100
		elif sys.argv[2] == 'rough':
			cnt = 10
		else:
			print('Invalid option: {0}. Use default iteration count: 1000'.format(sys.argv[1]))
			cnt = 1000

		dump(benchmark_all(cnt), sys.argv[3])


	elif sys.argv[1] == 'plot':
		if len(sys.argv) < 3:
			print(msg)
			exit(1)

		plot_all(load(sys.argv[2]))

	elif sys.argv[1] == 'accuracy':
		bench_accuracy();

	else:
		print(msg)
		exit(1)

