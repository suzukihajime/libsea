
///
// @file bemch_seqan.cc
//
// @brief
// a benchmark program, which compares speed of banded local
// alignment functions in the libsea and the SeqAn library.
//
// @detail
// Compile:
//   $ g++ -Wall -o bench_seqan bench_seqan.cc -O3 -lsea
// Note: please add '-O3' optimization flag for a fair comparison
//       since the SeqAn library is header-based and compiled on
//       the fly.
//
// Notice:
//   libsea:
// If you did not installed the libsea into your computer yet,
// you must tell the compiler where the header file and the library
// is. Please add '-L../build -I../include' to the flags.
//
//   SeqAn:
// The SeqAn library must also be installed on your computer.
// This program requires SeqAn basic headers. If they are not
// available on your machine, please download SeqAn source code
// from the github repository (https://github.com/seqan/seqan),
// copy the directory core/include/seqan/ to the directory at this
// file is located, and modify '#include <seqan/align.h>' to
// '#include "seqan/align.h"'. You can compile this program with
// passing '-I.' flag to the compiler.
//
#include <string>           // std::string
#include <seqan/align.h>    // base header of SeqAn::align
#include <sea.h>            // libsea header file
#include <sys/time.h>       // for gettimeofday

using namespace seqan;
using namespace sea;

char random_base(void);
std::string generate_random_sequence(int len);
std::string generate_mutated_sequence(std::string seq, double x, double d, int bw);

int main(void)
{
    int const len[6] = {100, 300, 1000, 3000, 10000, 30000};
    int const cnt = 1000;
    struct timeval tvs, tve;

    for(int j = 0; j < 6; j++) {
        std::cout << len[j] << ", ";

        std::string a = generate_random_sequence(len[j]);
        std::string b = generate_mutated_sequence(a, 0.1, 0.1, 10);

        // SeqAn localAlignment
        typedef String<Dna> TSequence;
        TSequence seq1(a);
        TSequence seq2(b);

        Score<int> scoringScheme(2, -3, -5, -1);
        gettimeofday(&tvs, NULL);
        for(int i = 0; i < cnt; i++) {
            Align<TSequence, ArrayGaps> align;
            resize(rows(align), 2);
            assignSource(row(align, 0), seq1);
            assignSource(row(align, 1), seq2);

            // the 31-th lower diagonal equals the 15-th upper cell of the libsea banded
            // alignment and the 32-th upper diagonal equals the 16-th lower cell.
            // See a diagram in naive_linear_banded.c for the details of the coordinates
            // in libsea.
            // See $INCLUDE/seqan/align/dp_alignment_impl.h for the details of the
            // implementation of the banded alignment algorithm in the SeqAn library.
            volatile int score = localAlignment(align, scoringScheme, -31, 32);
        }
        gettimeofday(&tve, NULL);

        long us = (tve.tv_sec - tvs.tv_sec) * 1000000 + (tve.tv_usec - tvs.tv_usec);
        std::cout << us << ", ";

        // libsea localAlignment
        AlignmentContext ctx(
            SEA_SW | SEA_AFFINE_GAP_COST |
            SEA_BANDED_DP | SEA_BANDWIDTH_32 |
            SEA_SEQ_ASCII | SEA_ALN_ASCII,
            50000,
            2, -3, -5, -1,  // match, mismatch, gap open, and gap extension cost
            0);
        gettimeofday(&tvs, NULL);
        for(int i = 0; i < cnt; i++) {
            AlignmentResult aln = ctx.align(
                a, 0, a.length(),
                b, 0, b.length());
            volatile int score = aln.score();
        }
        gettimeofday(&tve, NULL);

        us = (tve.tv_sec - tvs.tv_sec) * 1000000 + (tve.tv_usec - tvs.tv_usec);
        std::cout << us << std::endl;
    }
    return 0;
}

// random sequence generation functoins. see benchmark.c for details.
char random_base(void)
{
    switch(rand() % 4) {
        case 0: return 'A';
        case 1: return 'C';
        case 2: return 'G';
        case 3: return 'T';
        default: return 'A';
    }
}

std::string generate_random_sequence(int len)
{
    std::string seq;
    seq.reserve(len);
    seq.resize(len);
    for(int i = 0; i < len; i++) {
        seq[i] = random_base();
    }
    return seq;
}

std::string generate_mutated_sequence(std::string seq, double x, double d, int bw)
{
    int const len = seq.length();
    std::string mutated_seq;

    mutated_seq.reserve(len);
    mutated_seq.resize(len);
    for(int i = 0, j = 0, wave = 0; i < len; i++) {
        if(((double)rand() / (double)RAND_MAX) < x) {
            mutated_seq[i] = random_base(); j++;    /** mismatch */
        } else if(((double)rand() / (double)RAND_MAX) < d) {
            if(rand() & 0x01 && wave > -bw+1) {
                mutated_seq[i] = (j < len) ? seq[j++] : random_base();
                j++; wave--;                        /** deletion */
            } else if(wave < bw-2) {
                mutated_seq[i] = random_base();
                wave++;                             /** insertion */
            } else {
                mutated_seq[i] = (j < len) ? seq[j++] : random_base();
            }
        } else {
            mutated_seq[i] = (j < len) ? seq[j++] : random_base();
        }
    }
    return mutated_seq;
}
