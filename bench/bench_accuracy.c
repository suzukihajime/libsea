
/**
 * @file bench_accuracy.c
 *
 * @brief evaluate accuracy of the inexact maximum search algorithm.
 *
 * @detail
 * This program generates pairs of sequences, with 10-kbp similar
 * region and 10-kbp random region. The sequences are aligned with
 * the naive implementation and the difference-recurrence with
 * inexact search implementation. The results are compared, and the
 * identity ratio is reported.
 *
 * Options:
 *     -h       : show this message.
 *     -l INT   : specify the length of the similar and the random sections.
 *     -m FLOAT : specify match ratio of the similar section.
 *     -c INT   : specify the number of trial.
 *     -s INT   : specify the seed of the RNG.
 *     -k INT   : specify the search length stretch ratio.
 *     -d       : print the results of the individual trials.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "../include/sea.h"
#include "../util/bench.h"
#include "../build/scalar/naive_linear_banded_decl.h"
#include "../build/simd/diff_linear_banded_decl.h"
#include "../build/simd/diag_linear_banded_decl.h"

/** 
 * @fn print_help
 */
void print_help(void)
{
	char const *msg =
		"Datail:\n"
		"This program generates pairs of sequences, with 10-kbp similar\n"
		"region and 10-kbp random region. The sequences are aligned with\n"
		"the naive implementation and the difference-recurrence with\n"
		"inexact search implementation. The results are compared, and the\n"
		"identity ratio is reported.\n"
		"\n"
		"Options:\n"
		"    -h       : show this message.\n"
		"    -l INT   : specify the length of the similar and the random sections.\n"
		"    -m FLOAT : specify match ratio of the similar section.\n"
		"    -c INT   : specify the number of trial.\n"
		"    -s INT   : specify the seed of the RNG.\n"
		"    -k INT   : specify the search length stretch ratio.\n"
		"    -d       : print the results of the individual trials.\n";

	fprintf(stderr,
		"\n"
		"  -- diff accuracy evaluation program --\n"
		"\n"
		"Usage: ./bench_accuracy -l 10000 -m 0.8\n"
		"\n"
		"%s\n",
		msg);
	return;
}

/**
 * @fn random_base
 *
 * @brief return a character randomly from {'A', 'C', 'G', 'T'}.
 */
char random_base(void)
{
	switch(rand() % 4) {
		case 0: return 'A';
		case 1: return 'C';
		case 2: return 'G';
		case 3: return 'T';
		default: return 'A';
	}
}

/**
 * @fn generate_random_sequence
 *
 * @brief malloc memory of size <len>, then fill it randomely with {'A', 'C', 'G', 'T'}.
 *
 * @param[in] len : the length of sequence to generate.
 * @return a pointer to the generated sequence.
 */
char *generate_random_sequence(int len)
{
	int i;
	char *seq;		/** a pointer to sequence */
	seq = (char *)malloc(sizeof(char) * (len + 1));
	if(seq == NULL) { return NULL; }
	for(i = 0; i < len; i++) {
		seq[i] = random_base();
	}
	seq[len] = '\0';
	return seq;
}

/**
 * @fn generate_mutated_sequence
 *
 * @brief take a DNA sequence represented in ASCII, mutate the sequence at the given probability.
 *
 * @param[in] seq : a pointer to the string of {'A', 'C', 'G', 'T'}.
 * @param[in] x : mismatch rate
 * @param[in] d : insertion / deletion rate
 * @param[in] bw : bandwidth (the alignment path of the input sequence and the result does not go out of the band)
 *
 * @return a pointer to mutated sequence.
 */
char *generate_mutated_sequence(char *seq, double x, double d, int bw)
{
	int i, j, wave = 0;			/** wave is q-coordinate of the alignment path */
	int len;
	char *mutated_seq;

	if(seq == NULL) { return NULL; }
	len = strlen(seq);
	mutated_seq = (char *)malloc(sizeof(char) * (len + 1));
	if(mutated_seq == NULL) { return NULL; }
	for(i = 0, j = 0; i < len; i++) {
		if(((double)rand() / (double)RAND_MAX) < x) {
			mutated_seq[i] = random_base();	j++;	/** mismatch */
		} else if(((double)rand() / (double)RAND_MAX) < d) {
			if(rand() & 0x01 && wave > -bw+1) {
				mutated_seq[i] = (j < len) ? seq[j++] : random_base();
				j++; wave--;						/** deletion */
			} else if(wave < bw-2) {
				mutated_seq[i] = random_base();
				wave++;								/** insertion */
			} else {
				mutated_seq[i] = (j < len) ? seq[j++] : random_base();
			}
		} else {
			mutated_seq[i] = (j < len) ? seq[j++] : random_base();
		}
	}
	mutated_seq[len] = '\0';
	return mutated_seq;
}

/**
 * @fn main
 */
int main(int argc, char *argv[])
{
	long i, suc = 0;
	double ratio = 0.8;		/** default match ratio is 80% */
	long len = 10000; 		/** default length is 10kb */
	long cnt = 100000;		/** default #traial is 100000 */
	long seed = 0;			/** seed of the RNG */
	int k = 1;				/** search length stretch ratio */
	int detail = 0;			/** print detailed results */
	char *a, *b;
	char **pa, **pb;
	struct sea_context *naive, *diff;
	struct sea_result *rn, *rd;
	struct result res;		/** benchmark result */

	seed = time(NULL);

	/**
	 * parse options.
	 */
	while((i = getopt(argc, argv, "l:m:c:s:k:d")) != -1) {
		switch(i) {
			case 'h':
			default:
				print_help(); exit(1);
			case 'l':
				len = atoi((char *)optarg); break;
			case 'm':
				ratio = atof((char *)optarg); break;
			case 'c':
				cnt = atoi((char *)optarg); break;
			case 's':
				seed = atoi((char *)optarg); break;
			case 'k':
				k = atoi((char *)optarg); break;
			case 'd':
				detail = 1; break;
		}
	}

	/**
	 * initialize contexts.
	 */
	naive = sea_init_fp(
		SEA_SEA |
		SEA_BANDED_DP |
		SEA_SEQ_ASCII |
		SEA_ALN_ASCII |
		SEA_BANDWIDTH_32,
		diag_linear_banded_ascii_ascii_sea_sse_16_32,
		diag_linear_banded_matsize_ascii_ascii_sea_sse_16_32,
		2, -3, -4, -4, 0);

	diff = sea_init_fp(
		SEA_SEA |
		SEA_BANDED_DP |
		SEA_SEQ_ASCII |
		SEA_ALN_ASCII |
		SEA_HEURISTIC |	/** use SEA_EXACT if you want to use exact algorithm */
		SEA_BANDWIDTH_32,
		diff_linear_banded_ascii_ascii_sea_sse_8_32,
		diff_linear_banded_matsize_ascii_ascii_sea_sse_8_32,
		2, -3, -4, -4, 0);
	diff->param.k = k;

	/**
	 * accuracy bench
	 */
	for(i = 0; i < cnt; i++) {
		a = generate_random_sequence(len);
		b = generate_mutated_sequence(a,
			(1.0 - ratio) / 2.0, (1.0 - ratio) / 2.0, 31);
		strncpy(b+len*4/5, a, len/5);

		rn = sea_align(naive, a, 0, len*2, b, 0, len*2);
		rd = sea_align(diff,  a, 0, len*2, b, 0, len*2);

		if(detail == 1) {
			printf(
				"%d, %d, %d, "
				"%d, %d, %d, "
				"%d\n",
				rn->len, rn->alen, rn->blen,
				rd->len, rd->alen, rd->blen,
				strcmp(rn->aln, rd->aln));
		}
		if(strcmp(rn->aln, rd->aln) == 0) {
			suc++;
		}
		sea_aln_free(rn);
		sea_aln_free(rd);
		free(a);
		free(b);
	}

	/**
	 * time bench
	 */

	a = generate_random_sequence(len);
	b = generate_mutated_sequence(a,
		(1.0 - ratio) / 2.0, (1.0 - ratio) / 2.0, 31);
	strncpy(b+len*4/5, a, len/5);
	pa = (char **)malloc(sizeof(char *) * 1001);
	pb = (char **)malloc(sizeof(char *) * 1001);
	for(i = 0; i < 1000; i++) {
		pa[i] = a; pb[i] = b;
	}
	pa[1000] = pb[1000] = NULL;
	res = benchmark(diff, 1, pa, pb);
	free(a); free(b);
	free(pa); free(pb);

	/**
	 * print summary
	 */
//	printf("%ld, %ld, %ld, %ld\n", res.fill, res.search, res.trace, res.total);
	if(detail == 0) {
		printf("%f, %ld, %ld, %ld, %f, %ld, %ld\n", ratio, len, cnt, suc, (double)suc / (double)cnt, res.search, res.total);
	}

	sea_clean(naive);
	sea_clean(diff);
	return 0;
}

/**
 * end of bench_accuracy.c
 */
