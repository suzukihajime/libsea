
/**
 * @file benchmark.c
 *
 * @brief a benchmarking program of the seed-and-extend algorithms.
 *
 * @detail
 * This program takes benchmarks of the algorithms in this library. The variants used
 * in the benchmarks are fixed to alg=SEA, seq=ascii, and aln=ascii. The program
 * reads two input fasta files and issue queries to each algorithm from the beginning
 * of the set of sequences, or generates the specified number of random and modified
 * sequences and issue queries. Each elapsed time of the fill-in step, the score-search
 * step, and the traceback step is taken using macros in bench.h, and printed to
 * stderr.
 *
 * Options:
 *   Help
 *     -h : show this message.
 *
 *   Inputs and outputs
 *     -q <filename> : specify an input fasta file name. the content is used as sequence a.
 *     -t <filename> : specify an input fasta file name. the content is used as sequence b.
 *
 *     -o <filename> : specify an output csv file name. if it is not specified, the results
 *          are printed to stderr.
 *
 *   Output destination
 *     -s : print results to stdout.
 *     -e : print results to stderr.
 *
 *   Output format
 *     -b <string> : give 'legacy' if you want to get the results in the legacy (bench.c in
 *          alpha branch) format.
 *
 *   Query sequence generation
 *     -l : the length of queries. the default is 1000 (1kbp).
 *     -n : the number of sequence pairs generated.
 *     -x, -d : the mismatch rate, and the indel rate used to mutate sequences.
 *
 *   Benchmarking
 *     -c <number> : specify the number of iteration.
 *     -a <string> : specify algorithm. available options are
 *          all : take benchmarks of all the algorithms.
 *          <list> : a list of algorithms, such as 'NL32,NA32,BL32S16,BA32S16'
 *     -p <number> : the number of threads which run in parallel.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>			/** for fstat */
#include <unistd.h>				/** for getopt */
#include "../include/sea.h"
#include "../util/util.h"
#include "../util/bench.h"
#include "../build/config.h"	/** HAVE_SSE4, HAVE_AVX2 */

/**
 * include auto-generated headers for function declarations.
 */

/**
 * scalar implementations
 */
#include "../scalar/scalar.h"
#include "../build/scalar/naive_linear_full_decl.h"
#include "../build/scalar/naive_affine_full_decl.h"
#include "../build/scalar/naive_linear_banded_decl.h"
#include "../build/scalar/naive_affine_banded_decl.h"

/**
 * bit-parallel algorithms
 */
#include "../bit/bit.h"
#include "../build/bit/edit_banded_decl.h"

/**
 * simd algorithms
 */
#include "../simd/simd.h"
#include "../build/simd/diag_linear_banded_decl.h"
#include "../build/simd/diag_affine_banded_decl.h"
#include "../build/simd/diff_linear_banded_decl.h"
#include "../build/simd/diff_affine_banded_decl.h"

#include "../build/simd/diag_linear_dynamic_banded_decl.h"
#include "../build/simd/diag_affine_dynamic_banded_decl.h"
#include "../build/simd/diff_linear_dynamic_banded_decl.h"
#include "../build/simd/diff_affine_dynamic_banded_decl.h"

/**
 * @struct alg
 * @brief a set of a function pointer and corresponding information.
 */
struct alg {
	char const *name;		/** abbreviation of the algorithm */
	sea_int_t flags;		/** flags passed to the init function */
	sea_int_t bandwidth;	/** bandwidth */
	char const scmat[4];	/** score array */
	sea_int_t len;			/** maximum length, -1 if no limit */
	sea_int_t (*sea_sea)(
		struct sea_result *aln,
		struct sea_params param,
		void *mat);
	sea_int_t (*sea_matsize)(
		sea_int_t alen,
		sea_int_t blen,
		sea_int_t bandwidth);
};

/**
 * function table
 */
struct alg algs[] = {
	{"NL16",		SEA_HEURISTIC,	16, {2, -3, -5, -5},    -1, naive_linear_banded_ascii_ascii_sea, naive_linear_banded_matsize_ascii_ascii_sea},
	{"NL32",		SEA_HEURISTIC,	32, {2, -3, -5, -5},    -1, naive_linear_banded_ascii_ascii_sea, naive_linear_banded_matsize_ascii_ascii_sea},
	{"NL64",		SEA_HEURISTIC,	64, {2, -3, -5, -5},    -1, naive_linear_banded_ascii_ascii_sea, naive_linear_banded_matsize_ascii_ascii_sea},
	{"NA16",		SEA_HEURISTIC,	16, {2, -3, -5, -1},    -1, naive_affine_banded_ascii_ascii_sea, naive_affine_banded_matsize_ascii_ascii_sea},
	{"NA32",		SEA_HEURISTIC,	32, {2, -3, -5, -1},    -1, naive_affine_banded_ascii_ascii_sea, naive_affine_banded_matsize_ascii_ascii_sea},
	{"NA64",		SEA_HEURISTIC,	64, {2, -3, -5, -1},    -1, naive_affine_banded_ascii_ascii_sea, naive_affine_banded_matsize_ascii_ascii_sea},

	/** Kimura's bit-parallel edit-distance algorithm */
	{"B64",			SEA_HEURISTIC,	64, {2,  1,  0,  0},    -1, edit_banded_ascii_ascii_sea, edit_banded_matsize_ascii_ascii_sea},

#if HAVE_SSE4
	/** SSE4 diagonal linear-gap cost algorithms */
	{"BL16S8",		SEA_HEURISTIC,	16, {2, -3, -5, -5},   127, diag_linear_banded_ascii_ascii_sea_sse_8_16, diag_linear_banded_matsize_ascii_ascii_sea_sse_8_16},
	{"BL32S8",		SEA_HEURISTIC,	32, {2, -3, -5, -5},   127, diag_linear_banded_ascii_ascii_sea_sse_8_32, diag_linear_banded_matsize_ascii_ascii_sea_sse_8_32},
	{"BL16S16",		SEA_HEURISTIC,	16, {2, -3, -5, -5}, 32767, diag_linear_banded_ascii_ascii_sea_sse_16_16, diag_linear_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"BL32S16",		SEA_HEURISTIC,	32, {2, -3, -5, -5}, 32767, diag_linear_banded_ascii_ascii_sea_sse_16_32, diag_linear_banded_matsize_ascii_ascii_sea_sse_16_32},
	/** SSE4 diagonal affine-gap cost algorithms */
	{"BA16S8",		SEA_HEURISTIC,	16, {2, -3, -5, -1},   127, diag_affine_banded_ascii_ascii_sea_sse_8_16, diag_affine_banded_matsize_ascii_ascii_sea_sse_8_16},
	{"BA32S8",		SEA_HEURISTIC,	32, {2, -3, -5, -1},   127, diag_affine_banded_ascii_ascii_sea_sse_8_32, diag_affine_banded_matsize_ascii_ascii_sea_sse_8_32},
	{"BA16S16",		SEA_HEURISTIC,	16, {2, -3, -5, -1}, 32767, diag_affine_banded_ascii_ascii_sea_sse_16_16, diag_affine_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"BA32S16",		SEA_HEURISTIC,	32, {2, -3, -5, -1}, 32767, diag_affine_banded_ascii_ascii_sea_sse_16_32, diag_affine_banded_matsize_ascii_ascii_sea_sse_16_32},
	/** SSE4 difference linear-gap cost algorithms */
	{"DL16S8",		SEA_HEURISTIC,	16, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_sse_8_16, diff_linear_banded_matsize_ascii_ascii_sea_sse_8_16},
	{"DL32S8",		SEA_HEURISTIC,	32, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_sse_8_32, diff_linear_banded_matsize_ascii_ascii_sea_sse_8_32},
	{"DL16S16",		SEA_HEURISTIC,	16, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_sse_16_16, diff_linear_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"DL32S16",		SEA_HEURISTIC,	32, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_sse_16_32, diff_linear_banded_matsize_ascii_ascii_sea_sse_16_32},
	/** SSE4 difference affine-gap cost algorithms */
	{"DA16S8",		SEA_HEURISTIC,	16, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_sse_8_16, diff_affine_banded_matsize_ascii_ascii_sea_sse_8_16},
	{"DA32S8",		SEA_HEURISTIC,	32, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_sse_8_32, diff_affine_banded_matsize_ascii_ascii_sea_sse_8_32},
	{"DA16S16",		SEA_HEURISTIC,	16, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_sse_16_16, diff_affine_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"DA32S16",		SEA_HEURISTIC,	32, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_sse_16_32, diff_affine_banded_matsize_ascii_ascii_sea_sse_16_32},
	/** SSE4 difference linear-gap cost algorithms (with exact search) */
	{"DL16S8E",		SEA_EXACT,		16, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_sse_8_16, diff_linear_banded_matsize_ascii_ascii_sea_sse_8_16},
	{"DL32S8E",		SEA_EXACT,		32, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_sse_8_32, diff_linear_banded_matsize_ascii_ascii_sea_sse_8_32},
	{"DL16S16E",	SEA_EXACT,		16, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_sse_16_16, diff_linear_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"DL32S16E",	SEA_EXACT,		32, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_sse_16_32, diff_linear_banded_matsize_ascii_ascii_sea_sse_16_32},
	/** SSE4 difference affine-gap cost algorithms (with exact search) */
	{"DA16S8E",		SEA_EXACT,		16, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_sse_8_16, diff_affine_banded_matsize_ascii_ascii_sea_sse_8_16},
	{"DA32S8E",		SEA_EXACT,		32, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_sse_8_32, diff_affine_banded_matsize_ascii_ascii_sea_sse_8_32},
	{"DA16S16E",	SEA_EXACT,		16, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_sse_16_16, diff_affine_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"DA32S16E",	SEA_EXACT,		32, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_sse_16_32, diff_affine_banded_matsize_ascii_ascii_sea_sse_16_32},


	/** SSE4 diagonal linear-gap cost dynamic banded algorithms */
	{"BLD16S16",	SEA_HEURISTIC,	16, {2, -3, -5, -5}, 32767, diag_linear_dynamic_banded_ascii_ascii_sea_sse_16_16, diag_linear_dynamic_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"BLD32S16",	SEA_HEURISTIC,	32, {2, -3, -5, -5}, 32767, diag_linear_dynamic_banded_ascii_ascii_sea_sse_16_32, diag_linear_dynamic_banded_matsize_ascii_ascii_sea_sse_16_32},
	/** SSE4 diagonal affine-gap cost dynamic banded algorithms */
	{"BAD16S16",	SEA_HEURISTIC,	16, {2, -3, -5, -1}, 32767, diag_affine_dynamic_banded_ascii_ascii_sea_sse_16_16, diag_affine_dynamic_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"BAD32S16",	SEA_HEURISTIC,	32, {2, -3, -5, -1}, 32767, diag_affine_dynamic_banded_ascii_ascii_sea_sse_16_32, diag_affine_dynamic_banded_matsize_ascii_ascii_sea_sse_16_32},
	/** SSE4 difference linear-gap cost dynamic banded algorithms */
	{"DLD16S8",		SEA_HEURISTIC,	16, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_sse_8_16, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_sse_8_16},
	{"DLD32S8",		SEA_HEURISTIC,	32, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_sse_8_32, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_sse_8_32},
	{"DLD16S16",	SEA_HEURISTIC,	16, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_sse_16_16, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"DLD32S16",	SEA_HEURISTIC,	32, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_sse_16_32, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_sse_16_32},
	/** SSE4 difference affine-gap cost dynamic banded algorithms */
	{"DAD16S8",		SEA_HEURISTIC,	16, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_sse_8_16, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_sse_8_16},
	{"DAD32S8",		SEA_HEURISTIC,	32, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_sse_8_32, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_sse_8_32},
	{"DAD16S16",	SEA_HEURISTIC,	16, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_sse_16_16, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"DAD32S16",	SEA_HEURISTIC,	32, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_sse_16_32, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_sse_16_32},
	/** SSE4 difference linear-gap cost dynamic banded algorithms (with exact search) */
	{"DLD16S8E",	SEA_EXACT,		16, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_sse_8_16, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_sse_8_16},
	{"DLD32S8E",	SEA_EXACT,		32, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_sse_8_32, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_sse_8_32},
	{"DLD16S16E",	SEA_EXACT,		16, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_sse_16_16, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"DLD32S16E",	SEA_EXACT,		32, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_sse_16_32, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_sse_16_32},
	/** SSE4 difference affine-gap cost dynamic banded algorithms (with exact search) */
	{"DAD16S8E",	SEA_EXACT,		16, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_sse_8_16, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_sse_8_16},
	{"DAD32S8E",	SEA_EXACT,		32, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_sse_8_32, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_sse_8_32},
	{"DAD16S16E",	SEA_EXACT,		16, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_sse_16_16, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_sse_16_16},
	{"DAD32S16E",	SEA_EXACT,		32, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_sse_16_32, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_sse_16_32},
#endif /** #if HAVE_SSE4 */

#if HAVE_AVX2
	/** AVX2 diagonal linear-gap cost algorithms */
	{"BL32A8",		SEA_HEURISTIC,	32, {2, -3, -5, -5},   127, diag_linear_banded_ascii_ascii_sea_avx_8_32, diag_linear_banded_matsize_ascii_ascii_sea_avx_8_32},
	{"BL16A16",		SEA_HEURISTIC,	16, {2, -3, -5, -5}, 32767, diag_linear_banded_ascii_ascii_sea_avx_16_16, diag_linear_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"BL32A16",		SEA_HEURISTIC,	32, {2, -3, -5, -5}, 32767, diag_linear_banded_ascii_ascii_sea_avx_16_32, diag_linear_banded_matsize_ascii_ascii_sea_avx_16_32},
	/** AVX2 diagonal affine-gap cost algorithms */
	{"BA32A8",		SEA_HEURISTIC,	32, {2, -3, -5, -1},   127, diag_affine_banded_ascii_ascii_sea_avx_8_32, diag_affine_banded_matsize_ascii_ascii_sea_avx_8_32},
	{"BA16A16",		SEA_HEURISTIC,	16, {2, -3, -5, -1}, 32767, diag_affine_banded_ascii_ascii_sea_avx_16_16, diag_affine_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"BA32A16",		SEA_HEURISTIC,	32, {2, -3, -5, -1}, 32767, diag_affine_banded_ascii_ascii_sea_avx_16_32, diag_affine_banded_matsize_ascii_ascii_sea_avx_16_32},
	/** AVX2 difference linear-gap cost algorithms */
	{"DL32A8",		SEA_HEURISTIC,	32, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_avx_8_32, diff_linear_banded_matsize_ascii_ascii_sea_avx_8_32},
	{"DL16A16",		SEA_HEURISTIC,	16, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_avx_16_16, diff_linear_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"DL32A16",		SEA_HEURISTIC,	32, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_avx_16_32, diff_linear_banded_matsize_ascii_ascii_sea_avx_16_32},
	/** AVX2 difference affine-gap cost algorithms */
	{"DA32A8",		SEA_HEURISTIC,	32, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_avx_8_32, diff_affine_banded_matsize_ascii_ascii_sea_avx_8_32},
	{"DA16A16",		SEA_HEURISTIC,	16, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_avx_16_16, diff_affine_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"DA32A16",		SEA_HEURISTIC,	32, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_avx_16_32, diff_affine_banded_matsize_ascii_ascii_sea_avx_16_32},
	/** AVX2 difference linear-gap cost algorithms (exact) */
	{"DL32A8E",		SEA_EXACT,		32, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_avx_8_32, diff_linear_banded_matsize_ascii_ascii_sea_avx_8_32},
	{"DL16A16E",	SEA_EXACT,		16, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_avx_16_16, diff_linear_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"DL32A16E",	SEA_EXACT,		32, {2, -3, -5, -5},    -1, diff_linear_banded_ascii_ascii_sea_avx_16_32, diff_linear_banded_matsize_ascii_ascii_sea_avx_16_32},
	/** AVX2 difference affine-gap cost algorithms (exact) */
	{"DA32A8E",		SEA_EXACT,		32, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_avx_8_32, diff_affine_banded_matsize_ascii_ascii_sea_avx_8_32},
	{"DA16A16E",	SEA_EXACT,		16, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_avx_16_16, diff_affine_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"DA32A16E",	SEA_EXACT,		32, {2, -3, -5, -1},    -1, diff_affine_banded_ascii_ascii_sea_avx_16_32, diff_affine_banded_matsize_ascii_ascii_sea_avx_16_32},

	/** AVX2 diagonal linear-gap cost dynamic banded algorithms */
	{"BLD16A16",	SEA_HEURISTIC,	16, {2, -3, -5, -5}, 32767, diag_linear_dynamic_banded_ascii_ascii_sea_avx_16_16, diag_linear_dynamic_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"BLD32A16",	SEA_HEURISTIC,	32, {2, -3, -5, -5}, 32767, diag_linear_dynamic_banded_ascii_ascii_sea_avx_16_32, diag_linear_dynamic_banded_matsize_ascii_ascii_sea_avx_16_32},
	/** AVX2 diagonal affine-gap cost dynamic banded algorithms */
	{"BAD16A16",	SEA_HEURISTIC,	16, {2, -3, -5, -1}, 32767, diag_affine_dynamic_banded_ascii_ascii_sea_avx_16_16, diag_affine_dynamic_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"BAD32A16",	SEA_HEURISTIC,	32, {2, -3, -5, -1}, 32767, diag_affine_dynamic_banded_ascii_ascii_sea_avx_16_32, diag_affine_dynamic_banded_matsize_ascii_ascii_sea_avx_16_32},
	/** AVX2 difference linear-gap cost dynamic banded algorithms */
	{"DLD32A8",		SEA_HEURISTIC,	32, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_avx_8_32, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_avx_8_32},
	{"DLD16A16",	SEA_HEURISTIC,	16, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_avx_16_16, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"DLD32A16",	SEA_HEURISTIC,	32, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_avx_16_32, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_avx_16_32},
	/** AVX2 difference affine-gap cost dynamic banded algorithms */
	{"DAD32A8",		SEA_HEURISTIC,	32, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_avx_8_32, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_avx_8_32},
	{"DAD16A16",	SEA_HEURISTIC,	16, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_avx_16_16, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"DAD32A16",	SEA_HEURISTIC,	32, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_avx_16_32, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_avx_16_32},
	/** AVX2 difference linear-gap cost dynamic banded algorithms (exact) */
	{"DLD32A8E",	SEA_EXACT,		32, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_avx_8_32, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_avx_8_32},
	{"DLD16A16E",	SEA_EXACT,		16, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_avx_16_16, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"DLD32A16E",	SEA_EXACT,		32, {2, -3, -5, -5},    -1, diff_linear_dynamic_banded_ascii_ascii_sea_avx_16_32, diff_linear_dynamic_banded_matsize_ascii_ascii_sea_avx_16_32},
	/** AVX2 difference affine-gap cost dynamic banded algorithms (exact) */
	{"DAD32A8E",	SEA_EXACT,		32, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_avx_8_32, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_avx_8_32},
	{"DAD16A16E",	SEA_EXACT,		16, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_avx_16_16, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_avx_16_16},
	{"DAD32A16E",	SEA_EXACT,		32, {2, -3, -5, -1},    -1, diff_affine_dynamic_banded_ascii_ascii_sea_avx_16_32, diff_affine_dynamic_banded_matsize_ascii_ascii_sea_avx_16_32},
#endif /** #if HAVE_AVX2 */

	/** sentinel */
	{NULL, 0, 0, {0, 0, 0, 0}, 0, NULL, NULL}
};

/**
 * @fn print_usage
 *
 * @brief print usages of this program to stderr.
 *
 *
 * @param[in] fp : output file
 * @param[in] name : the name of the binary of this program, usually argv[0].
 * @param[in] level : message level. 0: normal, 1: detailed.
 *
 * @return none
 */
void print_usage(FILE *fp, char *name, sea_int_t level)
{
	char const *msg = 
		"Detail:\n"
		"This program takes benchmarks of the algorithms in this library. The variants used\n"
		"in the benchmarks are fixed to alg=SEA, seq=ascii, and aln=ascii. The program\n"
		"reads two input fasta files and issue queries to each algorithm from the beginning\n"
		"of the set of sequences, or generates the specified number of random and modified\n"
		"sequences and issue queries. Each elapsed time of the fill-in step, the score-search\n"
		"step, and the traceback step is taken using macros in bench.h, and printed to\n"
		"stderr.\n"
		"\n"
		"Options:\n"
		"  Help\n"
		"    -h : show this message.\n"
		"\n"
		"  Inputs and outputs\n"
		"    -q <filename> : specify an input fasta file name. the content is used as sequence a.\n"
		"    -t <filename> : specify an input fasta file name. the content is used as sequence b.\n"
		"\n"
		"    -o <filename> : specify an output csv file name. if it is not specified, the results\n"
		"         are printed to stderr.\n"
		"\n"
		"  Output destination\n"
		"    -s : print results to stdout.\n"
		"    -e : print results to stderr.\n"
		"\n"
		"  Output format\n"
		"    -b <string> : give 'legacy' if you want to get the results in the legacy (bench.c in\n"
		"         alpha branch) format.\n"
		"\n"
		"  Query sequence generation\n"
		"    -l : the length of queries. the default is 1000 (1kbp).\n"
		"    -n : the number of sequence pairs generated.\n"
		"    -x, -d : the mismatch rate, and the indel rate used to mutate sequences.\n"
		"\n"
		"  Benchmarking\n"
		"    -c <number> : specify the number of iteration.\n"
		"    -a <string> : specify algorithm. available options are\n"
		"         all : take benchmarks of all the algorithms.\n"
		"         <list> : a list of algorithms, such as 'NL32,NA32,BL32S16,BA32S16'\n"
		"    -p <number> : the number of threads which run in parallel.\n";

	fprintf(fp,
		"\n"
		"  --  libsea benchmarking program  --\n"
		"\n"
		"Usage: %s [options]\n"
		"\n"
		"%s\n",
		name, msg);
	return;
}

/**
 * @fn print_result
 *
 * @brief print results to file
 *
 * @param[in] file : a pointer to FILE
 * @param[in] arr : a pointer to an array of struct result.
 * @param[in] len : the length of the array.
 *
 * @return none.
 */
void print_result(FILE *fp, struct result *arr)
{
	int i;
	int arr_len = sizeof(algs) / sizeof(struct alg) - 1;
	for(i = 0; i < arr_len; i++) {
		fprintf(fp, "%s, %ld, %ld, %ld, %ld, %ld, %f\n",
			algs[i].name,
			arr[i].fill,
			arr[i].search,
			arr[i].trace,
			arr[i].fill + arr[i].search + arr[i].trace,
			arr[i].total,
			(arr[i].total > 0) ? (double)arr[i].total / (double)arr[0].total
							   : -1.0);
	}
	return;
}

/**
 * @fn print_result_legacy
 *
 * @brief print results to file using legacy (bench.c in the alpha branch) format.
 *
 * @param[in] file : a pointer to FILE
 * @param[in] arr : a pointer to an array of struct result.
 * @param[in] len : the length of the array.
 *
 * @return none.
 */
void print_result_legacy(FILE *fp, struct result *arr)
{
	int i, j;
	int arr_len = sizeof(algs) / sizeof(struct alg) - 1;
	char *legacyorder[] = {
		"B64",
		"DL16S8",
		"DL32S8",
		"DL16S16",
		"DL32S16",
		"BL16S8",
		"BL16S16",
		"BL32S16",
		"NL16",
		"NL32",
		"NL64",
		"DA16S8",
		"DA32S8",
		"DA16S16",
		"DA32S16",
		"BA16S8",
		"BA16S16",
		"BA32S16",
		"NA16",
		"NA32",
		"NA64"		
	};
	int reordermap[sizeof(legacyorder)];

	/**
	 * make reordering map
	 */
	for(j = 0; j < sizeof(legacyorder)/sizeof(char *); j++) {
		for(i = 0; i < arr_len; i++) {
			if(strcmp(legacyorder[j], algs[i].name) == 0) {
				reordermap[j] = i;
			}
		}
		if(i == arr_len) { legacyorder[j] = 0; }
	}
	for(i = 0; i < sizeof(legacyorder)/sizeof(char *); i++) {
		fprintf(fp, "%s, ", algs[reordermap[i]].name);
	}
	fprintf(fp, "\n");
	for(i = 0; i < sizeof(legacyorder)/sizeof(char *); i++) {
		fprintf(fp, "%ld, ", (arr[reordermap[i]].fill + 500) / 1000);
	}
	fprintf(fp, "\n");
	for(i = 0; i < sizeof(legacyorder)/sizeof(char *); i++) {
		fprintf(fp, "%ld, ", (arr[reordermap[i]].search + 500) / 1000);
	}
	fprintf(fp, "\n");
	for(i = 0; i < sizeof(legacyorder)/sizeof(char *); i++) {
		fprintf(fp, "%ld, ", (arr[reordermap[i]].trace + 500) / 1000);
	}
	fprintf(fp, "\n");
	return;
}

/**
 * @fn runall
 *
 * @brief run benchmarks of all the functions in the table.
 *
 * @param[in] count : number of queries issued in the benchmark.
 * @param[in] pa : a pointer to an array of ascii query sequences a.
 * @param[in] pb : a pointer to an array of ascii query sequences b.
 *
 * @return a pointer to an array of struct result (NULL terminated)
 */
struct result *runall(
	sea_int_t count,
	char **pa,
	char **pb)
{
	sea_int_t cnt_bench = sizeof(algs) / sizeof(struct alg) - 1;	/** a number of types of functions */
	sea_int_t i;
	sea_int_t flag_bandwidth;
	struct sea_context *ctx;
	struct result *arr;

	arr = (struct result *)malloc(cnt_bench * sizeof(struct result));

	for(i = 0; i < cnt_bench; i++) {
		switch(algs[i].bandwidth) {
			case 16: flag_bandwidth = SEA_BANDWIDTH_16; break;
			case 32: flag_bandwidth = SEA_BANDWIDTH_32; break;
			case 64: flag_bandwidth = SEA_BANDWIDTH_64; break;
			default: flag_bandwidth = SEA_BANDWIDTH_32; break;
		}
		/** generate a context */
		ctx = sea_init_fp(
			SEA_SEA |
			SEA_BANDED_DP |
			SEA_SEQ_ASCII |
			SEA_ALN_ASCII |
			algs[i].flags |
			flag_bandwidth,
			algs[i].sea_sea,
			algs[i].sea_matsize,
			algs[i].scmat[0], algs[i].scmat[1],	/** M, X */
			algs[i].scmat[2], algs[i].scmat[3],	/** Gi, Ge */
			0);

		/** take benchmark */
		arr[i] = benchmark(ctx, count, pa, pb);

		/** clean context */
		sea_clean(ctx);
	}
	return arr;
}

/**
 * @fn random_base
 *
 * @brief return a character randomly from {'A', 'C', 'G', 'T'}.
 */
char random_base(void)
{
	switch(rand() % 4) {
		case 0: return 'A';
		case 1: return 'C';
		case 2: return 'G';
		case 3: return 'T';
		default: return 'A';
	}
}

/**
 * @fn generate_random_sequence
 *
 * @brief malloc memory of size <len>, then fill it randomely with {'A', 'C', 'G', 'T'}.
 *
 * @param[in] len : the length of sequence to generate.
 * @return a pointer to the generated sequence.
 */
char *generate_random_sequence(int len)
{
	int i;
	char *seq;		/** a pointer to sequence */
	seq = (char *)malloc(sizeof(char) * (len + 1));
	if(seq == NULL) { return NULL; }
	for(i = 0; i < len; i++) {
		seq[i] = random_base();
	}
	seq[len] = '\0';
	return seq;
}

/**
 * @fn generate_mutated_sequence
 *
 * @brief take a DNA sequence represented in ASCII, mutate the sequence at the given probability.
 *
 * @param[in] seq : a pointer to the string of {'A', 'C', 'G', 'T'}.
 * @param[in] x : mismatch rate
 * @param[in] d : insertion / deletion rate
 * @param[in] bw : bandwidth (the alignment path of the input sequence and the result does not go out of the band)
 *
 * @return a pointer to mutated sequence.
 */
char *generate_mutated_sequence(char *seq, double x, double d, int bw)
{
	int i, j, wave = 0;			/** wave is q-coordinate of the alignment path */
	int len;
	char *mutated_seq;

	if(seq == NULL) { return NULL; }
	len = strlen(seq);
	mutated_seq = (char *)malloc(sizeof(char) * (len + 1));
	if(mutated_seq == NULL) { return NULL; }
	for(i = 0, j = 0; i < len; i++) {
		if(((double)rand() / (double)RAND_MAX) < x) {
			mutated_seq[i] = random_base();	j++;	/** mismatch */
		} else if(((double)rand() / (double)RAND_MAX) < d) {
			if(rand() & 0x01 && wave > -bw+1) {
				mutated_seq[i] = (j < len) ? seq[j++] : random_base();
				j++; wave--;						/** deletion */
			} else if(wave < bw-2) {
				mutated_seq[i] = random_base();
				wave++;								/** insertion */
			} else {
				mutated_seq[i] = (j < len) ? seq[j++] : random_base();
			}
		} else {
			mutated_seq[i] = (j < len) ? seq[j++] : random_base();
		}
	}
	mutated_seq[len] = '\0';
	return mutated_seq;
}

/**
 * @fn load_fasta_sequence
 *
 * @brief read a fasta file, dump a content to the memory, parse sequences and return an array of pointers to sequences.
 *
 * @param[in] fp : FILE *fp to input fasta file.
 *
 * @return a pointer to an array of pointers to ASCII(char) sequences.
 */
char **load_fasta_sequence(FILE *fp)
{
	int i, j, seq_cnt;
	int c;
	char *buf, *ptr, **pa;
	struct stat st;

	fstat(fileno(fp), &st);
	buf = (char *)malloc(sizeof(char) * (st.st_size + 1));
	
	/**
	 * dump file to buffer, eliminating \r and \n's, and parsing with '>'.
	 */
	for(i = 0, seq_cnt = 0; i < st.st_size;) {
		if((char)(c = getc(fp)) != EOF) {
			if(c != '>') {
				if((char)c != '\r' && (char)c != '\n') { buf[i++] = (char)c; }
			} else {
				/**
				 * skip until fp reaces the end of the line
				 */
				while((char)(c = getc(fp)) != '\r' && (char)c != '\n') {
					if(c == EOF) { break; }
				}
				while((char)(c = getc(fp)) == '\r' || (char)c == '\n') {
					if(c == EOF) { break; }
				}
				buf[i++] = '\0'; seq_cnt++;
			}
		} else {
			break;		/** when fp reached EOF */
		}
	}

	/**
	 * malloc an array of pointers
	 */
	pa = (char **)malloc(sizeof(char *) * (seq_cnt + 3));

	/**
	 * fill array
	 */
	for(i = 0, j = 0; i < st.st_size && j < seq_cnt; i++) {
		while(buf[i++] != '\0') {}
		pa[j++] = buf+i;
	}
	pa[seq_cnt] = NULL;
	pa[seq_cnt + 1] = buf;
	pa[seq_cnt + 2] = NULL;
	return(pa);
}

/**
 * @fn free_fasta_sequence
 *
 * @brief free memories malloc'd by load_fasta_sequence
 *
 * @param[in] pa : a pointer to an array of pointers to ASCII(char) sequences.
 */
void free_fasta_sequence(char **pa)
{
	int i;

	for(i = 0; pa[i] != NULL; i++) {}		/** search the end of array */
	free(pa[i+1]);
	free(pa);
	return;
}

/**
 * @fn main
 */
int main(int argc, char *argv[])
{
	sea_int_t i;
	sea_int_t cnt_bench = sizeof(algs) / sizeof(struct alg) - 1;	/** a number of types of functions */
	char *qfile = NULL,		/** query sequence file */
		 *tfile = NULL,		/** target sequence file */
		 *ofile = NULL;		/** output csv file */
	char **pa = NULL,
		 **pb = NULL;
	sea_int_t len = 1000,	/** sequence length */
			  cnt = 1000,	/** number of sequences */
		 	  legacy = 0; 	/** output format type. use legacy format if 1 */
	double x = 0.1,			/** mismatch rate */
		   d = 0.05;		/** insertion / deletion rate */
	struct result *arr;

	FILE *qfp = NULL,		/** query sequence file (sequence a) */
		 *tfp = NULL,		/** target sequence file (sequence b) */
		 *ofp = NULL;		/** output file */
	FILE *out = stderr;

	/**
	 * parse args
	 */
	while((i = getopt(argc, argv, "q:t:o:l:x:d:c:a:seb:h")) != -1) {
		switch(i) {
			/**
			 * input / output filenames.
			 */
			case 'q': qfile = (char *)optarg; break;
			case 't': tfile = (char *)optarg; break;
			case 'o': ofile = (char *)optarg; break;
			/**
			 * sequence generation parameters.
			 */
			case 'l': len = atoi((char *)optarg); break;
			case 'x': x = atof((char *)optarg); break;
			case 'd': d = atof((char *)optarg); break;
			/**
			 * benchmarking options
			 */
			case 'c': cnt = atoi((char *)optarg); break;
			case 'a': printf("%s\n", optarg); break;
			/**
			 * output destination options
			 */
			case 's': out = stdout; break;
			case 'e': out = stderr; break;
			/**
			 * output format options
			 */
			case 'b':
				if(strcmp("legacy", optarg) == 0 ||
				   strcmp("leg", optarg) == 0) {
					legacy = 1;
				}
				break;
			/**
			 * the others: print help message
			 */
			case 'h':
			default: print_usage(out, argv[0], 1); exit(1);
		}
	}

	if(qfile == NULL || tfile == NULL) {
		/**
		 * generate random sequences and mutated sequences if no input
		 * file is specified.
		 */
		pa = (char **)malloc(sizeof(char *) * (cnt + 1));		/** an array of pointers to sequences */
		pb = (char **)malloc(sizeof(char *) * (cnt + 1));
		for(i = 0; i < cnt; i++) {
			pa[i] = generate_random_sequence(len);
			pb[i] = generate_mutated_sequence(pa[i], x, d, 8);
		}
		pa[cnt] = NULL; pb[cnt] = NULL;
	} else {
		/**
		 * extract sequences from fasta files.
		 */
		if((qfp = fopen(qfile, "r")) != NULL) {
			pa = load_fasta_sequence(qfp);
		} else {
			fprintf(stderr, "Couldn't open input file '%s'.\n", qfile);
		}
		if((tfp = fopen(tfile, "r")) != NULL) {
			pb = load_fasta_sequence(tfp);
		} else {
			fprintf(stderr, "Couldn't open input file '%s'.\n", tfile);
		}
	}

	/**
	 * run benchmark.
	 */
	arr = runall(1, pa, pb);
	
	/**
	 * print results.
	 */
	if(legacy) {
		print_result_legacy(stdout, arr);
	} else {
		print_result(stdout, arr);
	}

	/**
	 * write results to file if output file is specified.
	 */
	if(ofile != NULL) {
		if((ofp = fopen(ofile, "w")) != NULL) {
			if(legacy) {
				print_result_legacy(ofp, arr);
			} else {
				print_result(ofp, arr);
			}
		} else {
			fprintf(stderr, "Couldn't open output file '%s'.\n", ofile);
		}
	}

	/**
	 * clean malloc'd memories
	 */
	free(arr);
	/**
	 * sequence a
	 */
	if(qfp != NULL) {
		fclose(qfp);
		free_fasta_sequence(pa);
	} else {
		for(i = 0; i < cnt; i++) {
			free(pa[i]);
		}
		free(pa);
	}
	/**
	 * sequence b
	 */
	if(tfp != NULL) {
		fclose(tfp);
		free_fasta_sequence(pb);
	} else {
		for(i = 0; i < cnt; i++) {
			free(pb[i]);
		}
		free(pb);
	}
	return 0;
}

/**
 * end of benchmark.c
 */
