

libsea -- a SIMD-based ultrafast sequence alignment library
===========================================================


## 概要

libseaは、バイオインフォマティクスにおける塩基配列のアライメントを、高速に計算する
ためのライブラリです。ローカルアライメント (Smith-Watermanのアルゴリズム)、
グローバルアライメント (Needleman-Wunschのアルゴリズム)、シード伸長アライメント
(seed-and-extendアライメントおよびX-dropアライメント)の各アルゴリズムを、
SIMD命令を使用した並列演算により高速に計算することができます。


## 特徴

* 高速: SeqAnなどの既存のライブラリと比べて10倍程度高速です
* 汎用性: シンプルなAPIで、基本的なアルゴリズムを網羅しています
* 低い環境依存性: 共通のAPIにより、SIMD命令を使えない環境でもコードを書き換える必要がありません
* 使いやすいライセンス: Apache v2ライセンスにより、既存のソフトウェアに組み込みやすくなっています


## ベンチマーク


## 環境

### アーキテクチャ

* x86_64 (SSE4.1命令もしくはAVX2命令が利用できるもの)

### OSとコンパイラ

* Linux
  * icc
  * gcc
  * clang

* Mac OS X
  * clang
  * gcc

* Windows (cygwin)
  * msvc
  * gcc
  * clang

### その他

* Python (2.7 or 3.3)
* coreutils (nmを使用します)


## インストール

libseaは、ビルドシステムにWafを使用しています。ターミナル上で以下のコマンドを入力する
ことで、インストールが可能です。

    $ git clone https://suzukihajime@bitbucket.org/suzukihajime/libsea.git
    $ cd libsea
    $ python waf configure
    $ python waf build
    $ python waf install (もしくは sudo python waf install)

### インストールされるファイル

waf installにより、以下のファイルがインストールされます。

sea.h      ($PREFIX/include)
libsea.so  ($PREFIX/lib)
libsea.a   ($PREFIX/lib)


## 使い方

### C言語

APIは以下の4つしかありません。sea_initでコンテキストを生成し、sea_alignで
アライメントの計算を行います。残りの2つは必要なくなったデータの後処理を行うためのものです。

* sea_init
* sea_align
* sea_aln_free
* sea_clean

C言語によるサンプルコードと実行結果を以下に示します。

sea_sample.c:

	#include <stdio.h>
	#include <sea.h>

	int main(void) {

	char const *a = "ATCTATCATCTGTCGCTCGATCGA";
	char const *b = "ATCTATCATCTGTCGCTCGATCGA";

	/**
	 * アライメントコンテキストを初期化する
	 */
	struct sea_context *ctx = sea_init(
		SEA_SW |				/** Smith-Watermanのアルゴリズム */
		SEA_AFFINE_GAP_COST |	/** アフィンギャップコスト */
		SEA_BANDED_DP |			/** banded DP */
		SEA_BANDWIDTH_32 |		/** バンド幅 : 32 */
		SEA_SEQ_ASCII |			/** 入力フォーマット : ASCII */
		SEA_ALN_ASCII,			/** 出力フォーマット : ASCII */
		10000,					/** 最大入力配列長 : 10000 */
		2, -3, -5, -1,			/** マッチ / ミスマッチ / ギャップ開始 / ギャップ伸長コスト */
		100);					/** X-dropスレッショルド */
	
	/**
	 * アライメントを行う
	 */
	struct sea_result *aln = sea_align(
		ctx,					/** アライメントコンテキスト */
		a,						/** 配列aへのポインタ */
		0,						/** 配列a上の読み込み先頭位置 */
		strlen(a),				/** 先頭位置からの長さ */
		b,						/** 配列bへのポインタ */
		0,						/** 配列b上の読み込み先頭位置 */
		strlen(b));				/** 先頭位置からの長さ */

	/**
	 * 結果を表示。sea_aln構造体には、以下のメンバが含まれる。
	 * 位置、長さ、スコア:      aln.pos, aln.len, aln.score
	 * アライメント文字列:      aln.aln
	 * 入力に関する情報:       aln.a, aln.apos, aln.alen
	 *                       aln.b, aln.bpos, aln.blen
	 */
	printf("%s\n", aln.aln);	/** MMMMMM... と表示される */
	
	/** 必要のなくなった結果をfreeする */
	sea_aln_free(ctx, aln);

	/** コンテキストの解放 */
	sea_clean(ctx);

	return 0;
	}

実行結果:

	$ gcc -Wall -o sea_sample sea_sample.c -lsea
	$ ./sea_sample
	MMMMMMMMMMMMMMMMMMMMMMMM
	$

### C++

bindings/c++ディレクトリにラッパを用意しています。それを利用したサンプルを以下に示します。

### Python


### Java


### D


