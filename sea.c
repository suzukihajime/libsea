
/**
 * @file sea.c
 * @brief a top level implementation of the libsea C API
 * @author Hajime Suzuki
 * @date 12/29/2014
 */

#include <stdlib.h>				/* for malloc / free */
#include "include/sea.h"		/* definitions of public APIs and structures */
#include "util/util.h"			/* definitions of internal utility functions */
#include "build/config.h"

#if HAVE_SCALAR
	#include "scalar/scalar.h"	/* for scalar_get_sea */
#endif
#if HAVE_BIT
	#include "bit/bit.h"		/* for bit_get_sea */
#endif
#if HAVE_SIMD
	#include "simd/simd.h"		/* for simd_get_sea */
#endif
#if HAVE_ALLOCA_H
 	#include <alloca.h>
#endif

/**
 * @fn aligned_malloc
 *
 * @brief an wrapper of posix_memalign
 *
 * @param[in] size : size of memory in bytes.
 * @param[in] align : alignment size.
 *
 * @return a pointer to the allocated memory.
 */
void *aligned_malloc(size_t size, size_t align)
{
	void *ptr;
	posix_memalign(&ptr, align, size);
	return(ptr);
}

/**
 * @fn aligned_free
 *
 * @brief free memory which is allocated by aligned_malloc
 *
 * @param[in] ptr : a pointer to the memory to be freed.
 */
void aligned_free(void *ptr)
{
	free(ptr);
	return;
}

/**
 * @macro AMALLOC
 *
 * @brief an wrapper macro of alloca or aligned_malloc
 */
#define ALLOCA_THRESH_SIZE		( 1000000 )		/** 1MB */

#if HAVE_ALLOCA_H
	#define AMALLOC(ptr, size, align) { \
	 	if((size) > ALLOCA_THRESH_SIZE) { \
	 		(ptr) = aligned_malloc(size, align); \
	 	} else { \
	 		(ptr) = alloca(size); (ptr) = (((ptr)+(align)-1) / (align)) * (align); \
	 	} \
	}
#else
	#define AMALLOC(ptr, size, align) { \
		(ptr) = aligned_malloc(size, align); \
	}
#endif /* #if HAVE_ALLOCA_H */

/**
 * @macro AFREE
 *
 * @breif an wrapper macro of alloca or aligned_malloc
 */
#if HAVE_ALLOCA_H
	#define AFREE(ptr, size) { \
	 	if((size) > ALLOCA_THRESH_SIZE) { \
	 		aligned_free(ptr); \
	 	} \
	}
#else
	#define AFREE(ptr, size) { \
		aligned_free(ptr); \
	}
#endif /* #if HAVE_ALLOCA_H */

/**
 * @fn sea_init_flags_vals
 *
 * @brief internal functions, check arguments and fill proper values (or default values) to sea_context.
 *
 * @param[ref] ctx : a pointer to blank context.
 * @param[in] flags : option flags. see flags.h for more details.
 * @param[in] len : maximal alignment length. give 0 if you are not sure. (len >= 0)
 * @param[in] m : match award in the Dynamic Programming. (m >= 0)
 * @param[in] x :  mismatch cost. (x < m)
 * @param[in] gi : gap open cost. (or just gap cost in the linear-gap cost) (2gi <= x)
 * @param[in] ge : gap extension cost. (ge <= 0) valid only in the affine-gap cost. the total penalty of the gap with length L is gi + (L - 1)*ge.
 * @param[in] xdrop : xdrop threshold. (xdrop > 0) valid only in the seed-and-extend alignment. the extension is terminated when the score S meets S < max - xdrop.
 *
 * @return SEA_SUCCESS if proper values are set, corresponding error number if failed.
 *
 * @sa sea_init, sea_init_fp
 */
static sea_int_t
sea_init_flags_vals(
	struct sea_params *param,
	sea_int_t flags,
	sea_int_t len,
	sea_int_t m,
	sea_int_t x,
	sea_int_t gi,
	sea_int_t ge,
	sea_int_t xdrop)
{
	sea_int_t int_flags = flags;		/* internal copy of the flags */
	sea_int_t bandwidth = 0;
	sea_int_t error_label = SEA_ERROR;

	/** default: affine-gap cost */
	if((int_flags & SEA_FLAGS_MASK_COST) == 0) {
		int_flags = (int_flags & ~SEA_FLAGS_MASK_COST) | SEA_AFFINE_GAP_COST;
	}

	/** default: banded DP */
	if((int_flags & SEA_FLAGS_MASK_DP) == 0) {
		int_flags = (int_flags & ~SEA_FLAGS_MASK_DP) | SEA_BANDED_DP;
	}

	/** default: heuristic search */
	if((int_flags & SEA_FLAGS_MASK_POLICY) == 0) {
		int_flags = (int_flags & ~SEA_FLAGS_MASK_POLICY) | SEA_HEURISTIC;
	}

	/** default bandwidth of the banded DP: 32 */
	if((int_flags & SEA_FLAGS_MASK_BANDWIDTH) == 0) {
		int_flags = (int_flags & ~SEA_FLAGS_MASK_BANDWIDTH) | SEA_BANDWIDTH_32;
	}

	/** default input sequence format: ASCII */
	if((int_flags & SEA_FLAGS_MASK_SEQ) == 0) {
		int_flags = (int_flags & ~SEA_FLAGS_MASK_SEQ) | SEA_SEQ_ASCII;
	}

	/** default output format: ASCII string.
	 * (yields MMMM for the input pair (AAAA and AAAA).
	 * use SEA_ALN_CIG_BYTE for cigar string.)
	 */
	if((int_flags & SEA_FLAGS_MASK_ALN) == 0) {
		int_flags = (int_flags & ~SEA_FLAGS_MASK_ALN) | SEA_ALN_ASCII;
	}

	/** check if DP cost values are proper. the cost values must satisfy m >= 0, x < m, 2*gi <= x, ge <= 0. */
	if(m < 0 || x >= m || 2*gi > x || ge > 0) {
		return SEA_ERROR_INVALID_COST;
	}

	/** check if the unit-cost is available. if so, use faster bit-parallel algorithm. */
	if(m == 2 && x == 1 && gi == 0
		&& (((int_flags & SEA_FLAGS_MASK_COST) == SEA_LINEAR_GAP_COST)
		|| (((int_flags & SEA_FLAGS_MASK_COST) == SEA_AFFINE_GAP_COST) && (ge == -1)))) {
		int_flags = (int_flags & ~SEA_FLAGS_MASK_COST) | SEA_UNIT_COST;
	}

	/** if unit-cost option is specified, correct the cost values */
	if((int_flags & SEA_FLAGS_MASK_COST) == SEA_UNIT_COST) {
		m = 2; x = 1; gi = 0; ge = 0;
	}

	/** if linear-gap option is specified, set unused value (gap extension cost) zero. */
	if((int_flags & SEA_FLAGS_MASK_COST) == SEA_LINEAR_GAP_COST) {
		ge = 0;
	}
	
	/* push scores to context */
	param->m = m;
	param->x = x;
	param->gi = gi;
	param->ge = ge;

	/* special parameters */
	param->mask = 0;		/** for the mask-match algorithm */
	param->k = 4;			/** search length stretch ratio: default is 4 */

	/* check if len and xdrop values are proper. (len > 0 and xdrop > 0) */
	if(len < 0 || xdrop < 0) {
		return SEA_ERROR_INVALID_ARGS;
	}
	param->xdrop = xdrop;

	/* determine bandwidth */
	if((int_flags & SEA_FLAGS_MASK_DP) == SEA_BANDED_DP) {
		/* when banded dp is specified, check flags and fill the bandwidth variable with proper value. */
		switch(int_flags & SEA_FLAGS_MASK_BANDWIDTH) {
			case SEA_BANDWIDTH_16: bandwidth = 16; break;
			case SEA_BANDWIDTH_32: bandwidth = 32; break;
			case SEA_BANDWIDTH_64: bandwidth = 64; break;
			case SEA_BANDWIDTH_128: bandwidth = 128; break;
			default: bandwidth = 32; break;		/* if bandwidth is not specified, set the default bandwidth. */
		}
	} else {
		/* leave the bandwidth zero since the bandwidth is nonsense parameter in the full dp. */
		bandwidth = 0;
	}
	param->bandwidth = bandwidth;

	/* push flags to the context */
	param->flags = int_flags;

	error_label = SEA_SUCCESS;
	return(error_label);
}

/**
 * @fn sea_init
 *
 * @brief constructs and initializes an alignment context.
 *
 * @param[in] flags : option flags. see flags.h for more details.
 * @param[in] len : maximal alignment length. len must hold len > 0.
 * @param[in] m : match award in the Dynamic Programming. (m >= 0)
 * @param[in] x :  mismatch cost. (x < m)
 * @param[in] gi : gap open cost. (or just gap cost in the linear-gap cost) (2gi <= x)
 * @param[in] ge : gap extension cost. (ge <= 0) valid only in the affine-gap cost. the total penalty of the gap with length L is gi + (L - 1)*ge.
 * @param[in] xdrop : xdrop threshold. (xdrop > 0) valid only in the seed-and-extend alignment. the extension is terminated when the score S meets S < max - xdrop.
 *
 * @return a pointer to sea_context structure.
 *
 * @sa sea_free, sea_sea
 */
struct sea_context *sea_init(
	sea_int_t flags,
	sea_int_t len,
	sea_int_t m,
	sea_int_t x,
	sea_int_t gi,
	sea_int_t ge,
	sea_int_t xdrop)
{
	struct sea_context *ctx = NULL;
	sea_int_t alg_index = 0;			/* index of function array. see details for util/table.th */
	sea_int_t seq_index = 0;			/* index of function array. */
	sea_int_t aln_index = 0;			/* index of function array. */
	sea_int_t error_label = SEA_ERROR;

	struct sea_params param = {0, 0, 0, 0, 0, 0, 0, 0};
	struct sea_funcs fp = {NULL, NULL};

	/** malloc sea_context */
	ctx = (struct sea_context *)malloc(sizeof(struct sea_context));
	if(ctx == NULL) {
		error_label = SEA_ERROR_OUT_OF_MEM;
		goto _sea_init_error_handler;
	}

	/** init value fields of sea_context */
	if((error_label = sea_init_flags_vals(&param, flags, len, m, x, gi, ge, xdrop)) != SEA_SUCCESS) {
		goto _sea_init_error_handler;
	}

	/** calculate index of function table. see util/table.th and include/sea.h */
	alg_index = ((param.flags & SEA_FLAGS_MASK_ALG)>>SEA_FLAGS_POS_ALG) - 1;
	seq_index = ((param.flags & SEA_FLAGS_MASK_SEQ)>>SEA_FLAGS_POS_SEQ) - 1;
	aln_index = ((param.flags & SEA_FLAGS_MASK_ALN)>>SEA_FLAGS_POS_ALN) - 1;

	/** retrieve the DP function. the priority is: unit-cost > simd > scalar */
	if((param.flags & SEA_FLAGS_MASK_COST) == SEA_UNIT_COST) {
		/* when the unit-cost algorithm is available */
		#if HAVE_BIT
			error_label = bit_get_sea(&fp, param, len, alg_index, seq_index, aln_index);
		#elif HAVE_SCALAR
			error_label = scalar_get_sea(&fp, param, len, alg_index, seq_index, aln_index);
		#endif
	} else {
		/* check if the SIMD algorithm is available on the machine */
		#if HAVE_SIMD
			if((error_label = simd_get_sea(&fp, param, len, alg_index, seq_index, aln_index)) != SEA_SUCCESS) {
				#if HAVE_SCALAR
					/* if not, use the scalar implementation instead */
					error_label = scalar_get_sea(&fp, param, len, alg_index, seq_index, aln_index);
				#endif
			}
		#elif HAVE_SCALAR
			error_label = scalar_get_sea(&fp, param, len, alg_index, seq_index, aln_index);
		#endif
	}
	/* check if error_label is success */
	if(error_label != SEA_SUCCESS) {
		goto _sea_init_error_handler;
	}

	/* check if the retrieved function pointers are not NULL */
	if(fp._sea_matsize == NULL || fp._sea_sea == NULL) {
		error_label = SEA_ERROR_UNSUPPORTED_ALG;
		goto _sea_init_error_handler;
	}

	/* push params and fps to the context */
	ctx->param = param;
	ctx->fp = fp;
	ctx->error_label = SEA_SUCCESS;
	return ctx;

_sea_init_error_handler:
	if(ctx != NULL) {
		/** clear params */
		ctx->param.flags = 0;
		ctx->param.m = 0;
		ctx->param.x = 0;
		ctx->param.gi = 0;
		ctx->param.ge = 0;
		ctx->param.mask = 0;
		ctx->param.xdrop = 0;
		ctx->param.bandwidth = 0;

		/** clear function pointers */
		ctx->fp._sea_matsize = NULL;
		ctx->fp._sea_sea = NULL;

		/** set error label (the value can be retrieved with sea_get_error) */
		ctx->error_label = error_label;
	}
	return ctx;
}

/**
 * @fn sea_init_fp
 *
 * @brief constructs and initializes an alignment context.
 *
 * @param[in] sea_sea : a pointer to an alignment function.
 * @param[in] sea_matsize : a pointer to a function which calculates a size of a matrix.
 * @param[in] m : match award in the Dynamic Programming. (m >= 0)
 * @param[in] x :  mismatch cost. (x < m)
 * @param[in] gi : gap open cost. (or just gap cost in the linear-gap cost) (2gi <= x)
 * @param[in] ge : gap extension cost. (ge <= 0) valid only in the affine-gap cost. the total penalty of the gap with length L is gi + (L - 1)*ge.
 * @param[in] xdrop : xdrop threshold. (xdrop > 0) valid only in the seed-and-extend alignment. the extension is terminated when the score S meets S < max - xdrop.
 *
 * @return a pointer to sea_context structure.
 *
 * @sa sea_free, sea_sea
 */
struct sea_context *sea_init_fp(
	sea_int_t flags,
	sea_int_t (*sea_sea)(
		struct sea_result *aln,
		struct sea_params param,
		void *mat),
	sea_int_t (*sea_matsize)(
		sea_int_t alen,
		sea_int_t blen,
		sea_int_t bandwidth),
	sea_int_t m,
	sea_int_t x,
	sea_int_t gi,
	sea_int_t ge,
	sea_int_t xdrop)
{
	struct sea_context *ctx = NULL;
	sea_int_t error_label = SEA_ERROR;

	struct sea_params param = {0, 0, 0, 0, 0, 0, 0, 0};
	struct sea_funcs fp = {NULL, NULL};

	/** malloc sea_context */
	ctx = (struct sea_context *)malloc(sizeof(struct sea_context));
	if(ctx == NULL) {
		error_label = SEA_ERROR_OUT_OF_MEM;
		goto _sea_init_fp_error_handler;
	}

	/** init value fields of sea_context */
	if((error_label = sea_init_flags_vals(&param, flags, 0, m, x, gi, ge, xdrop)) != SEA_SUCCESS) {
		goto _sea_init_fp_error_handler;
	}

	/* check if the retrieved function pointers are not NULL */
	if(sea_matsize == NULL || sea_sea == NULL) {
		error_label = SEA_ERROR_INVALID_ARGS;
		goto _sea_init_fp_error_handler;
	}

	/* set pointers */
	fp._sea_matsize = sea_matsize;
	fp._sea_sea = sea_sea;

	/* push params and fps to the context */
	ctx->param = param;
	ctx->fp = fp;
	ctx->error_label = SEA_SUCCESS;
	return ctx;

_sea_init_fp_error_handler:
	if(ctx != NULL) {
		/** clear params */
		ctx->param.flags = 0;
		ctx->param.m = 0;
		ctx->param.x = 0;
		ctx->param.gi = 0;
		ctx->param.ge = 0;
		ctx->param.mask = 0;
		ctx->param.xdrop = 0;
		ctx->param.bandwidth = 0;

		/** clear function pointers */
		ctx->fp._sea_matsize = NULL;
		ctx->fp._sea_sea = NULL;

		/** set error label (the value can be retrieved with sea_get_error) */
		ctx->error_label = error_label;
	}
	return ctx;
}

/**
 * @fn sea_align
 *
 * @brief alignment function. 
 *
 * @param[ref] ctx : a pointer to an alignment context structure. must be initialized with sea_init function.
 * @param[in] a : a pointer to the query sequence a. see seqreader.h for more details of query sequence formatting.
 * @param[in] apos : the alignment start position in a. (0 <= apos < length(sequence a)) (or search start position in the Smith-Waterman algorithm). the alignment includes the position apos.
 * @param[in] alen : the extension length in a. (0 < alen) (to be exact, alen is search area length in the Smith-Waterman algorithm. the maximum extension length in the seed-and-extend alignment algorithm. the length of the query a to be aligned in the Needleman-Wunsch algorithm.)
 * @param[in] b : a pointer to the query sequence b.
 * @param[in] bpos : the alignment start position in b. (0 <= bpos < length(sequence b))
 * @param[in] blen : the extension length in b. (0 < blen)
 *
 * @return an pointer to the sea_result structure.
 *
 * @sa sea_init
 */
struct sea_result *sea_align(
	struct sea_context *ctx,
	void const *a,
	sea_int_t apos,
	sea_int_t alen,
	void const *b,
	sea_int_t bpos,
	sea_int_t blen)
{
	sea_int_t alnsize,					/** size of struct sea_result and alignment string */
			  matsize;					/** size of dynamic programming matrix */
	sea_int_t const masize = 256;		/** size of memory alignment */
	struct sea_result *aln = NULL;
	void *mat;
	sea_int_t error_label = SEA_ERROR;

	/* check if ctx points to valid context */
	if(ctx == NULL) {
		error_label = SEA_ERROR_INVALID_CONTEXT;
		goto _sea_error_handler;
	}
	/* check if the pointers, start position values, extension length values are proper. */
	if(a == NULL || b == NULL || apos < 0 || alen < 0 || bpos < 0 || blen < 0) {
		error_label = SEA_ERROR_INVALID_ARGS;
		goto _sea_error_handler;
	}

	/* calculate the size of sea_result (a structure to hold alignment results) */
	alnsize = sizeof(char) * (alen + blen + ctx->param.bandwidth + 1) + sizeof(struct sea_result);

	/* malloc memory of sea_result */
	aln = (struct sea_result *)malloc(alnsize);
	if(aln == NULL) {
		error_label = SEA_ERROR_OUT_OF_MEM;
		goto _sea_error_handler;
	}
	/* clean up sea_result */
	aln->aln = (void *)((struct sea_result *)aln + 1);
	((char *)(aln->aln))[0] = 0;
	aln->score = 0;
	aln->a = a; aln->apos = apos; aln->alen = alen;
	aln->b = b; aln->bpos = bpos; aln->blen = blen;
	aln->len = alnsize;
	aln->ctx = ctx;

	/**
	 * if one of the length is zero, returns with score = 0 and aln = "".
	 */
	if(alen == 0 || blen == 0) {
		return aln;
	}

	/* malloc memory of the DP matrix */
	matsize = ctx->fp._sea_matsize(alen, blen, ctx->param.bandwidth);
	matsize = ((matsize + masize - 1) / masize) * masize;
//	mat = (void *)aligned_malloc(matsize, masize);	/** 256-byte aligned */
	AMALLOC(mat, matsize, masize);
	if(mat == NULL) {
		error_label = SEA_ERROR_OUT_OF_MEM;
		goto _sea_error_handler;
	}

	/* do alignment */
	error_label = ctx->fp._sea_sea(aln, ctx->param, mat);
	if(error_label != SEA_SUCCESS) {
		goto _sea_error_handler;					/** when the path went out of the band */
	}

	/* clean DP matrix */
//	aligned_free(mat);
	AFREE(mat, matsize);

	return aln;

_sea_error_handler:
	if(aln != NULL) {
//		aln->a = aln->b = aln->aln = NULL;
		((char *)(aln->aln))[0] = 0;
		aln->len = error_label;
		aln->score = 0;
		aln->apos = aln->bpos = aln->alen = aln->blen = 0;
	}
	return aln;
}

/**
 * @fn sea_get_error_num
 *
 * @brief extract error number from a context or a result
 *
 * @param[ref] ctx : a pointer to an alignment context structure. can be NULL.
 * @param[ref] aln : a pointer to a result structure. can be NULL.
 *
 * @return error number, defined in sea_error
 */
sea_int_t sea_get_error_num(
	struct sea_context *ctx,
	struct sea_result *aln)
{
	sea_int_t error_label = SEA_SUCCESS;
	if(ctx != NULL) {
		error_label = ctx->error_label;
	}
	if(aln != NULL) {
		error_label = aln->len;
	}
	return error_label;
}

/**
 * @fn sea_aln_free
 *
 * @brief clean up sea_result structure
 *
 * @param[in] aln : an pointer to sea_result structure.
 *
 * @return none.
 *
 * @sa sea_sea
 */
void sea_aln_free(
	struct sea_result *aln)
{
	if(aln != NULL) {
		free(aln);
		return;
	}
	return;
}

/**
 * @fn sea_clean
 *
 * @brief clean up the alignment context structure.
 *
 * @param[in] ctx : a pointer to the alignment structure.
 *
 * @return none.
 *
 * @sa sea_init
 */
void sea_clean(
	struct sea_context *ctx)
{
	if(ctx != NULL) {
		free(ctx);
		return;
	}
	return;
}

/*
 * end of sea.c
 */
