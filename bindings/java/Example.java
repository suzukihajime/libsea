
/**
 * @file Example.java
 *
 * @brief a simple example of java wrapper of libsea API.
 *
 * @detail
 * Compile:
 * 
 * If you need to rebuild libsea.jar, see AlignmentContext.java.
 * To compile this file;
 * $ javac -cp libsea.jar Example.java 
 * and run;
 * $ java -cp .:libsea.jar Example
 */

import libsea.*;

class Example {
	public static void main(String[] args) {
		String a = new String("AAAA");
		String b = new String("AAAA");

		AlignmentContext ctx = new AlignmentContext(
			SeaFlags.SW |
			SeaFlags.AFFINE_GAP_COST |
			SeaFlags.FULL_DP |
			SeaFlags.SEQ_ASCII |
			SeaFlags.ALN_ASCII,
			1000,
			2, -3, -5, -1,
			1000);

		AlignmentResult aln = ctx.align(
			a, 0, a.length(),
			b, 0, b.length());

		System.out.println(a);
		System.out.println(b);
		System.out.println(aln.aln);
		System.out.println("score:  " + aln.score);
		System.out.println("length: " + aln.len);
		System.out.println("apos:   " + aln.apos);
		System.out.println("alen:   " + aln.alen);
		System.out.println("bpos:   " + aln.bpos);
		System.out.println("blen:   " + aln.blen);

		aln.clean();
		ctx.clean();
	}
}
