
/**
 * @file SeaException.java
 *
 * @brief Exception class implementation
 */

package libsea;

/**
 * @class SeaException
 */
public class SeaException extends Exception {
	private static final long serialVersionUID = 1L;
    private int error;
    public SeaException(int error) {
        super("SeaException");
        this.error = error;
    }
    public int getErrorNumber() {
        return error;
    }
}
