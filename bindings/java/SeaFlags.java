
/**
 * @file Flags.java
 *
 * @brief constants for flags parameter of AlignmentContext class.
 *
 * @detail
 * Compile:
 *
 * $ javac -cp jna-4.1.0.jar AlignmentContext.java AlignmentResult.java SeaFlags.java -d .
 * $ cp jna-4.1.0.jar libsea.jar
 * $ jar uf libsea.jar libsea
 *
 */

package libsea;

/**
 * @class SeaFlags
 *
 * @brief constants for flags parameter of AlignmentContext class
 */
public class SeaFlags {

	private SeaFlags() {}

	private static final int FLAGS_POS_ALG = 0;
	private static final int FLAGS_POS_COST = 2;
	private static final int FLAGS_POS_DP = 4;
	private static final int FLAGS_POS_BANDWIDTH = 6;
	private static final int FLAGS_POS_SEQ = 10;
	private static final int FLAGS_POS_ALN = 12;

	public static final int SW 					= 1<<FLAGS_POS_ALG;
	public static final int SEA					= 2<<FLAGS_POS_ALG;
	public static final int NW 					= 3<<FLAGS_POS_ALG;

	public static final int AFFINE_GAP_COST 	= 1<<FLAGS_POS_COST;
	public static final int LINEAR_GAP_COST 	= 2<<FLAGS_POS_COST;
	public static final int UNIT_COST 			= 3<<FLAGS_POS_COST;

	public static final int FULL_DP				= 1<<FLAGS_POS_DP;
	public static final int BANDED_DP			= 2<<FLAGS_POS_DP;
	public static final int DYNAMIC_BANDED_DP	= 3<<FLAGS_POS_DP;

	public static final int BANDWIDTH_16 		= 1<<FLAGS_POS_BANDWIDTH;
	public static final int BANDWIDTH_32 		= 2<<FLAGS_POS_BANDWIDTH;
	public static final int BANDWIDTH_64 		= 3<<FLAGS_POS_BANDWIDTH;
	public static final int BANDWIDTH_128 		= 4<<FLAGS_POS_BANDWIDTH;

	public static final int SEQ_ASCII 			= 1<<FLAGS_POS_SEQ;
	public static final int SEQ_4BIT 			= 2<<FLAGS_POS_SEQ;
	public static final int SEQ_2BIT 			= 3<<FLAGS_POS_SEQ;
	public static final int SEQ_4BIT8PACKED 	= 4<<FLAGS_POS_SEQ;
	public static final int SEQ_2BIT8PACKED 	= 5<<FLAGS_POS_SEQ;
	public static final int SEQ_1BIT64PACKED 	= 6<<FLAGS_POS_SEQ;

	public static final int ALN_ASCII 			= 1<<FLAGS_POS_ALN;
	public static final int ALN_CIGAR			= 2<<FLAGS_POS_ALN;


}
