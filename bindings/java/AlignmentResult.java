
/**
 * @file AlignmentResult.java
 *
 * @brief a java wrapper of sea_result struct, based on JNA.
 *
 * @detail
 * Compile:
 *
 * $ javac -cp jna-4.1.0.jar AlignmentContext.java AlignmentResult.java SeaFlags.java SeaException.java -d .
 * $ cp jna-4.1.0.jar libsea.jar
 * $ jar uf libsea.jar libsea
 *
 */

package libsea;

import java.util.List;
import java.util.Arrays;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;


/**
 * @class AlignmentResult
 * @brief java equivalent to sea_result struct in C.
 */
public class AlignmentResult extends Structure {
	public String a;
	public String b;
	public String aln;
	public int score;
	public int len;
	public int apos;
	public int alen;
	public int bpos;
	public int blen;

	public void clean() {
		SeaLibrary.INSTANCE.sea_aln_free(this);
	}

	@Override
	protected List<String> getFieldOrder() {
		return Arrays.asList(
			new String[] {
				"a", "b",
				"aln",
				"score",
				"len",
				"apos", "alen",
				"bpos", "blen"});
	}
}
