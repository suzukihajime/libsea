
/**
 * @file AlignmentContext.java
 *
 * @brief AlignmentContext class implementation
 *
 * @detail
 * Compile:
 *
 * $ javac -cp jna-4.1.0.jar AlignmentContext.java AlignmentResult.java SeaFlags.java SeaException.java -d .
 * $ cp jna-4.1.0.jar libsea.jar
 * $ jar uf libsea.jar libsea
 *
 */

package libsea;

import java.util.List;
import java.util.Arrays;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;


/**
 * @interface SeaLibrary
 * @brief java interface to libsea C API, based on JNA.
 */
interface SeaLibrary extends Library {
	SeaLibrary INSTANCE = (SeaLibrary)Native.loadLibrary("sea", SeaLibrary.class);

	public long sea_init(
		int flags,
		int length,
		int m, int x, int gi, int ge,
		int xdrop);
	public AlignmentResult sea_align(
		long ctx,
		Pointer a, int apos, int alen,
		Pointer b, int bpos, int blen);
	public AlignmentResult sea_align(
		long ctx,
		String a, int apos, int alen,
		String b, int bpos, int blen);
	public int sea_get_error_num(
		long ctx,
		AlignmentResult aln);
	public void sea_aln_free(
		AlignmentResult aln);
	public void sea_clean(
		long ctx);
}

/**
 * @class AlignmentContext
 *
 * @brief a wrapper class of sea_context struct.
 */
public class AlignmentContext {

	private long _ctx;		/** a pointer to struct sea_context */

	public AlignmentContext(
		int flags,
		int length,
		int m, int x, int gi, int ge,
		int xdrop)
	throws SeaException {
		_ctx = SeaLibrary.INSTANCE.sea_init(
			flags,
			length,
			m, x, gi, ge,
			xdrop);
		if(_ctx == 0) {
			throw new SeaException(-5);
		}
		if(SeaLibrary.INSTANCE.sea_get_error_num(_ctx, null) < 0) {
			throw new SeaException(SeaLibrary.INSTANCE.sea_get_error_num(_ctx, null));
		}
		/**
		 * if wrong argument is passed, sea_init returns NULL.
		 */
	}

	public void clean() {
		if(_ctx != 0) {
			SeaLibrary.INSTANCE.sea_clean(_ctx);
		}
	}

	public AlignmentResult align(
		Pointer a,
		int apos,
		int alen,
		Pointer b,
		int bpos,
		int blen)
	throws SeaException {
		if(_ctx != 0) {
			AlignmentResult aln = SeaLibrary.INSTANCE.sea_align(
					_ctx,
					a, apos, alen,
					b, bpos, blen);
			if(aln == null) {
				throw new SeaException(-5);
			}
			if(SeaLibrary.INSTANCE.sea_get_error_num(_ctx, aln) < 0) {
				throw new SeaException(SeaLibrary.INSTANCE.sea_get_error_num(_ctx, aln));
			}
			return(aln);
		}
		return(null);
	}

	public AlignmentResult align(
		String a,
		int apos,
		int alen,
		String b,
		int bpos,
		int blen)
	throws SeaException {
		if(_ctx != 0) {
			AlignmentResult aln = SeaLibrary.INSTANCE.sea_align(
					_ctx,
					a, apos, alen,
					b, bpos, blen);
			if(aln == null) {
				throw new SeaException(-5);
			}
			if(SeaLibrary.INSTANCE.sea_get_error_num(_ctx, aln) < 0) {
				throw new SeaException(SeaLibrary.INSTANCE.sea_get_error_num(_ctx, aln));
			}
			return(aln);
		}
		return(null);
	}

	public AlignmentResult align(
		String a,
		String b)
	throws SeaException {
		if(_ctx != 0) {
			AlignmentResult aln = SeaLibrary.INSTANCE.sea_align(
					_ctx,
					a, 0, a.length(),
					b, 0, b.length());
			if(aln == null) {
				throw new SeaException(-5);
			}
			if(SeaLibrary.INSTANCE.sea_get_error_num(_ctx, aln) < 0) {
				throw new SeaException(SeaLibrary.INSTANCE.sea_get_error_num(_ctx, aln));
			}
			return(aln);
		}
		return(null);
	}
}
