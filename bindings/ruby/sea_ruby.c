
#include <ruby.h>

void Init_Sea(void) {
	VALUE rb_cAlignmentContext;

	rb_cAlignmentContext = rb_define_class("AlignmentContext", rb_cObject);

	rb_define_method(rb_cAlignmentContext, "Init", rb_sea_init, 7);
}

