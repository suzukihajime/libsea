
/**
 * @file sea.di
 *
 * @brief D import for libsea C API.
 *
 * @author Hajime Suzuki
 * @date 2014/01/13
 * @detail
 * See example.d for a simple usage of this wrapper.
 */

module sea;

import std.string;

alias 	int 		sea_int_t;			/**< sea_int_t: for general integer variables. must be signed, 32-bit integer or larger. */
alias 	int 		sea_cell_t;			/**< sea_cell_t: for cell values in DP matrices. must be signed, 32-bit integer. */
alias	const int 	sea_const_t;

/**
 * @enum sea_flags_pos
 *
 * @brief positions of option flag bit field.
 */
sea_const_t SEA_FLAGS_POS_ALG = 0;
sea_const_t SEA_FLAGS_POS_COST = 2;
sea_const_t SEA_FLAGS_POS_DP = 4;
sea_const_t SEA_FLAGS_POS_BANDWIDTH = 6;
sea_const_t SEA_FLAGS_POS_SEQ = 10;
sea_const_t SEA_FLAGS_POS_ALN = 12;
sea_const_t SEA_FLAGS_POS_PROC = 14;

/**
 * @enum sea_flags_mask
 *
 * @brief bit-field masks
 */
sea_const_t SEA_FLAGS_MASK_ALG 		= 0x03<<SEA_FLAGS_POS_ALG;
sea_const_t SEA_FLAGS_MASK_COST		= 0x03<<SEA_FLAGS_POS_COST;
sea_const_t SEA_FLAGS_MASK_DP		= 0x03<<SEA_FLAGS_POS_DP;
sea_const_t SEA_FLAGS_MASK_BANDWIDTH= 0x0f<<SEA_FLAGS_POS_BANDWIDTH;
sea_const_t SEA_FLAGS_MASK_SEQ		= 0x03<<SEA_FLAGS_POS_SEQ;
sea_const_t SEA_FLAGS_MASK_ALN		= 0x03<<SEA_FLAGS_POS_ALN;
sea_const_t SEA_FLAGS_MASK_PROC 	= 0x03<<SEA_FLAGS_POS_PROC;

/**
 * @enum sea_flags_alg
 *
 * @brief constants of algorithm option.
 */
sea_const_t SEA_SW 					= 1<<SEA_FLAGS_POS_ALG;
sea_const_t SEA_SEA					= 2<<SEA_FLAGS_POS_ALG;
sea_const_t SEA_NW 					= 3<<SEA_FLAGS_POS_ALG;

/**
 * @enum sea_flags_cost
 *
 * @brief constants of cost option.
 */
sea_const_t SEA_AFFINE_GAP_COST 	= 1<<SEA_FLAGS_POS_COST;
sea_const_t SEA_LINEAR_GAP_COST 	= 2<<SEA_FLAGS_POS_COST;
sea_const_t SEA_UNIT_COST 			= 3<<SEA_FLAGS_POS_COST;

/**
 * @enum sea_flags_dp
 *
 * @brief constants of the DP option.
 */
sea_const_t SEA_FULL_DP				= 1<<SEA_FLAGS_POS_DP;
sea_const_t SEA_BANDED_DP			= 2<<SEA_FLAGS_POS_DP;
sea_const_t SEA_DYNAMIC_BANDED_DP	= 3<<SEA_FLAGS_POS_DP;

/**
 * @enum sea_flags_bandwidth
 *
 * @brief constants of the bandwidth option.
 */
sea_const_t SEA_BANDWIDTH_16 		= 1<<SEA_FLAGS_POS_BANDWIDTH;
sea_const_t SEA_BANDWIDTH_32 		= 2<<SEA_FLAGS_POS_BANDWIDTH;
sea_const_t SEA_BANDWIDTH_64 		= 3<<SEA_FLAGS_POS_BANDWIDTH;
sea_const_t SEA_BANDWIDTH_128 		= 4<<SEA_FLAGS_POS_BANDWIDTH;

/**
 * @enum sea_flags_seq
 *
 * @brief constants of the sequence format option.
 */
sea_const_t SEA_SEQ_ASCII 			= 1<<SEA_FLAGS_POS_SEQ;
sea_const_t SEA_SEQ_4BIT 			= 2<<SEA_FLAGS_POS_SEQ;
sea_const_t SEA_SEQ_2BIT 			= 3<<SEA_FLAGS_POS_SEQ;
sea_const_t SEA_SEQ_4BIT8PACKED 	= 4<<SEA_FLAGS_POS_SEQ;
sea_const_t SEA_SEQ_2BIT8PACKED 	= 5<<SEA_FLAGS_POS_SEQ;
sea_const_t SEA_SEQ_1BIT64PACKED 	= 6<<SEA_FLAGS_POS_SEQ;

/**
 * @enum sea_flags_aln
 *
 * @brief constants of the alignment format option.
 */
sea_const_t SEA_ALN_ASCII 			= 1<<SEA_FLAGS_POS_ALN;
sea_const_t SEA_ALN_CIGAR			= 2<<SEA_FLAGS_POS_ALN;

/**
 * @enum sea_error
 *
 * @brief error flags. see sea_init function and status member in the sea_result structure for more details.
 */
sea_const_t SEA_SUCCESS 				=  0;	/*!< success */
sea_const_t SEA_ERROR 					= -1;	/*!< unknown error */
sea_const_t SEA_ERROR_INVALID_MEM 		= -2;	/*!< invalid pointer to memory */
sea_const_t SEA_ERROR_INVALID_CONTEXT 	= -3;	/*!< invalid pointer to the alignment context */
sea_const_t SEA_ERROR_OUT_OF_BAND 		= -4;	/*!< traceback failure. using wider band may resolve this type of error. */
sea_const_t SEA_ERROR_OUT_OF_MEM 		= -5;	/*!< out of memory error. mostly caused by exessively long queries. */
sea_const_t SEA_ERROR_ARGS 				= -6;	/*!< inproper input arguments. */
sea_const_t SEA_ERROR_UNSUPPORTED_ALG 	= -7;	/*!< unsupported combination of algorithm and processor options. use naive implementations instead. */

/**
 * @struct sea_aln
 *
 * @brief a structure containing an alignment result.
 *
 * @sa sea_sea, sea_aln_free
 */
struct sea_result {
	const(void)*a; 			/*!< a pointer to a sequence a. */
	const(void)*b;			/*!< a pointer to a sequence b. */
	void *aln;				/*!< a pointer to a alignment result. */
	sea_int_t score;		/*!< an alignment score. */
	sea_int_t len;			/*!< alignment length. (the length of a content of aln) */
	sea_int_t apos;			/*!< alignment start position on a. */
	sea_int_t alen;			/*!< alignment length on a. the alignment interval is a[apos]..a[apos+alen-1] */
	sea_int_t bpos;			/*!< alignment start position on b. */
	sea_int_t blen;			/*!< alignment length on b. the alignment interval is b[bpos]..b[bpos+blen-1] */
	sea_context *ctx;/*!< a pointer to a alignment context structure. */
}

/**
 * @struct sea_context
 *
 * @brief a structure containing an alignment context.
 *
 * @sa sea_init, sea_clean
 */
struct sea_context {
	sea_int_t flags;		/*!< a bitfield of option flags */
	sea_int_t scmat[4];		/*!< a dynamic programming cost array. scmat[0] is match award. scmat[1] is mismatch cost. scmat[2] and scmat[3] are gap-open and gap-extension cost, respectively. see sea_init API for more details. */ 
	sea_int_t xdrop;		/*!< xdrop threshold. see sea_init for more details */
	sea_int_t bandwidth;	/*!< the width of the band. */
	sea_int_t function(		/*!< a pointer to a function which returns the size of dynamic programming matrices in bytes. */
		sea_int_t alen,
		sea_int_t blen,
		sea_int_t bandwidth)
			_sea_matsize;
	sea_int_t function(	/*!< a pointer to an alignment function. */
		sea_result *aln,
		const sea_int_t scmat[4],
		sea_int_t xdrop,
		sea_int_t bandwidth,
		void *mat,
		sea_int_t matsize)
			_sea_sea;
}

/**
 * C function declarations; use extern (C) to specify calling convention
 */
extern (C) {

/**
 * @fn sea_init
 *
 * @brief constructs and initializes an alignment context.
 *
 * @param[in] flags : option flags. see flags.h for more details.
 * @param[in] len : maximal alignment length. len must hold len > 0.
 * @param[in] m : match award in the Dynamic Programming. (m > 0)
 * @param[in] x :  mismatch cost. (x < 0)
 * @param[in] gi : gap open cost. (or just gap cost in the linear-gap cost) (gi < 0)
 * @param[in] ge : gap extension cost. (ge < 0) valid only in the affine-gap cost. the total penalty of the gap with length L is gi + (L - 1)*ge.
 * @param[in] xdrop : xdrop threshold. (xdrop > 0) valid only in the seed-and-extend alignment. the extension is terminated when the score S meets S < max - xdrop.
 *
 * @return a pointer to sea_context structure.
 *
 * @sa sea_free, sea_sea
 */
sea_context *sea_init(
	sea_int_t flags,
	sea_int_t len,
	sea_int_t m,
	sea_int_t x,
	sea_int_t gi,
	sea_int_t ge,
	sea_int_t xdrop);

/**
 * @fn sea_aln
 *
 * @brief alignment function. 
 *
 * @param[ref] ctx : a pointer to an alignment context structure. must be initialized with sea_init function.
 * @param[in] a : a pointer to the query sequence a. see seqreader.h for more details of query sequence formatting.
 * @param[in] apos : the alignment start position in a. (0 <= apos < length(sequence a)) (or search start position in the Smith-Waterman algorithm). the alignment includes the position apos.
 * @param[in] alen : the extension length in a. (0 < alen) (to be exact, alen is search area length in the Smith-Waterman algorithm. the maximum extension length in the seed-and-extend alignment algorithm. the length of the query a to be aligned in the Needleman-Wunsch algorithm.)
 * @param[in] b : a pointer to the query sequence b.
 * @param[in] bpos : the alignment start position in b. (0 <= bpos < length(sequence b))
 * @param[in] blen : the extension length in b. (0 < blen)
 *
 * @return a pointer to the sea_result structure.
 *
 * @sa sea_init
 */
sea_result *sea_align(
	sea_context *ctx,
	const(void)*a,
	sea_int_t apos,
	sea_int_t alen,
	const(void)*b,
	sea_int_t bpos,
	sea_int_t blen);

/**
 * @fn sea_aln_free
 *
 * @brief clean up sea_result structure
 *
 * @param[in] aln : an pointer to sea_result structure.
 *
 * @return none.
 *
 * @sa sea_sea
 */
void sea_aln_free(
	sea_result *aln);

/**
 * @fn sea_clean
 *
 * @brief clean up the alignment context structure.
 *
 * @param[in] ctx : a pointer to the alignment structure.
 *
 * @return none.
 *
 * @sa sea_init
 */
void sea_clean(
	sea_context *ctx);

} /* end of extern (C) */

/**
 * @class AlignmentResult
 */
class AlignmentResult {
private:
	sea_result *_aln;

public:
	this(sea_result *aln) {
		_aln = aln;
	}
	~this() {
		sea_aln_free(_aln);
	}
	const(void)*a() @property {
		return _aln.a;
	}
	const(void)*a(const(void)*n) @property {
		_aln.a = n; return _aln.a;
	}
	int apos() @property {
		return _aln.apos;
	}
	int apos(int n) @property {
		_aln.apos = n; return _aln.apos;
	}
	int alen() @property {
		return _aln.alen;
	}
	int alen(int n) @property {
		_aln.alen = n; return _aln.alen;
	}
	const(void)*b() @property {
		return _aln.b;
	}
	const(void)*b(const(void)*n) @property {
		_aln.b = n; return _aln.b;
	}
	int bpos() @property {
		return _aln.bpos;
	}
	int bpos(int n) @property {
		_aln.bpos = n; return _aln.bpos;
	}
	int blen() @property {
		return _aln.blen;
	}
	int blen(int n) @property {
		_aln.blen = n; return _aln.blen;
	}
	void *aln() @property {
		return _aln.aln;
	}
	void *aln(void *n) @property {
		_aln.aln = n; return _aln.aln;
	}
	char[] alnstr() @property {
		return fromStringz(cast(char *)_aln.aln);
	}
	char[] alnstr(char[] n) @property {
		_aln.aln = n.ptr; return fromStringz(cast(char *)_aln.aln);
	}
	int score() @property {
		return _aln.score;
	}
	int score(int n) @property {
		_aln.score = n; return _aln.score;
	}
	int len() @property {
		return _aln.len;
	}
	int len(int n) @property {
		_aln.len = n; return _aln.len;
	}
}


/**
 * @class AlignmentContext
 */
class AlignmentContext {

private:
	sea_context *_ctx;

public:
	this(
		sea_int_t flags,
		sea_int_t len,
		sea_int_t m,
		sea_int_t x,
		sea_int_t gi,
		sea_int_t ge,
		sea_int_t xdrop) {
		_ctx = sea_init(flags, len, m, x, gi, ge, xdrop);
	}

	~this() {
		if(_ctx != null) {
			sea_clean(_ctx);
			_ctx = null;
		}
	}

	AlignmentResult getAlignment(
		const(void)*a, ulong apos, ulong alen,
		const(void)*b, ulong bpos, ulong blen) {

		auto aln = sea_align(
			_ctx,						/** the alignment context */
			a,							/** a pointer to the sequence a */
			cast(int)apos,				/** a start position on sequence a */
			cast(int)alen,				/** the length of the sequence a */
			b,							/** a pointer to the sequence b */
			cast(int)bpos,				/** a start position on sequence a */
			cast(int)blen);				/** the length of the sequence a */
		return(new AlignmentResult(aln));
	}

	AlignmentResult getAlignment(
		string a, ulong apos, ulong alen,
		string b, ulong bpos, ulong blen) {

		auto aln = sea_align(
			_ctx,						/** the alignment context */
			cast(const(void)*)a.ptr,	/** a pointer to the sequence a */
			cast(int)apos,				/** a start position on sequence a */
			cast(int)alen,				/** the length of the sequence a */
			cast(const(void)*)b.ptr,	/** a pointer to the sequence b */
			cast(int)bpos,				/** a start position on sequence a */
			cast(int)blen);				/** the length of the sequence a */
		return(new AlignmentResult(aln));
	}

	AlignmentResult getAlignment(
		string a,
		string b) {

		auto aln = sea_align(
			_ctx,						/** the alignment context */
			cast(const(void)*)a.ptr,	/** a pointer to the sequence a */
			0,							/** a start position on sequence a */
			cast(int)a.length,			/** the length of the sequence a */
			cast(const(void)*)b.ptr,	/** a pointer to the sequence b */
			0,							/** a start position on sequence a */
			cast(int)b.length);			/** the length of the sequence a */
		return(new AlignmentResult(aln));
	}
}

/*
 * end of sea.h
 */
