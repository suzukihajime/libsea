
/**
 * @file example.d
 *
 * @brief a simple example of a libsea D API
 *
 * @detail
 * Compile:
 * 
 * $ dmd example.d sea.d -L-lsea
 */
import std.stdio;
import std.string;
import sea;

void main() {
	auto a = "AAAA";
	auto b = "AAAA";

	auto ctx = new AlignmentContext(
		SEA_NW |				/** the Smith-Waterman algorithm */
		SEA_AFFINE_GAP_COST |	/** the affine-gap cost */
		SEA_BANDED_DP |			/** the banded dynamic programming */
		SEA_BANDWIDTH_32 |		/** bandwidth : 32 */
		SEA_SEQ_ASCII |			/** input sequence format : ASCII */
		SEA_ALN_ASCII,			/** output format : ASCII */
		10000,					/** maximum input sequence length : 10000 */
		2, -3, -5, -1,			/** match, mismatch, gap open, and gap extension cost */
		100);

	auto aln = ctx.getAlignment(
		a, 0, a.length,
		b, 0, b.length);

	writeln(a);
	writeln(b);
	writeln(aln.alnstr);
	writeln("score:  ", aln.score);
	writeln("length: ", aln.len);
	writeln("apos:   ", aln.apos);
	writeln("alen:   ", aln.alen);
	writeln("bpos:   ", aln.bpos);
	writeln("blen:   ", aln.blen);
	return;
}

