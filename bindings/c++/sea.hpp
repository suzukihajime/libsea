
/**
 * @header sea.hpp
 *
 * @brief a C++ binding of libsea C API
 *
 * @author Hajime Suzuki
 * @date 2015/1/11
 *
 * @detail
 * See README and sea.h for general information on this library.
 *
 * Usage:
 *   See example.cpp for a simple usage.
----    example.cpp    ----
#include <iostream>
#include <string>
#include "sea.hpp"

using namespace sea;
using namespace std;

int main(void) {
	string a("AAAA");
	string b("AAAA");

	AlignmentContext ctx(
		SEA_SW |				// the Smith-Waterman algorithm
		SEA_AFFINE_GAP_COST |	// the affine-gap cost
		SEA_BANDED_DP |			// the banded dynamic programming
		SEA_BANDWIDTH_32 |		// bandwidth : 32
		SEA_SEQ_ASCII |			// input sequence format : ASCII
		SEA_ALN_ASCII,			// output format : ASCII
		10000,					// maximum input sequence length : 10000
		2, -3, -5, -1,			// match, mismatch, gap open, and gap extension cost
		100);					// the xdrop threshold

	AlignmentResult aln = ctx.align(
		a, 0, a.length(),		// a pointer to seq.a, start postion on a, and search length on a.
		b, 0, b.length());		// a pointer to seq.b, start postion on b, and search length on b.

	cout << a << endl
		 << b << endl
		 << (char *)aln.aln() << endl;
	cout << "score:  " << aln.score() << endl
		 << "length: " << aln.len() << endl
		 << "apos:   " << aln.apos() << endl
		 << "alen:   " << aln.alen() << endl
		 << "bpos:   " << aln.bpos() << endl
		 << "blen:   " << aln.blen() << endl;
	return 0;
}
---- end of example.cpp ----
 *
 * @sa example.cpp
 */

#include <string>

namespace sea {

	#include <sea.h>

	/**
	 * @class AlignmentResult
	 *
	 * @brief wrapper class of struct sea_aln.
	 */
	class AlignmentResult {
	private:
		struct sea_result *_aln;
	public:
		/**
		 * constructor and destructor
		 */
		AlignmentResult(
			struct sea_result *aln) {
			_aln = aln;
		}
		~AlignmentResult(void) {
			sea_aln_free(_aln);			
		}
		/**
		 * setters and getters
		 *
		 * sequence a
		 */
		void const *a(void) const {
			return(_aln->a);
		}
		void const *a(void const *n) {
			_aln->a = n;
			return(_aln->a);
		}

		sea_int_t &apos(void) const {
			return(_aln->apos);
		}
		sea_int_t &apos(sea_int_t const &n) {
			_aln->apos = n;
			return(_aln->apos);
		}

		sea_int_t &alen(void) const {
			return(_aln->alen);
		}
		sea_int_t &alen(sea_int_t const &n) {
			_aln->alen = n;
			return(_aln->alen);
		}

		/**
		 * sequence b
		 */
		void const *b(void) const {
			return(_aln->b);
		}
		void const *b(void const *n) {
			_aln->b = n;
			return(_aln->b);
		}

		sea_int_t &bpos(void) const {
			return(_aln->bpos);
		}
		sea_int_t &bpos(sea_int_t const &n) {
			_aln->bpos = n;
			return(_aln->bpos);
		}

		sea_int_t &blen(void) const {
			return(_aln->blen);
		}
		sea_int_t &blen(sea_int_t const &n) {
			_aln->blen = n;
			return(_aln->blen);
		}

		/**
		 * alignment string, score, and length
		 */
		void *aln(void) const {
			return(_aln->aln);
		}
		void *aln(void *n) {
			_aln->aln = n;
			return(_aln->aln);
		}
		sea_int_t &score(void) const {
			return(_aln->score);
		}
		sea_int_t &score(sea_int_t const &n) {
			_aln->score = n;
			return(_aln->score);
		}
		sea_int_t &len(void) const {
			return(_aln->len);
		}
		sea_int_t &len(sea_int_t const &n) {
			_aln->len = n;
			return(_aln->len);
		}
	};

	class AlignmentContext {
	private:
		struct sea_context *_ctx;

	public:
		/**
		 * constructor. see sea.h for details of parameters.
		 */
		AlignmentContext(
			sea_int_t flags,
			sea_int_t len,
			sea_int_t m,
			sea_int_t x,
			sea_int_t gi,
			sea_int_t ge,
			sea_int_t xdrop) {
			_ctx = sea_init(flags, len, m, x, gi, ge, xdrop);
		}
		/**
		 * destructor, calls sea_aln_free
		 */
		~AlignmentContext(void) {
			if(_ctx != NULL) {
				sea_clean(_ctx);
				_ctx = NULL;
			}
		}
		/**
		 * alignment functions. see sea.h
		 */
		AlignmentResult align(
			void const *a, sea_int_t apos, sea_int_t alen,
			void const *b, sea_int_t bpos, sea_int_t blen) {
			return(AlignmentResult(
				sea_align(_ctx, a, apos, alen, b, bpos, blen)));
		}

		AlignmentResult align(
			std::string a, sea_int_t apos, sea_int_t alen,
			std::string b, sea_int_t bpos, sea_int_t blen) {
			return(AlignmentResult(
				sea_align(_ctx, a.c_str(), apos, alen, b.c_str(), bpos, blen)));
		}

		AlignmentResult align(
			std::string a,
			std::string b) {
			return(AlignmentResult(
				sea_align(_ctx, a.c_str(), 0, a.length(), b.c_str(), 0, b.length())));
		}
	};
}

/**
 * end of sea.hpp
 */
