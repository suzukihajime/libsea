
/**
 * @file example.cpp
 *
 * @brief a simple example of libsea C++ APIs
 *
 * @sa sea.hpp
 */
#include <iostream>
#include <string>
#include "sea.hpp"

using namespace sea;
using namespace std;

int main(void) {
	string a("AAAA");
	string b("AAAA");

	AlignmentContext ctx(
		SEA_SW |				// the Smith-Waterman algorithm
		SEA_AFFINE_GAP_COST |	// the affine-gap cost
		SEA_BANDED_DP |			// the banded dynamic programming
		SEA_BANDWIDTH_32 |		// bandwidth : 32
		SEA_SEQ_ASCII |			// input sequence format : ASCII
		SEA_ALN_ASCII,			// output format : ASCII
		10000,					// maximum input sequence length : 10000
		2, -3, -5, -1,			// match, mismatch, gap open, and gap extension cost
		100);					// the xdrop threshold

	AlignmentResult aln = ctx.align(
		a, 0, a.length(),		// a pointer to seq.a, start postion on a, and search length on a.
		b, 0, b.length());		// a pointer to seq.b, start postion on b, and search length on b.

	cout << a << endl
		 << b << endl
		 << (char *)aln.aln() << endl;
	cout << "score:  " << aln.score() << endl
		 << "length: " << aln.len() << endl
		 << "apos:   " << aln.apos() << endl
		 << "alen:   " << aln.alen() << endl
		 << "bpos:   " << aln.bpos() << endl
		 << "blen:   " << aln.blen() << endl;

	return 0;
}
