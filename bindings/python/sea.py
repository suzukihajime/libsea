#! /usr/bin/env python
# encoding: utf-8

"""
@file sea.py

@brief a Python wrapper of libsea C APIs

@author Hajime Suzuki
@date 2014/01/11

@detail
for a simple example, see example.py.
Here is a copy of example.py
----    example.py    ----

import sea
import sys

a = b'AAAA'
b = b'AAAA'

ctx = sea.AlignmentContext(
	# list of options
	['SW',						# the Smith-Waterman algorithm
	'AFFINE_GAP_COST',			# the affine-gap cost
	'FULL_DP'],					# use a full dynamic programming matrix
	1000,						# the length of query sequence
	2, -3, -5, -1,				# M, X, Gi, and Ge
	100)						# xdrop threshold

result = ctx.align(
	a, 0, len(a),				# a pointer to seq.a, start postion on a, and search length on a.
	b, 0, len(b))				# a pointer to seq.b, start postion on b, and search length on b.

print(a.decode())
print(b.decode())
print(result.aln().decode())
print('score:  ', result.score())
print('length: ', result.len())
print('apos:   ', result.apos())
print('alen:   ', result.alen())
print('bpos:   ', result.bpos())
print('blen:   ', result.blen())

---- end of example.py ----
"""

import sys
import re
import ctypes

class SeaError(Exception):

	error_str_dict = {
		 0: 'success',
		-1: 'unknown error',
		-2: 'invalid pointer',
		-3: 'invalid pointer to context',
		-4: 'traceback out of band',
		-5: 'out of memory',
		-6: 'cell overflow',
		-7: 'invalid arguments',
		-8: 'unsupported algorithm',
		-9: 'invalid cost'
	}

	def __init__(self, value):
		self.value = self.error_str_dict[value]
	def __str__(self):
		return self.value

class AlignmentContext:
	"""
	@class AlignmentContext

	@brief a python wrapper class of a sea_context struct.
	"""

	_ctx = 0
	flag_encode_dict = {
		'sw' 				: (1<<0),
		'sea'				: (2<<0),
		'nw' 				: (3<<0),
		'smithswaterman' 	: (1<<0),
		'seedandextend'		: (2<<0),
		'needlemanwunsch' 	: (3<<0),

		'affine' 			: (1<<2),
		'linear'		 	: (2<<2),
		'affinegap' 		: (1<<2),
		'lineargap'	 		: (2<<2),
		'affinegapcost' 	: (1<<2),
		'lineargapcost' 	: (2<<2),
		'unit' 				: (3<<2),
		'unitcost' 			: (3<<2),
		'edit'				: (3<<2),
		'editdist'			: (3<<2),
		'editdistcost'		: (3<<2),
		'bit'				: (3<<2),
		'bitparallel'		: (3<<2),

		'full'				: (1<<4),
		'banded'			: (2<<4),
		'dynamicbanded'		: (3<<4),
		'fulldp'			: (1<<4),
		'bandeddp'			: (2<<4),
		'dynamicbandeddp'	: (3<<4),

		'16' 				: (1<<6),
		'32' 				: (2<<6),
		'64' 				: (3<<6),
		'128' 				: (4<<6),
		'band16' 			: (1<<6),
		'band32' 			: (2<<6),
		'band64' 			: (3<<6),
		'band128'			: (4<<6),
		'bandwidth16' 		: (1<<6),
		'bandwidth32' 		: (2<<6),
		'bandwidth64' 		: (3<<6),
		'bandwidth128'		: (4<<6),

		'seqascii' 			: (1<<10),
		'alnascii'			: (1<<12)}

	def __init__(self,
		algorithm,
		cost,
		dp_type,
		m, x, gi, ge,
		bandwidth = None,
		seq_type = None,
		aln_type = None,
		length = 0,
		xdrop = 0):
		"""
		@fn __init__

		@brief AlignmentContext constructor
		"""
		# load library
		self.load_library()

		# check arguments
		if m <= 0 or gi > 0 or ge > 0:
			# raise exception here
			raise # invalidCostError

		# default flags
		flags_list = []
		flags_list.append(algorithm)
		flags_list.append(cost)
		flags_list.append(dp_type)

		if bandwidth is not None:
			flags_list.append(str(bandwidth))
		else:
			flags_list.append('32')

		if seq_type is not None:
			flags_list.append(str(seq_type))
		else:
			flags_list.append('SEQ_ASCII')

		if aln_type is not None:
			flags_list.append(str(aln_type))
		else:
			flags_list.append('ALN_ASCII')

		flags = sum([self.encode_flag(f) for f in flags_list])

		if not isinstance(length, int):
			length = int(length)

		if not isinstance(m, int):
			m = int(m)

		if not isinstance(x, int):
			x = int(x)

		if not isinstance(gi, int):
			gi = int(gi)

		if not isinstance(ge, int):
			ge = int(ge)

		if not isinstance(xdrop, int):
			xdrop = int(xdrop)

		# generate context
		self._ctx = self.lib.sea_init(
			flags, length, m, x, gi, ge, xdrop)
		# if NULL is returned, raise exception
		if self._ctx == 0:
			self._ctx = None
			raise SeaError(-5)
		if self.lib.sea_get_error_num(self._ctx, None) < 0:
			raise SeaError(self.lib.sea_get_error_num(self._ctx, None))

#		print(self._ctx)
	def __del__(self):
		if self._ctx is not None:
			self.lib.sea_clean(self._ctx)

	def encode_flag(self, flag):
		f = flag.lower().strip().replace("-", "").replace("_", "").replace(" ", "")
		if f in self.flag_encode_dict:
			return self.flag_encode_dict[f]
		else:
			return 0

	def load_library(self):
		# load library
		libname = 'libsea.so'
		if sys.platform == 'darwin':
			libname = re.compile(r'\..+$').sub('.dylib', libname)
		self.lib = ctypes.cdll.LoadLibrary(libname)

		# function prototypes
		#   sea_init
		self.lib.sea_init.argtypes = [
			ctypes.c_int,		# sea_int_t flags
			ctypes.c_int,		# sea_int_t len
			ctypes.c_int,		# sea_int_t m
			ctypes.c_int,		# sea_int_t x
			ctypes.c_int,		# sea_int_t gi
			ctypes.c_int,		# sea_int_t ge
			ctypes.c_int]		# sea_int_t xdrop
		self.lib.sea_init.restype = ctypes.c_ulong

		#   sea_align
		self.lib.sea_align.argtypes = [
			ctypes.c_ulong,		# struct sea_context *ctx
			ctypes.c_char_p,	# void const *a
			ctypes.c_int,		# sea_int_t apos
			ctypes.c_int,		# sea_int_t alen
			ctypes.c_char_p,	# void const *b
			ctypes.c_int,		# sea_int_t bpos
			ctypes.c_int]		# sea_int_t blen
		self.lib.sea_align.restype = ctypes.POINTER(sea_result)

		#   sea_get_error_num
		self.lib.sea_get_error_num.argtypes = [
			ctypes.c_ulong,		# struct sea_context *ctx
			ctypes.POINTER(sea_result)]		# struct sea_result *aln
		self.lib.sea_get_error_num.restype = ctypes.c_int

		#   sea_aln_free
		self.lib.sea_aln_free.argtypes = [
			ctypes.POINTER(sea_result)]

		#   sea_clean
		self.lib.sea_clean.argtypes = [
			ctypes.c_ulong]		# struct sea_context *ctx


	def align(self, a, apos, alen, b, bpos, blen):
		"""
		@fn align

		@brief a python wrapper of sea_align
		"""
		if not isinstance(a, bytes):
			a = str.encode(a)

		if not isinstance(apos, int):
			apos = int(apos)

		if not isinstance(alen, int):
			alen = int(alen)

		if not isinstance(b, bytes):
			b = str.encode(b)

		if not isinstance(bpos, int):
			bpos = int(bpos)

		if not isinstance(blen, int):
			blen = int(blen)

		if self._ctx is not None:
			res = self.lib.sea_align(
				self._ctx,
				ctypes.c_char_p(a), apos, alen,
				ctypes.c_char_p(b), bpos, blen)
			if res == 0:
				raise SeaError(-5)
			if self.lib.sea_get_error_num(self._ctx, res) < 0:
				raise SeaError(self.lib.sea_get_error_num(self._ctx, res))
			return(AlignmentResult(self, res))
		else:
			return None

class sea_result(ctypes.Structure):
	_fields_ = [
		('a', ctypes.c_char_p),
		('b', ctypes.c_char_p),
		('aln', ctypes.c_char_p),
		('score', ctypes.c_int),
		('len', ctypes.c_int),
		('apos', ctypes.c_int),
		('alen', ctypes.c_int),
		('bpos', ctypes.c_int),
		('blen', ctypes.c_int)]

class AlignmentResult:
	"""
	@class AlignmentResult

	@brief a python wrapper class of a sea_result struct.
	"""
	def __init__(self, ctx, aln):
		"""
		@fn __init__

		@brief AlignmentResult constructor
		"""
		self._ctx = ctx
		self._aln = aln
		self.a = aln.contents.a
		self.apos = aln.contents.apos
		self.alen = aln.contents.alen
		self.b = aln.contents.b
		self.bpos = aln.contents.bpos
		self.blen = aln.contents.blen
		self.len = aln.contents.len
		self.aln = aln.contents.aln
		self.score = aln.contents.score

	def __del__(self):
		"""
		@fn __del__

		@brief AlignmentResult destructor
		"""
		self._ctx.lib.sea_aln_free(self._aln)
