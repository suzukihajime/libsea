#! /usr/bin/env python
# encoding: utf-8

"""
@file example.py

@brief a simple example of a python wrapper of libsea API
"""

import sea
import sys

a = b'AAAA'
b = b'AAAA'

ctx = sea.AlignmentContext(
	# list of options
	'SW',						# the Smith-Waterman algorithm
	'AFFINE_GAP_COST',			# the affine-gap cost
	'FULL_DP',					# use a full dynamic programming matrix
	2, -3, -5, -1,				# M, X, Gi, and Ge
	length = 1000,				# the length of query sequence
	xdrop = 100)				# xdrop threshold

result = ctx.align(
	a, 0, len(a),				# a pointer to seq.a, start postion on a, and search length on a.
	b, 0, len(b))				# a pointer to seq.b, start postion on b, and search length on b.

print(a.decode())
print(b.decode())
print(result.aln.decode())
print('score:  ', result.score)
print('length: ', result.len)
print('apos:   ', result.apos)
print('alen:   ', result.alen)
print('bpos:   ', result.bpos)
print('blen:   ', result.blen)
